{*
 * Tree list
 *
 * Tree filelist template fragment
 *
 * @author Christopher Han <xiphux@gmail.com>
 * @copyright Copyright (c) 2010 Christopher Han
 * @package GitPHP
 * @subpackage Template
 *}

{include file='vf_path.tpl' pathobject=$tree target='tree'}

<table class="table table-striped">
    <thead>
    <tr>
        <th>name</th>
        <th>updated</th>
        <th>message</th>
        <th>activity</th>
    </tr>
    </thead>
    <tbody>
{if $no_commits==false}
    {foreach from=$tree->GetContents() item=treeitem}
    <tr>
        {if $treeitem instanceof GitPHP_Blob}
            <td class="list fileName" colspan="4">
                <i class="icon-file"></i>
                <a href="javascript:void(0)" class="file" data-project="{$project->GetProject()|urlencode}" data-hash="{$treeitem->GetHash()}" data-hb="{$commit->GetHash()}" data-file="{$treeitem->GetPath()|escape:'url'}">{$treeitem->GetName()}</a>
            </td>
            {elseif $treeitem instanceof GitPHP_Tree}
            <td class="list fileName" colspan="4">
                <i class="icon-folder-open"></i>
                <a href="javascript:void(0)" class="folder" data-project="{$project->GetProject()|urlencode}" data-hash="{$treeitem->GetHash()}" data-hb="{$commit->GetHash()}" data-file="{$treeitem->GetPath()|escape:'url'}">{$treeitem->GetName()}</a>
            </td>
        {/if}
    </tr>
    {/foreach}
{else}
    <tr>
        <td colspan="4">No files</td>
    </tr>
{/if}

    </tbody>
</table>
