<?php
class ForumMessage  {
    private $ID=null, $Data=null;

    public function __construct($ID=null, $Result=null){
        if ($Result)
            $this->Data=$Result;
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM forum_messages WHERE message_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the ID of this message
     *
     * @return null|int
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the topic ID this message is in
     *
     * @return int
     */
    public function getTopicID(){
        $this->fetchData();
        return (int)$this->Data['topic_id'];
    }

    /**
     * Gets the forum object that this message is in
     *
     * @return ForumTopic
     */
    public function getTopic(){
        return new ForumTopic($this->getTopicID());
    }

    /**
     * Creates a new message
     *
     * @static
     * @param int $TopicID
     * @param int $UserID
     * @param string $Message
     * @param int $ReplyTo
     * @return bool|int
     */
    static public function create($TopicID, $UserID, $Message, $ReplyTo=0){
        $Time=time();
        if ($Result=db_query_params("INSERT INTO forum_messages(user_id, body, topic_id, reply_to, time_posted) VALUES($1,$2,$3,$4,$5)",array($UserID, $Message, $TopicID, $ReplyTo, $Time))){
            $MessageID=(int)db_insertid($ReplyTo,'forum_messages','message_id');

            db_query_params("UPDATE forum_topics SET last_activity=$1 WHERE topic_id=$2",array($Time, $TopicID));
            user_get_object($UserID)->addForumPostcount();

            return $MessageID;
        }else{
            return false;
        }
    }

    /**
     * Gets the message body
     *
     * @return string
     */
    public function getBody(){
        $this->fetchData();
        return $this->Data['body'];
    }

    /**
     * Gets the user object that created this message
     *
     * @return GFUser
     */
    public function getUser(){
        $this->fetchData();
        return user_get_object($this->Data['user_id']);
    }

    /**
     * Gets the timestamp this message was made
     *
     * @return int
     */
    public function getTime(){
        $this->fetchData();
        return (int)$this->Data['time_posted'];
    }

    /**
     * Gets the message ID that this message this was a reply to
     *
     * @return int
     */
    public function getReplyTo(){
        $this->fetchData();
        return (int)$this->Data['reply_to'];
    }

    /**
     * Generates the HTML to display this message
     *
     * @param int $MessageNumber
     * @param int $ForumID
     * @param int $TopicID
     * @param bool $Threaded
     * @param bool $Community
     * @return string
     */
    public function getHTML($MessageNumber, $ForumID, $TopicID, $Threaded=false, $Community=false){
        $HTML='<div class="message clearfix';
        if ($this->getReplyTo()!=0 && $Threaded==true){
            $HTML.=' reply';
        }
        $HTML.='" id="m'.$this->getID().'" data-message-id="'.$this->getID().'">
            <div class="member_data">
                <a href="'.$this->getUser()->getURL().'">
                    '.$this->getUser()->getImageHTML(80,80).'<br />
                    '.$this->getUser()->getRealName().'
                </a><br />
                <a href="/account/messages.php?action=compose&to='.$this->getUser()->getID().'" target="_none">
                    <span class="icon-envelope"></span>
                </a><br />
                Posts: '.$this->getUser()->getForumPostCount().'
            </div>
            <div class="message_data">
                <div class="message_meta">
                    <span class="pull-left">
                        '.date('M d, Y g:ia',$this->getTime());
		// Check if this message can be replied to (is it the first message, is the user signed in, is this nested further than 1 level)
        if ($this->getReplyTo()==0 && $MessageNumber!=1 && session_get_user()){
            $HTML.=' <a href="?';
            if (!$Community)
                $HTML.='group_id='.$this->getTopic()->getGroupID().'&';
            $HTML.='forum='.$ForumID.'&topic='.$TopicID.'&cmd=reply&message='.$this->getID().'">Reply</a>';
        }

        $HTML.='</span>
                    <span class="pull-right">';

        if ($this->getReplyTo()!=0 && $Threaded==false){
            $ResponseTo=new ForumMessage($this->getReplyTo());
            $HTML.='In response to <a href="#m'.$ResponseTo->getID().'">'.$ResponseTo->getUser()->getRealName().'</a> | ';
        }

        $HTML.='<a href="#m'.$this->getID().'">#'.$MessageNumber.'</a>
                    </span>
                </div>
                <div class="message_body">
                    '.$this->getBody().'
                </div>
            </div>
        </div>
        <hr />';

        return $HTML;
    }
}
?>