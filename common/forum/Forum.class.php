<?php
class Forum {
    private $ID=null, $Data=null;

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM forums WHERE forum_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the ID of this forum
     *
     * @return null|int
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the name of this forum
     *
     * @return string
     */
    public function getName(){
        $this->fetchData();
        return $this->Data['name'];
    }

    /**
     * Gets the group this forum belongs in
     *
     * @return int
     */
    public function getGroupID(){
        $this->fetchData();
        return (int)$this->Data['group_id'];
    }

    /**
     * Gets the description of this forum
     *
     * @return string
     */
    public function getDescription(){
        $this->fetchData();
        return $this->Data['description'];
    }

    /**
     * Gets the timestamp of the last activity within this forum
     *
     * @return int
     */
    public function getLastActivity(){
        $Result=db_query_params("SELECT last_activity FROM forum_topics ORDER BY last_activity DESC LIMIT 1");
        return (int)db_result($Result,0,'last_activity');
    }

    /**
     * Gets all topics within this forum
     *
     * @return bool|array of ForumTopic objects
     */
    public function getTopics(){
        if ($Result=db_query_params("SELECT topic_id FROM forum_topics WHERE forum_id=$1 ORDER BY last_activity DESC", array($this->getID()))){
            $Return=array();

            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=new ForumTopic($Row['topic_id']);

            return $Return;
        }else{
            return false;
        }
    }

    /**
     * Gets the URL to go to this forum
     *
     * @return string
     */
    public function getURL(){
        return '/forum/forum.php?group_id='.$this->getGroupID().'&forum='.$this->getID();
    }

    /**
     * Generates the HTML link to go to this forum
     *
     * @return string
     */
    public function makeLink(){
        return '<a href="'.$this->getURL().'">'.$this->getName().'</a>';
    }

    /**
     * Creates a new forum
     *
     * @static
     * @param string $Name
     * @param string $Description
     * @param int $GroupID
     * @return bool|int
     */
    static public function create($Name, $Description, $GroupID){
        if ($Result=db_query_params("INSERT INTO forums(name,description,group_id) VALUES($1,$2,$3)",array($Name,$Description,$GroupID))){
            return (int)db_insertid($Result,'forums','forum_id');
        }else{
            return false;
        }
    }
}
?>