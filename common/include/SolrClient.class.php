<?php 
require_once '/usr/share/gforge/common/SolrPhpClient/Apache/Solr/Service.php';
require_once SOLR_CONFIG_FILE;

abstract class SolrClient {
  
  var $solrURL;
  var $solrPort;
  var $solrPath;
  var $solrCore;
  
  protected function __construct($solrURL, $solrPort, $solrPath, $solrCore) {
    $this->solrURL = $solrURL;
    $this->solrPort = $solrPort;
    $this->solrCore = $solrCore;
    $this->solrPath = $solrPath;
  }
  
  public function search($query, $start, $resultLimit) {
    $queryParams = array();
    $queryParams["defType"] = "edismax";
    $queryParams["core"] = $this->solrCore;
    $queryParams["start"] = $start;
    $queryParams["rows"] = $resultLimit;
    $this->updateCoreQueryParams($queryParams);
    $solr = new Apache_Solr_Service($this->solrURL, $this->solrPort, $this->solrPath.$this->solrCore);
    $response = $solr->search('*'.$query.'*',$start,$resultLimit,$queryParams);
    return $response->getRawResponse();
  }
    
  public function getResults($results) {
    $thumbs = '';
    $results = json_decode($results, true);
    foreach($results['response']['docs'] as $response) {
      $thumbs = $thumbs.$this->getThumb($response);
    }
    return $thumbs;
  }
  
  abstract function updateCoreQueryParams(&$queryParams);
  abstract function writeModal();
  abstract function writeJavaScriptInclude();
}

?>