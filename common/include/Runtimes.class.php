<?php
class Runtimes extends Error {
	private $ID, $Data=null;

	function __construct($ID=null){
		$this->ID=$ID;
	}

	private function fetch_data(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * from runnable_runtimes WHERE run_id=$1", array($this->ID));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
	}

    function getRuntime(){
        $this->fetch_data();

        return (int)$this->Data['runtime'];
    }

    /**
     * getCompletedDate - Gets a timestamp of the completion date
     * Returns:
     *      (int) timestamp
     *
     * @return int
     */
    function getCompletedDate(){
        $this->fetch_data();

        return (int)$this->Data['date_completed'];
    }

    /**
     * create - Creates a new row in the runnable_runtimes table
     *
     * @static
     * @param $InterfaceID
     * @param int $Runtime
     * @param int $DateCompleted
     * @param $RunID
     * @internal param int $RunnableID
     * @return bool
     */
	static function create($InterfaceID, $Runtime, $DateCompleted, $RunID){
		if (db_query_params("INSERT INTO runnable_runtimes(interface_id, runtime, date_completed, run_id) VALUES($1,$2,$3,$4)", array($InterfaceID, $Runtime, $DateCompleted,$RunID))){
			return true;
		}else{
			self::setError("Unable to insert new runtime");
			return false;
		}
	}

    /**
     * getAvg - Gets the length of time it takes to run a model in seconds
     *
     * @static
     * @internal param int $RunnableID
     * @param $InterfaceID
     * @return float|string
     */
	static function getAvg($InterfaceID){
		$Total=0;
		$Count=0;

		$Result=db_query_params("SELECT * FROM runnable_runtimes WHERE interface_id=$1", array($InterfaceID));
		while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
			$Count++;
			$Total+=$Row['runtime'];
		}

		if ($Count)
			return round(($Total/$Count)/1000, 1);
		else
			return 'N/A';
	}
}
?>