<?php
class Gitolite extends Error  {
    private $Settings=array(), $Changed;

    function __construct(){
       $this->Changed=true;
        $this->loadSettings();
    }

    /**
     * Loads the settings from the database
     */
    private function loadSettings(){
        if ($this->Changed){
            $Result=db_query_params("SELECT * FROM cem_object_git_acl");
            $this->Settings=util_result_columns_to_array($Result,PGSQL_ASSOC);
            $this->Changed=false;
        }
    }

    /**
     * Sets the access permissions for a user on a certain component
     * Permissions can be a combination of:
     *  R - Read
     *  W - Write
     *  + - Rollback
     *
     * @param int $UserID
     * @param int $CEMID
     * @param string $Permissions
     * @return bool
     */
    function setPermissions($UserID, $CEMID, $Permissions){
        if (!is_int($UserID) || !is_int($CEMID) || !is_string($Permissions)){
            $this->setError('Wrong parameter types');
            return false;
        }

        $Result=db_query_params("SELECT permissions FROM cem_object_git_acl WHERE user_id=$1 AND cem_id=$2",array($UserID, $CEMID));
        if (db_numrows($Result)){
            //Already exists
            $Row=db_fetch_array($Result,0,PGSQL_ASSOC);
            if ($Row['permissions']!==$Permissions){
                //Permissions aren't the same as what they are trying to be set to, update permissions
                if (db_query_params("UPDATE cem_object_git_acl SET permissions=$1 WHERE user_id=$2 AND cem_id=$3",array($Permissions,$UserID,$CEMID))){
                    $this->Changed=true;
                    return true;
                }else{
                    $this->setError('Cannot update ACL table');
                    return false;
                }
            }else{
                //Nothing to do
                return true;
            }
        }else{
            //Create new record
            if (db_query_params("INSERT INTO cem_object_git_acl(user_id,cem_id,permissions) values($1,$2,$3)",array($UserID,$CEMID,$Permissions))){
                $this->Changed=true;
                return true;
            }else{
                $this->setError('Cannot update ACL table');
                return false;
            }
        }
    }

    /**
     * Removes all permissions from a user on a particular componenent
     *
     * @param int $UserID
     * @param int $CEMID
     * @return bool
     */
    function removeAllPermissions($UserID, $CEMID){
        return (bool)db_query_params("DELETE FROM cem_object_git_acl WHERE user_id=$1 AND cem_id=$2",array($UserID,$CEMID));
    }

    /**
     * Gets a user's permission on a particular component
     * Returns an array of the following format:
     * array(
     *      'read'   => true|false
     *      'write'  => true|false
     *      'rewind' => true|false
     *  )
     *
     * @static
     * @param int $UserID
     * @param int $CEMID
     * @return array|bool
     */
    static function getUserPermissions($UserID, $CEMID){
        if ($Result=db_query_params("SELECT permissions FROM cem_object_git_acl WHERE user_id=$1 AND cem_id=$2",array($UserID,$CEMID))){
            $Perm=db_result($Result,0,'permissions');
            return array(
                'read'      => strstr($Perm,'R')?true:false,
                'write'     => strstr($Perm,'W')?true:false,
                'rewind'    => strstr($Perm,'+')?true:false
            );
        }else{
            return false;
        }
    }

    /**
     * Exports the config file to a specific location
     */
    function exportFile(){
        $this->loadSettings();

        $AccessControl=array();

        foreach($this->Settings as $i){
            if (!isset($AccessControl[$i['cem_id']]))
                $AccessControl[$i['cem_id']]=array();

            $AccessControl[$i['cem_id']][$i['user_id']]=$i['permissions'];
        }

        print_r($AccessControl);

        $Text="repo    gitolite-admin\n    RW+ = git-admin\n\n";
        $Text.="repo    testing\n    RW+ = @all\n\n";

        foreach($AccessControl as $cemid=>$access_conrol){
            $CEM=new CEM($cemid);
            $Group=group_get_object($CEM->getGroupID());
            $Text.="repo    ".$Group->getUnixName().".".$CEM->getUnixName()."-".$CEM->getID()."\n";

            foreach($access_conrol as $userid=>$perm){
                $User=user_get_object($userid);
                $Text.="    ".$perm." = ".$User->getUnixName()."\n";
            }

            $Text.="\n";
        }

        //return $Text;
        $File='/tmp/gitolite/git_acl_settings.txt';

        unlink($File);
        file_put_contents($File,$Text);
    }
}
?>
