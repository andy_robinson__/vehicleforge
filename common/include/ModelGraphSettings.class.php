<?php

class ModelGraphSettings extends Error {
	private $ID=null, $Data=null;
	function __construct($ID=null){
		$this->ID=$ID;
	}
	
	function fetch_data(){
		if ($this->ID!==null && $this->Data===null){
			$Result=db_query_params("SELECT * FROM model_graph_settings WHERE model_id=$1",array($this->getID()));
			$this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
			return true;
		}
	}
	
	 static function createEntry($ModelID, $GraphSettings){
	
		if (!is_int($ModelID) || !is_string($GraphSettings)) {
			$this->setError('Check parameters in ModelGraphSettings::create');
			throw new Exception('Check parameters in ModelGraphSettings::create');
			return false;					
		}
		$Result = array();
		$id = null;
		db_begin();
		$Result=db_query_params("INSERT INTO model_graph_settings(model_id, graph_settings) VALUES($1, $2)",array($ModelID, $GraphSettings));
		if (!$Result){
			$this->setError('Insert Failed: '. db_error());
			db_rollback();
			return false;
		} else {
			$id = db_insertid($Result,'model_graph_settings','model_id');
			if (!$id) {
				$this->setError('Could Not Get Model_ID: ' .db_error());
				db_rollback();
				return false;
			}
			db_commit();
			return true;
		}
	}
	
	static function entryExists($ModelID) {
		if (!is_int($ModelID)) {
			$this->setError('ModelID needs to be an Integer');
			throw new Exception('ModelID needs to be an Integer');
			return false;
		}
		$Result=db_query_params("SELECT * FROM model_graph_settings WHERE model_id=$1",array($ModelID));
		$id = (int)db_result($Result,0,'model_id');
		if ($id > 0) {
			// Entry exists so return true
			return true;
		}
		
		return false;
		
	}
	
	// Success == true
	// Fail == false
	static function updateEntry ($ModelID, $GraphSettings) {
		if (!is_int($ModelID) || !is_string($GraphSettings)) {
			$this->setError('Check parameters in ModelGraphSettings::updateEntry');
			throw new Exception('Check parameters in ModelGraphSettings::updateEntry');
			return false;
		}
		
		db_begin();
		$result = db_query_params("UPDATE model_graph_settings SET graph_settings=$1 WHERE model_id=$2",array($GraphSettings,$ModelID));
		db_commit();
		return true;
	}
	
	
	function getData(){
		return $this->Data;	
		}
	
	function getID(){
		return $this->ID;
	}
	
	function getModelID() {
		return $this->Data['model_id'];
	}
	
	function getGraphSettings() {
		return $this->Data['graph_settings'];
	}
}

?>