<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 1:33 PM 12/1/11
 */

class SiteActivity  extends Error{
	private static $_instance;

	protected static $Types=array(
                        1   =>  'user_profile_comment', //$RefID=home_comments.comment_id
						2   =>  'project_home_comment', //$RefID=home_comments.comment_id
						3   =>  'project_add_member',
						4   =>  'project_news_post',//$RefID=news_bytes.id
						5   =>  'project_forum_post', //$RefID=forum.msg_id
						6   =>  'project_gallery_addition',
						7   =>  'project_bug_addition', //$RefID=artifact.artifact_id
						8   =>  'project_task_addition', //$RefID=project_task.project_task_id, $AssignedToID=project_assigned_to.assigned_to_id
						9   =>  'project_task_update', //$RefID=project_task.project_task_id, $AssignedToID=project_assigned_to.assigned_to_id
						10  =>  'project_create',//$RefID=groups.group_id
						11  =>  'project_bug_update',//$RefID=artifact.artifact_id
						12  =>  'project_bug_closed',//$RefID=artifact.artifact_id
                        13  =>  'project_component_add'//$RefID=cem_object.cem_id
	);
	protected $Me=null,
	$AllowedToReply=array(1,2,3,4,6);

	function __construct($MyUserObject=null){
		if ($MyUserObject)
			$this->Me=$MyUserObject;
	}

	static function getInstance(){
		if (isset(self::$_instance))
			return self::$_instance;

		$Class=__CLASS__;
		self::$_instance=new $Class;
		return self::$_instance;
	}

    protected function generateActivityJS($Type, $ObjectID, $TargetElementID, $Timing=10000){
        if ($Type==1 || $Type=="group"){
            $UpdateURL="/project/ajax/update_activity.php";
            $GetURL="/project/ajax/get_activity.php";
        }else if($Type==2 || $Type=="user"){
            $UpdateURL="ajax/update_subs.php";
            $GetURL="ajax/get_sub_data.php";
        }else{
            return false;
        }

        return '
var updateInterval;

window.onblur=function(){
    clearInterval(updateInterval);
};
window.onfocus=function(){
    updateInterval=setInterval(updateActivity, '.$Timing.');
};

function updateActivity(){
    $.getJSON("'.$UpdateURL.'",{object_id:'.$ObjectID.'},function(msg){
        for(var i in msg){
            if ($("#a"+msg[i]).length==0){
                $.getJSON("'.$GetURL.'",{aid:msg[i]},function(msg){
                    var $data=$(\'<div class="hidden sub">\'+msg+\'</div>\');
                    $("'.$TargetElementID.'").prepend($data);
                    $(".hidden.sub").slideDown(function(){
                        $("#a"+msg[i]).unwrap();
                    });
                });
            }
        }
    });
}';
    }

	/**
	 * getType() - Returns the ID associated with a type in the $Types array
	 * @param string|int $Type
	 * @return bool|int
	 */
	static function getType($Type){
		if (is_int($Type)){
			return $Type;
		}elseif(is_string($Type)){
			$Types=array_flip(self::$Types);
            print_r($Types);

            if (in_array($Type, array_keys($Types))){
                return $Types[$Type];
            }else{
                return false;
            }
		}

		return false;
	}

	/**
	 * getSingle() - Gets a single piece of activity data
	 * @param int $ActivityID
	 * @return array
	 */
	static function getSingle($ActivityID){
		$Result=db_query_params("SELECT * FROM site_activity WHERE activity_id=$1",array($ActivityID));
		return db_fetch_array($Result,null,PGSQL_ASSOC);
	}

    /**
     * getByRefID - Returns all activity_id's from a reference ID
     * Useful for deleting activity
     *
     * @static
     * @param int $RefID
     * @return array
     */
    static function getByRefID($RefID){
        $Return=array();
        $Result=db_query_params("SELECT activity_id FROM site_activity WHERE ref_id=$1",array($RefID));
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=$Row['activity_id'];

        return $Return;
    }

    /**
     * deleteByRefID - Deletes activity referencing $RefID
     *
     * @static
     * @param $RefID
     */
    static function deleteByRefID($RefID){
        $Activities=SiteActivity::getByRefID($RefID);
        foreach($Activities as $i)
            SiteActivity::delete($i);
    }

	/**
	 * create() - Creates a new row in the activity table
	 * @param int $CreatedByID
	 * @param int|string $ActivityType
	 * @param int $ObjectID
	 * @param int $RefID
	 * @param null|int $AssignedToID
	 * @return bool|int
	 */
	static function create($CreatedByID,$ActivityType,$ObjectID,$RefID=null,$AssignedToID=null){
            $Type=self::getType($ActivityType);
			if ($Result=db_query_params("INSERT INTO site_activity(time_posted,user_id,type_id,object_id,ref_id,assigned_to_id) VALUES($1,$2,$3,$4,$5,$6)",array(time(),$CreatedByID,$Type,$ObjectID,$RefID,$AssignedToID))){
				return db_insertid($Result,'site_activity','activity_id');
			}else{
				return false;
			}
	}

	/**
	 * activityHeader() - Gets the activity content header depending on the action taken
	 * @param array $Data
	 * @return string
	 */
	protected function activityHeader($Data){
		$On='';

        if ($Data['type_id']==1){
            //Comment on user profile page
			$User=user_get_object($Data['user_id']);
			$Image=$User->getImageHTML(50,50);
			$Author='<a href="/users/'.$User->getUnixName().'">'.$User->getRealName().'</a>';

			if ($Data['object_id']!=user_getid() && !isset($GLOBALS['ONUSERPAGE'])){
				if ($Data['object_id']==$Data['user_id']){
					$On='on their profile';
				}else{
					$OnUser=user_get_object($Data['object_id']);
					$On='on <a href="/users/'.$OnUser->getUnixName().'">'.$OnUser->getRealName().'</a>';
				}
			}else{
				$On='';
			}
		}else{
            //Comment on project page
			$Group=group_get_object($Data['object_id']);

            if ($Data['type_id']==2){
				//User made a comment
				$User=user_get_object($Data['user_id']);
				$Author='<a href="/users/'.$User->getUnixName().'">'.$User->getRealName().'</a>';
				$Image=$User->getImageHTML(50,50);
			}else{
				//Project generated message
				$Author='<a href="/projects/'.$Group->getUnixName().'">'.$Group->getPublicName().'</a>';
				$Image=$Group->getImageHTML(50,50);

			}

			if (!isset($GLOBALS['ONPROJECTPAGE']))
				$On=' on <a href="/projects/'.$Group->getUnixName().'">'.$Group->getPublicName().'</a>';
		}



        switch ($Data['type_id']){
			case 3:
				//New member
				break;

			case 4:
				//News post
				$Result=db_query_params("SELECT * FROM forum_group_list WHERE group_forum_id=$1",array($Data['ref_id']));
				$News=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/forum/forum.php?forum_id='.$News['group_forum_id'].'">News post</a>';
				break;

			case 5:
				//Form post
				break;

			case 6:
				//Gallery
				break;

			case 7:
				//New bug
				$Result=db_query_params("SELECT artifact_id, group_artifact_id FROM artifact WHERE artifact_id=$1",array($Data['ref_id']));
				$Bug=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/tracker/index.php?func=detail&aid='.$Bug['artifact_id'].'&group_id='.$Data['object_id'].'&atid='.$Bug['group_artifact_id'].'">New artifact</a>';
				break;

			case 8:
				//New task
				$Result=db_query_params("SELECT * FROM project_task WHERE project_task_id=$1",array($Data['ref_id']));
				$Task=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/pm/task.php?func=detailtask&project_task_id='.$Task['project_task_id'].'&group_id='.$Data['object_id'].'&group_project_id='.$Task['group_project_id'].'">New task assigned to you</a>';
				break;

			case 9:
				//Task update
				$Result=db_query_params("SELECT project_task_id,group_project_id FROM project_task WHERE project_task_id=$1",array($Data['ref_id']));
				$Task=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/pm/task.php?func=detailtask&project_task_id='.$Task['project_task_id'].'&group_id='.$Data['object_id'].'&group_project_id='.$Task['group_project_id'].'">Task updated</a>';
				break;

			case 10:
				//New project
				break;

			case 11:
				//New bug
				$Result=db_query_params("SELECT artifact_id, group_artifact_id FROM artifact WHERE artifact_id=$1",array($Data['ref_id']));
				$Bug=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/tracker/index.php?func=detail&aid='.$Bug['artifact_id'].'&group_id='.$Data['object_id'].'&atid='.$Bug['group_artifact_id'].'">Artifact Updated</a>';
				break;

			case 12:
				//Closed bug
				$Result=db_query_params("SELECT artifact_id, group_artifact_id FROM artifact WHERE artifact_id=$1",array($Data['ref_id']));
				$Bug=db_fetch_array($Result,null,PGSQL_ASSOC);
				$On=': <a href="/tracker/index.php?func=detail&aid='.$Bug['artifact_id'].'&group_id='.$Data['object_id'].'&atid='.$Bug['group_artifact_id'].'">Artifact Closed</a>';
				break;

            case 13:
                $CEM=new CEM($Data['ref_id']);
                $On=': New Component <a href="/components/?group_id='.$Data['object_id'].'&cid='.$Data['ref_id'].'">'.$CEM->getName().'</a>';
                break;

            default:
		}

		return '<div id="a'.$Data['activity_id'].'">
		<div class="feed clearfix">
			<div class="feed-image">'.$Image.'</div>
			<div class="feed-content">
				<time class="date" data-init="'.$Data['time_posted'].'">
					'.date('M d, Y',$Data['time_posted']).' at '.date('g:ia',$Data['time_posted']).'
				</time>
				<div class="author">'.$Author.' '.$On.'</div>
				<div class="message">
					<p>';
	}

	/**
	 * activityFooter() - Gets the activity content footer
	 * @param array $Data
	 * @return string
	 */
	protected function activityFooter($Data){
		$Return='</p></div>';
        if ($Data['user_id']==user_getid() && ($Data['type_id']==1 || $Data['type_id']==2))
            $Return.='<div class="comment_actions"><a href="javascript:void(0)" class="a_edit">Edit</a> | <a href="javascript:void(0)" class="a_delete">Delete</a></div>';
        $Return.='</div></div>';
		return $Return;
	}

	/**
	 * delete() - Deletes an activity and the data associated with it
	 * Only applies to home_comments table as the other events are deleted in their own function/scripts
	 * @param int $ActivityID
	 * @return bool
	 */
	static function delete($ActivityID){
		//TODO: Check if the requesting user has the permission to delete this activity

        db_query_params("DELETE FROM site_activity WHERE activity_id=$1",array($ActivityID));
        return true;

		$Result=db_query_params("SELECT * FROM site_activity WHERE activity_id=$1",array($ActivityID));
		$Activity=db_fetch_array($Result);

		//db_begin();
		switch($Activity['type_id']){
			case 1:
			case 2:
				//Delete from home_comments
				/*if (db_query_params("DELETE FROM home_comments WHERE comment_id=$1",array($Activity['ref_id']))){
					if (db_query_params("DELETE FROM site_activity WHERE activity_id=$1",array($ActivityID))){
						if (db_query_params("DELETE FROM site_activity_comments WHERE site_activity_id=$1",array($ActivityID))){
							db_commit();
							return true;
						}
					}
				}*/
				break;

			case 5:

				break;
		}



		//If the code has made it to this point, one of the above queries has failed
		db_rollback();
		return false;
	}

	function getSiteActivityData($Limit=5){
		$Result=db_query_params("SELECT * FROM site_activity ORDER BY time_posted DESC LIMIT $1",array($Limit));
		return util_result_columns_to_array($Result);
	}

	function getSiteActivityContent($Limit=5){
		$Return='';
		$Data=$this->getSiteActivityData($Limit);

		foreach($Data as $i){
			$Return.=$this->formatData($i);
		}

		return $Return;
	}

	/**
	 * formatData() - Formats the data correctly to display on a feed
	 * @param array $Data
	 * @return bool|string
	 */
	function formatData($Data){
		//$User=user_get_object($Data['user_id']);
		$Return=$this->activityHeader($Data);

        if ($Data['type_id']==1){
            $Object=user_get_object($Data['object_id']);
        }elseif($Data['type_id']<=14){
            $Object=group_get_object($Data['object_id']);
        }else{
            echo 'Unknown type ID in SiteActivity';
        }

		switch($Data['type_id']){
			case 2:
				//$Return.='<a href="/users/'.$User->getUnixName().'">'.$User->getRealName().'</a>';
				//$Return.='on <a href="/projects/'.$Object->getUnixName().'">'.$Object->getPublicName().'</a>';
				break;

			case 3:
				$Member=user_get_object($Data['ref_id']);
				$Return.='New member: <a href="/users/'.$Member->getUnixName().'">'.$Member->getRealName().'</a>';
				break;

			case 5:
				$Result=db_query_params("SELECT * FROM forum WHERE msg_id=$1",array($Data['ref_id']));
				$Message=db_fetch_array($Result,null,PGSQL_ASSOC);
				$Return.='Post in '.$Message['subject'];
				break;

			case 6:
				$Return.='Project new image';
				break;
		}


		//Get activity content
		switch ($Data['type_id']){
			case 1:
				if ($Result=db_query_params("SELECT * FROM home_comments WHERE type_id=$1 AND comment_id=$2",array(1,$Data['ref_id']))){
					$Comment=db_fetch_array($Result,null,PGSQL_ASSOC);
					$Return.=$Comment['comment'];
				}else{
					$this->setError("Error fetching comment");
					return false;
				}
				break;

			case 2:
				if ($Result=db_query_params("SELECT * FROM home_comments WHERE type_id=$1 AND comment_id=$2",array(2,$Data['ref_id']))){
					$Comment=db_fetch_array($Result,null,PGSQL_ASSOC);
					$Return.=$Comment['comment'];
				}else{
					$this->setError("Error fetching comment");
					return false;
				}
				break;

			case 3:
				$Return.='Please welcome your new project associate!';
				break;

			case 4:
				$Result=db_query_params("SELECT * FROM forum_group_list WHERE group_forum_id=$1",array($Data['ref_id']));
				$News=db_fetch_array($Result,null,PGSQL_ASSOC);
				$Return.=$News['description'];
				break;

			case 5:
				$Return.=$Message['body'];
				break;

			case 7:
			case 11:
			case 12:
				$Result=db_query_params("SELECT details FROM artifact WHERE artifact_id=$1",array($Data['ref_id']));
				$Bug=db_fetch_array($Result,null,PGSQL_ASSOC);
				$Return.=$Bug['details'];
				break;

			case 8:
			case 9:
				$Result=db_query_params("SELECT details FROM project_task WHERE project_task_id=$1",array($Data['ref_id']));
				$Task=db_fetch_array($Result,null,PGSQL_ASSOC);
				$Return.=$Task['details'];
				break;

			case 10:
				$Return.='Project has been created';
				break;

            case 13:
            case 14:
                $Return.='';
                break;

			default:
				$Return.='Unfinished activity for ID='.$Data['type_id'];
		}

		$Return.=$this->activityFooter($Data);

		$Replies=$this->getRepliesData($Data['activity_id']);
		foreach($Replies as $i){
			$Return.=$this->formatReply($i);
		}

		$Return.=$this->getReplyFunction($Data);

		$Return.='<hr />
		</div><!--end a#-->';

		return $Return;
	}

	/**
	 * submitReply() - Replies to a single activity
	 * @param int $ActivityID
	 * @param string $Reply
	 * @return bool|int
	 */
	function submitReply($ActivityID,$Reply){
		if ($Result=db_query_params("INSERT INTO site_activity_comments(site_activity_id,user_id,time_posted,comment) VALUES($1,$2,$3,$4)",array($ActivityID,user_getid(),time(),$Reply))){
			return db_insertid($Result,'site_activity_comments','activity_comment_id');
		}else{
			return false;
		}
	}

	/**
	 * getRepliesData() - Get all replies associated with an activity action
	 * @param int $ActivityID
	 * @return array
	 */
	function getRepliesData($ActivityID){
		$Return=array();
		$Result=db_query_params("SELECT * FROM site_activity_comments WHERE site_activity_id=$1 ORDER BY time_posted ASC",array($ActivityID));
		while($Row=db_fetch_array($Result)){
			$Return[]=$Row;
		}

		return $Return;
	}

	/**
	 * formatReply() - Formats a reply for display
	 * @param array $Data
	 * @return string
	 */
	function formatReply($Data){
		$User=user_get_object($Data['user_id']);

		return '<div class="feed-nested">
			<div class="feed-image">
				'.$User->getImageHTML(30,30,array('class'=>'user-image')).'
			</div>
			<div class="feed-content">
				<div class="author"><a href="/users/'.$User->getUnixName().'">'.$User->getRealName().'</a></div>
				<time class="date" data-init="'.$Data['time_posted'].'">'.date('M d, Y',$Data['time_posted']).' at '.date('g:ia',$Data['time_posted']).'</time>
				<div class="message">
					<p>'.$Data['comment'].'</p>
				</div>
			</div>
		</div>';
	}

	/**
	 * getReplyFunction - Displays a textarea/reply button to activity that can be replied to
	 * @param $Data
	 * @return string
	 */
	function getReplyFunction($Data){
		$Return='';
		if (in_array($Data['type_id'],$this->AllowedToReply)){
			$Return.='<div class="feed-content hidden comment_panel">
					<div class="message">
						<textarea class="reply_textarea collapsed" autocomplete="off" placeholder="Enter a comment" rows="3" cols="50"></textarea>
					</div>
				</div>
				<div class="feed-comment clearfix">
					<input type="button" class="cancel_button btn small hidden pull-right" value="Cancel" />
					<input type="button" class="reply_button btn small pull-right" value="Reply" />
				</div>';
		}

		return $Return;
	}
}
?>
