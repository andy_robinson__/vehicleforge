<?php
class PubKey    {
    private $ID, $Data=null;

    public function __construct($ID=null, $Result=null){
        $this->Data=$Result;
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data==null){
            $Result=db_query_params("SELECT * FROM user_pubkeys WHERE user_pubkey_id=$1", array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the ID of the pubkey
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the key
     *
     * @return string
     */
    public function getKey(){
        $this->fetchData();
        return $this->Data['pubkey'];
    }

    /**
     * Gets the user assigned title for this key
     *
     * @return string
     */
    public function getTitle(){
        $this->fetchData();
        return $this->Data['title'];
    }

    public function getUserID(){
        $this->fetchData();
        return $this->Data['user_id'];
    }

    static public function getByUserID($UserID){
        if ($Result=db_query_params("SELECT * FROM user_pubkeys WHERE user_id=$1",array($UserID))){
            $Return=array();
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=new PubKey($Row['user_pubkey_id'], $Row);

            return $Return;
        }

        return false;
    }

    static function addKey($UserID, $Key, $Title){
      if (file_put_contents('/tmp/gitolite/keys/'.user_get_object($UserID)->getUnixName().'.pub',$Key))
            return db_query_params("INSERT INTO user_pubkeys(title,pubkey,user_id) VALUES($1,$2,$3)",array($Title,$Key,$UserID));

        return false;
    }

    static public function removeKey($KeyID){
        $Key=new PubKey($KeyID);
        if (unlink('/tmp/gitolite/keys/'.user_get_object($Key->getUserID())->getUnixName().'.pub')){
            return db_query_params("DELETE FROM user_pubkeys WHERE user_pubkey_id=$1",array($KeyID));
        }

        return false;
    }
}
?>