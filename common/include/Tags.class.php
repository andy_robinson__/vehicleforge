<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:44 AM 12/29/11
 */

class Tags extends Error    {
	private $TagID, $Data;

	function __construct($TagID=null){
		$this->TagID=$TagID;

		if ($TagID){
			$Result=db_query_params("SELECT * FROM project_tags WHERE tag_id=$1",array($TagID));
			$this->Data=db_fetch_array($Result);
		}
	}

	function getName(){
		return $this->Data['name'];
	}

	function exists($TagName){
		$Result=db_query_params("SELECT tag_id FROM project_tags WHERE tag_name=$1",array($TagName));
		return db_numrows($Result);
	}

	function getIdByName($TagName){
		if (($Result=db_query_params("SELECT tag_id FROM project_tags WHERE tag_name=$1",array($TagName)))==false){
			$this->setError(_("Cannot find a tag with a name of ".$TagName));
			return false;
		}

		$Data=db_fetch_array($Result);
		return $Data['tag_id'];
	}

	function create($TagName){
		if (($Result=db_query_params("INSERT INTO project_tags(tag_name) VALUES($1)",array($TagName)))==false){
			$this->setError(_("Unable to create new tag"));
			return false;
		}

		return true;
	}
}

class TagJoin extends Error {
	private $TagJoinID, $Data;

	function __construct($TagJoinID=null){
		$this->TagJoinID=$TagJoinID;

		if ($TagJoinID){
			$Result=db_query_params("SELECT * FROM project_tags_join WHERE project_tag_join_id=$1",array($TagJoinID));
			$this->Data=db_fetch_array($Result);
		}
	}

	function create($GroupID,$TagID){
		return db_query_params("INSERT INTO project_tags_join(group_id,tag_id) VALUES($1,$2)",array($GroupID,$TagID));
	}
}

?>