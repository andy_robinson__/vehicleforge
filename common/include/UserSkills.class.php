<?php
class UserSkill    {
    private $ID, $Data=null;

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM user_skills WHERE user_skill_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    public function getID(){
        return $this->ID;
    }

    /**
     * @static
     * @param $UserID
     * @param $SkillName
     * @param $Keywords
     * @return bool|int
     */
    static function create($UserID, $SkillName, $Keywords){
        if ($Check=self::getBySkillName($SkillName, $UserID)){
            //Already exists
            return true;
        }else{
            db_begin();
            if($Result=db_query_params("INSERT INTO user_skills(user_id, user_skill_name) VALUES($1, $2)",array($UserID, $SkillName))){
                $SkillID=(int)db_insertid($Result,'user_skills','user_skill_id');

                $KeywordIDs=array();
                foreach($Keywords as $i){
                    if ($KeywordIDs[]=SkillKeyword::create($i)){

                    }else{
                        db_rollback();
                        return false;
                    }
                }

                foreach($KeywordIDs as $i){
                    if (!db_query_params("INSERT INTO user_skills_keywords_join(user_skill_id, skill_keyword_id) VALUES($1, $2)",array($SkillID, $i))){
                        db_rollback();
                        return false;
                    }
                }

                db_commit();

                return $SkillID;
            }else{
                return false;
            }
        }
    }

    /**
     * Gets data based on a skill name
     * Returns:
     *      If nobody has the $SkillName:           false
     *      If one person has the $SkillName:       (int) user_skill_id
     *      If multiple people have the $SkillName: (array) user_skill_id
     *
     * @static
     * @param string $SkillName
     * @param null|int $UserID
     * @return array|bool|int
     */
    static function getBySkillName($SkillName, $UserID=null){
        $QB=new QueryBuilder;
        $QB->addSql("SELECT user_skill_id FROM user_skills WHERE ")
            ->addParam("user_skill_name", $SkillName);

        if ($UserID){
            $QB->addSql(" AND ")
                ->addParam("user_id", $UserID);
        }

        if($Result=db_query_params($QB->getQuery(), $QB->getParams())){
            if ($Count=db_numrows($Result)){
                if ($Count>1){
                    $Return=array();
                    while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                        $Return[]=$Row['user_skill_id'];

                    return $Return;
                }else{
                    return (int)db_result($Result,0,'user_skill_id');
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    static function getByUserID($UserID){
        if($Result=db_query_params("SELECT user_skill_id FROM user_skills WHERE user_id=$1",array($UserID))){
            $Return=array();
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=new UserSkill($Row['user_skill_id']);

            return $Return;
        }else{
            return false;
        }
    }

    public function getSkillName(){
        $this->fetchData();
        return $this->Data['user_skill_name'];
    }

    public function getUserID(){
        $this->fetchData();
        return $this->Data['user_id'];
    }

    public function getKeywords(){
        if($Result=db_query_params("SELECT skill_keyword_id FROM user_skills_keywords_join WHERE user_skill_id=$1",array($this->getID()))){
            $Return=array();
            while($Row=db_fetch_array($Return,null,PGSQL_ASSOC))
                $Return[]=new SkillKeyword($Row['skill_keyword_id']);

            return $Return;
        }else{
            return false;
        }
    }
}
?>