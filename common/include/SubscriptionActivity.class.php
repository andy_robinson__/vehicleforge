<?php
class SubscriptionActivity extends SiteActivity {
	private $UserID;

	function __construct($UserID){
		$this->UserID=$UserID;
	}

    /**
     * getData() - Retrieves all activity associated with a particular user
     * Retrieves:
     *      Any comments on my page
     *      Project things assigned to me
     *      Projects info from project I am subscribed to
     * @param bool $AJAX
     * @return array|bool
     */
	function getData($AJAX=false){
		$QB=new QueryBuilder;
        if ($AJAX){
            $QB->addSql('SELECT activity_id FROM site_activity WHERE (');
        }else{
            $QB->addSql('SELECT * FROM site_activity WHERE (');
        }
		$QB->addParam('type_id',1)
			->addSql(' AND ')
			->addParam('object_id',$this->UserID)
            ->addSql(')');

        $Result=db_query_params("SELECT * FROM channel_subscriptions WHERE user_id=$1",array($this->UserID));
        if (db_numrows($Result)){
            $Users=array();
            $Groups=array();
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
                if ($Row['type_id']==1)
                    $Users[]=$Row['ref_id'];
                elseif ($Row['type_id']==2)
                    $Groups[]=$Row['ref_id'];
                else
                    $this->setError('Unknown type_id in subscriptions');
            }

            if ($Users){
                //Track posts on user's profile page
                //Track activity made by user
                $QB->addSql(' OR (')
                    ->addParamArray('type_id',array(1,2,3,4,5,6,7,8,9,10,11,12,13,14),' OR ')
                    ->addSql(' AND ')
                    ->addParamArray('object_id',$Users,' OR ')
                    ->addSql(')');
            }

            if ($Groups){
                //Track activity made on a project
                $QB->addSql(' OR (')
                    ->addParamArray('type_id',array(1,2,3,4,5,6,7,8,9,10,11,12,13,14),' OR ')
                    ->addSql(' AND ')
                    ->addParamArray('object_id',$Groups,' OR ')
                    ->addSql(')');
            }
        }

        $QB->addSql(' OR ')
            ->addParam('assigned_to_id',$this->UserID)
            ->addSql(' ORDER BY time_posted DESC')
            ->addSql(' LIMIT 10');


		/*
		$ParamCount=0;
		$Params=array();

		$QueryString="SELECT * FROM site_activity WHERE ((";

		//Track activity on own page
		$NeededTypes=array(1);
		foreach($NeededTypes as $i){
			$ParamCount++;
			$Params[]=$i;
			$QueryString.="type_id=$".$ParamCount." OR ";
		}

		$ParamCount++;
		$Params[]=$this->UserID;
		$QueryString=substr($QueryString,0,strlen($QueryString)-4).' AND object_id=$'.$ParamCount.')';

        $Result=db_query_params("SELECT * FROM channel_subscriptions WHERE user_id=$1",array($this->UserID));
        if (db_numrows($Result)){
            $QueryString.=' OR ((';

            //Track activity on projects and users that user is subscribed to
            //Does not track bugs or tasks NOT assigned to user
            $NeededTypes=array(1,2,3,4,5,6,1,2,9,10);
            foreach($NeededTypes as $i){
                $ParamCount++;
                $Params[]=$i;
                $QueryString.="type_id=".'$'.$ParamCount." OR ";
            }

            $QueryString=substr($QueryString,0,strlen($QueryString)-4).") AND (";

            while($Row=db_fetch_array($Result)){
                $ParamCount++;
                $Params[]=$Row['ref_id'];
                $QueryString.="object_id=$".$ParamCount." OR ";
            }

            $QueryString=substr($QueryString,0,strlen($QueryString)-4).'))';
        }


		//Track activity on project items that can be assigned to a user
		$ParamCount++;
		$Params[]=$this->UserID;
		$QueryString.=" OR assigned_to_id=$".$ParamCount." ";

		//Add in a minimum time limit
		if ($StartTime){
			$ParamCount++;
			$Params[]=$StartTime;
			$QueryString.="AND time_posted>$".$ParamCount;
		}

		$QueryString.=") ORDER BY time_posted DESC";*/
		//echo $QB->getQueryVals();
        //echo $QueryString;

        $Return=array();

		if ($Result=db_query_params($QB->getQuery(),$QB->getParams())){
            if ($AJAX){
                while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                    $Return[]=(int)$Row['activity_id'];
            }else{
			    $Return=util_result_columns_to_array($Result);
            }

			//print_r($Return);

			return $Return;
		}else{
			$this->setError("Unable to query site activity");
			return false;
		}
	}

    /**
     * getContent() - Gets all activity associated with a user and displays it
     * @return void
     */
	function getContent(){
		$Data=$this->getData();
		if (count($Data)){
			foreach($Data as $i)
				echo $this->formatData($i);
		}else{
			echo '<strong>No activity, subscribe to a feed to see new activity</strong>';
		}
	}

    function generateActivityUpdateJS($ObjectID, $TargetElementID, $Timing=10000){
        return parent::generateActivityJS('user', $ObjectID, $TargetElementID, $Timing);
    }
}
?>
