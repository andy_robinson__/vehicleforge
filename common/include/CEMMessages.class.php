<?php
class CEMMessages extends Error {
	private $ID, $Data=null;

	function __construct($ID=null){
		$this->ID=$ID;
	}

	private function fetch_data(){
        if ($this->ID!==null && $this->Data===null){
            if ($Result=db_query_params("SELECT * FROM cem_runnable_messages WHERE message_id=$1", array($this->getID()))){
                $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
                return true;
            }else{
                $this->setError('Unable to fetch message data');
                return false;
            }
        }
	}

	function getID(){
		return $this->ID;
	}

	function getMessage(){
        $this->fetch_data();

		return $this->Data['message'];
	}

	static function create($Message, $RunningID){
		if ($Result=db_query_params("INSERT INTO cem_runnable_messages(message,running_id) VALUES($1,$2)", array($Message, $RunningID))){
			return db_insertid($Result,'cem_runnable_messages','message_id');
		}else{
			//self::setError('Unable to create message in DB');
			return false;
		}
	}

	/**
	 * getByRunningID - Returns all messages associated with a single run
	 * Returns an array of CEMMessage objects
	 *
	 * @static
	 * @param $RunningID
	 * @return array
	 */
	static function getByRunningID($RunningID){
		$Return=array();
		$Result=db_query_params("SELECT * FROM cem_runnable_messages WHERE running_id=$1",array($RunningID));
		while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
			$Return[]=new CEMMessages($Row['message_id']);

		return $Return;
	}
}
?>