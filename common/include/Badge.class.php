<?php
class Badge {
    private $ID, $Data=null;

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data==null){
            $Result=db_query_params("SELECT * FROM badges WHERE badge_id=$1", array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the ID of the badge or null if no ID set
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the filename for this badge
     *
     * @return string
     */
    public function getFile(){
        $this->fetchData();
        return $this->Data['filename'];
    }

    /**
     * Gets the HTML needed to display this badge
     *
     * @return string
     */
    public function getHTML(){
        if ($File=$this->getFile()){
            return '<img src="/image_store/site_images/badges/'.$File.'" class="user-badge" alt="'.$this->getTitle().'" />';
        }else{
            return '<img src="" class="user-badge" alt="'.$this->getTitle().'" />';
        }
    }

    /**
     * Gets the title for the badge
     *
     * @return string
     */
    public function getTitle(){
        $this->fetchData();
        return $this->Data['title'];
    }

    /**
     * Gets the description for the badge
     *
     * @return string
     */
    public function getDescription(){
        $this->fetchData();
        return $this->Data['description'];
    }
}
?>