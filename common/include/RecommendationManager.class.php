<?php
$RecommendationManager=new RecommendationManager;

class RecommendationManager extends Error   {
    function _set($UserID,$TypeID,$ObjectID,$Value){
        if (is_bool($Value))
            $Value=(true)?'t':'f';

        //$UserID=user_getid();

        if ($this->_hasRecommended($UserID,$ObjectID,$TypeID)){
            //Recommendation already exists - Update row
            if(!db_query_params("UPDATE recommendations SET rvalue=$1 WHERE user_id=$2 AND object_id=$3 AND type_id=$4",array($Value,$UserID,$ObjectID,$TypeID))){
                $this->setError('Recommendation manager: Unable to update');
                return false;
            }
        }else{
            //Does not exists, create new row
            if (!db_query_params("INSERT INTO recommendations(user_id,type_id,object_id,rvalue) VALUES($1,$2,$3,$4)",array($UserID,$TypeID,$ObjectID,$Value))){
                $this->setError('Recommendation manager: Unable to add');
                return false;
            }
        }
        return true;
    }

    function _hasRecommended($UserID,$ObjectID,$TypeID){
        $Result=db_query_params("SELECT * FROM recommendations WHERE type_id=$1 AND object_id=$2 AND user_id=$3",array($TypeID,$ObjectID,$UserID));
        return db_numrows($Result);
    }

    function _get($TypeID,$ObjectID,$UserID=null){
        if ($UserID){
            $Result=db_query_params("SELECT * FROM recommendations WHERE type_id=$1 AND object_id=$2 AND user_id=$3",array($TypeID,$ObjectID,$UserID));
            $Row= db_fetch_array($Result);
            return ($Row['rvalue']=='t')?true:false;
        }else{
            $Result=db_query_params("SELECT * FROM recommendations WHERE type_id=$1 AND object_id=$2",array($TypeID,$ObjectID));
            return util_result_columns_to_array($Result);
        }
    }

	function _getRec($TypeID,$ObjectID){
		$Yes=0;
		$No=0;
		$Result=db_query_params("SELECT * FROM recommendations WHERE object_id=$1 AND type_id=$2",array($ObjectID,$TypeID));
		while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
			if ($Row['rvalue']=='f'){
				$No++;
			}elseif($Row['rvalue']=='t'){
				$Yes++;
			}
		}

		return array($Yes,$No);
	}
}

class ProjectRecommendation extends RecommendationManager   {

}

class UserRecommendation extends RecommendationManager  {
    function set($UserID,$ObjectID,$Value){
        return $this->_set($UserID,1,$ObjectID,$Value);
    }

    function get($UserID,$ObjectID){
        return $this->_get(1,$ObjectID,$UserID);
    }

	function hasRecommended($UserID,$ObjectID){
		return $this->_hasRecommended($UserID,$ObjectID,1);
	}

	function getRec($ObjectID){
		return $this->_getRec(1,$ObjectID);
	}
}
?>
