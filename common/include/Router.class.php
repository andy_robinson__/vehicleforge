<?php
class Router    {
    /**
     * Defines the routes for URL's
     * URL's with more parameters need to be higher in the array, otherwise other rules will be applied first
     * and  return a wrong URL
     *
     * @var array
     */
    static private $Routes=array(
        '/\/components\/(index.php)?\?group_id=([\d|\w]+)&cid=([\d]+)/'        => '/projects/${2}/component/${3}',
        '/\/components\/(index.php)?\?group_id=([\d|\w]+)/'                    => '/projects/${2}/components'
    );

    /**
     * Changes an ugly URL (one with &'s) to a clean URL using the patterns in the $Routes variable
     *
     * @static
     * @param $URL
     * @return mixed
     */
    static function route($URL){
        foreach(self::$Routes as $pattern=>$rule){
            if (preg_match( $pattern, $URL)){
                return preg_replace($pattern, $rule, $URL);
            }
        }

        return $URL;
    }
}
?>