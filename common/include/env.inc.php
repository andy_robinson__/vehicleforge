<?php
/*
 * Sets the default required environement for FusionForge
 *
 * Some of the settings made here can be overwrite in the
 * configuration file if needed.
 * 
 */



//Necessary to autoload classes
spl_autoload_register(function($className){
    $Possibilities=array(
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'include'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'forum'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'tracker'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.'sdk-1.5.2'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.'sdk-1.5.2'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.$className.'.class.php'
    );

    foreach($Possibilities as $i){
        //echo $i.'<br>';
        if (file_exists($i)){
            require_once $i;
            return true;
        }
    }

    return false;
});


// Attempt to set up the include path, to fix problems with relative includes
$fusionforge_basedir = "/usr/share/gforge";//dirname(dirname(__DIR__)) ;

$include_path = join(PATH_SEPARATOR, 
	array("/etc/gforge/custom", "/etc/gforge", ".", 
		"$fusionforge_basedir/common", "$fusionforge_basedir/www",
		"$fusionforge_basedir/plugins", "$fusionforge_basedir",
		"$fusionforge_basedir/www/include",
		"$fusionforge_basedir/common/include",
		"$fusionforge_basedir/www/Zend",
		//"$fusionforge_basedir/image_store",
		"/usr/share/php","/usr/share/pear"));
define('GF_IMAGE_STORE',$fusionforge_basedir.'/image_store/');

// By default, the include_path is changed to include path needed by Gforge.
// If this does not work, then set defines to real path directly.
//
// In case of failure, the following defines are set:
//    $gfconfig : Directory where are the configuration files (/etc/gforge).
//    $gfcommon : Directory common of gforge (for common php classes).
//    $gfwww    : Directory www of gforge (publicly accessible files).
//    $gfplugins: Directory for plugins.
//

// Easyforge config, allow several instances of gforge based on server name.
/*if (getenv('sys_localinc')) {
	$gfcgfile = getenv('sys_localinc');
	$gfconfig = dirname($gfcgfile).'/';
} elseif (isset($_SERVER['SERVER_NAME']) && file_exists($fusionforge_basedir.'/config/'.$_SERVER['SERVER_NAME'].'/local.inc.php')) {
	$gfcgfile = $fusionforge_basedir.'/config/'.$_SERVER['SERVER_NAME'].'/local.inc.php';
	$gfconfig = $fusionforge_basedir.'/config/'.$_SERVER['SERVER_NAME'].'/';
} elseif (file_exists($fusionforge_basedir.'/config/local.inc.php')) {
	$gfcgfile = $fusionforge_basedir.'/config/local.inc.php';
	$gfconfig = $fusionforge_basedir.'/config/';
} elseif (file_exists('/etc/gforge/local.inc.php')) {
	$gfcgfile = '/etc/gforge/local.inc.php';
	$gfconfig = '/etc/gforge/';
} elseif (file_exists('/etc/gforge/local.inc')) {*/
	//THIS ONE
	/*$gfcgfile = '/etc/gforge/local.inc';
	$gfconfig = '/etc/gforge/';
    $gf_db_config='/etc/gforge/db.inc';
    define('DB_CONFIG','/etc/gforge/db.inc');*/
/*} else {
	$gfcgfile = 'local.inc';
	$gfconfig = '';
}*/

if( !ini_set('include_path', $include_path ) && !set_include_path( $include_path )) {
	$gfcommon = $fusionforge_basedir.'/common/';
	$gfwww = $fusionforge_basedir.'/www/';
	$gfplugins = $fusionforge_basedir.'/plugins/';
	$gfimagestore=$fusionforge_basedir.'/image_store/';
} else {
	$gfcommon = '';
	$gfwww = '';
	$gfplugins = '';
	$gfimagestore='';
}

?>
