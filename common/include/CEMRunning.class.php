<?php
class CEMRunning extends Error  {
	private $ID=null, $Data=null;

	function __construct($ID=null){
        $this->ID=$ID;
	}

	private function fetch_data(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM cem_runnables_running WHERE running_id=$1", array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
	}

    /**
     * getID - Returns the run_id
     *
     * @return null|int
     */
	function getID(){
		return (int)$this->ID;
	}

    /**
     * getQueueURL - Returns the queue URL for this run
     *
     * @return string
     */
	function getQueueURL(){
        $this->fetch_data();

		return $this->Data['queue_url'];
	}

    /**
     * getOwnerId - Returns a user_id that started this run
     *
     * @return int
     */
	function getOwnerID(){
        $this->fetch_data();

		return (int)$this->Data['owner_id'];
	}

    /**
     * @return int
     *
     * @deprecated
     */
	function getRunnableID(){
        $this->fetch_data();

		return false;
	}

    /**
     * Gets the time the run started
     *
     * @return int mixed
     */
    function getTimeStarted(){
        $this->fetch_data();

        return (int)$this->Data['time_started'];
    }

    /**
     * getInterfaceID - Gets the interface ID for this model
     *
     * @return int
     */
    function getInterfaceID(){
        $this->fetch_data();

        return (int)$this->Data['interface_id'];
    }

    /**
     * getStatus - Returns the run status state of the run
     *
     * @return bool
     */
	function getStatus(){
        $this->fetch_data();
        return util_parse_bool($this->Data['finished']);

		//return $this->Data['finished']=='TRUE'?true:false;
	}

    /**
     * setStatus - Sets the finished status state for a running model
     *
     * @param bool $FinishedStatus
     * @return bool
     */
	function setStatus($FinishedStatus){
        $FinishedStatus=util_convert_bool($FinishedStatus);

        if (db_query_params("UPDATE cem_runnables_running SET finished=$1 WHERE running_id=$2", array($FinishedStatus, $this->getID()))){
            return true;
        }else{
            return false;
        }
    }

    /**
     * create - Creates a new model run
     *
     * @static
     * @param int $InterfaceID
     * @param int $UserID
     * @param string $QueueURL
     * @throws InvalidArgumentException
     * @return bool|int
     */
	static function create($InterfaceID, $UserID, $QueueURL){
        if (!is_int($InterfaceID))
            throw new InvalidArgumentException('Check intID in CEMRun::create');

        if (!is_int($UserID))
            throw new InvalidArgumentException('Check userID in CEMRun::create');

        if (!is_string($QueueURL)){
            throw new InvalidArgumentException('Check queue in CEMRun::create');
        }

		if ($Result=db_query_params("INSERT INTO cem_runnables_running(interface_id, owner_id, queue_url,time_started) VALUES($1,$2,$3,$4)", array($InterfaceID, $UserID, $QueueURL, time()))){
			return (int)db_insertid($Result,'cem_runnables_running','running_id');
		}else{
			return false;
		}
	}

	/**
	 * @static
	 * @param $RunningID
	 * @return bool
	 *
	 * @deprecated - These should no longed be deleted
	 */
	static function delete($RunningID){
		/*if (db_query_params("DELETE FROM cem_runnables_running WHERE running_id=$1", array($RunningID))){
			return true;
		}else{
			self::setError('Unabled to delete running cem object');
			return false;
		}*/
	}

    /**
     * getByUser - Gets all model runs form a user
     *
     * @static
     * @param $UserID
     * @return array
     *
     * @deprecated - Now found in User.class.php
     */
	static function getByUser($UserID){
		$Return=array();
		$Result=db_query_params("SELECT * FROM cem_runnables_running WHERE owner_id=$1 AND NOT finished", array($UserID));
		while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
			$Return[]=new CEMRunning($Row['running_id']);
		}

		return false;
	}

    /**
     * Get all messages from this run
     *
     * @param bool $Sort
     * @return array of stdObjects
     */
    public function getMessages($Sort=false){
        $Messages=array();
        $Return=array();
        if ($Result=db_query_params("SELECT message FROM cem_runnable_messages WHERE running_id=$1",array($this->getID()))){
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Messages[]=$Row['message'];
        }

        if ($Sort){
            //Decode into object to get properties
            $DecodedMessages=array();
            foreach($Messages as $i)
                $DecodedMessages[]=json_decode($i);

            //Sort by timestamp
            $Messages=$this->sortMessages($DecodedMessages);

            return $Messages;
        }else{
            return $Messages;
        }
    }

    /**
     * Sorts the messages by their timestamp
     *
     * @param array $Messages
     * @return array
     */
    private function sortMessages($Messages){
        usort($Messages, array('CEMRunning','compareMessages'));
        return $Messages;
    }

    /**
     * Compares two message's timestamps
     *
     * @param $A
     * @param $B
     * @return int
     */
    private static function compareMessages($A, $B){
        if ($A->occur==$B->occur)return 0;
        return ($A->occur < $B->occur)?-1:1;
    }

    public function setViewStatus($ViewStatus){
        $ViewStatus=util_convert_bool($ViewStatus);

        return db_query_params("UPDATE cem_runnables_running SET viewed=$1 WHERE running_id=$2",array($ViewStatus, $this->getID()));
    }
}
?>