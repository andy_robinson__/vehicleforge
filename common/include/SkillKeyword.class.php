<?php
class SkillKeyword  {
    private $ID, $Data=null;

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM skill_keywords WHERE skill_keyword_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    public function getID(){
        return $this->ID;
    }

    public function getName(){
        $this->fetchData();
        return $this->Data['skill_keyword_name'];
    }

    /**
     * Creates the keyword in the database if it does not exist
     * Returns the ID of the keyword
     *
     * @static
     * @param string $Keyword
     * @return bool|int
     */
    static function create($Keyword){
        $Keyword=strtolower($Keyword);

        if ($Check=self::check($Keyword)){
            //Already exists
            return $Check;
        }else{
            //Create new
            if($Result=db_query_params("INSERT INTO skill_keywords(skill_keyword_name) VALUES($1)",array($Keyword))){
                return (int)db_insertid($Result,'skill_keywords','skill_keyword_id');
            }else{
                return false;
            }
        }
    }

    static public function check($Keyword){
        $Result=db_query_params("SELECT skill_keyword_id FROM skill_keywords WHERE skill_keyword_name=$1",array($Keyword));
        if (db_numrows($Result)){
            //Already exists
            return (int)db_result($Result,0,'skill_keyword_id');
        }else{
            return false;
        }
    }
}
?>