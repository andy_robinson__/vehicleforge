<?php
class Role2 {
    private $ID, $Data=null;

    public function __construct($ID=null, $Result=null){
        $this->Data=$Result;
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data==null){
            $Result=db_query_params("SELECT * FROM roles WHERE role_id=$1", array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the ID of the role
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the name of the role
     *
     * @return string
     */
    public function getName(){
        $this->fetchData();
        return $this->Data['name'];
    }

    /**
     * Gets the description of the role
     *
     * @return string
     */
    public function getDescription(){
        $this->fetchData();
        return $this->Data['description'];
    }

    /**
     * Gets a string key for this role
     *
     * @return string
     */
    public function getKey(){
        $this->fetchData();
        return $this->Data['key'];
    }

    /**
     * Creates a new role
     * Returns the role_id on success
     *
     * @static
     * @param string $Name
     * @param string $Key
     * @param string $Description
     * @return bool|int
     */
    static public function create($Name, $Key, $Description=''){
        if ($Result=db_query_params("INSERT INTO roles(name,description, key) VALUES($1,$2,$3)",array($Name,$Description,$Key))){
            return (int)db_insertid($Result,'roles','role_id');
        }

        return false;
    }

    /**
     * Gets an array of all roles
     *
     * @static
     * @return bool|array of Role2 objects
     */
    static public function getRoles(){
        if ($Result=db_query_params("SELECT * FROM roles")){
            $Return=array();

            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=new Role2($Row['role_id'], $Row);

            return $Return;
        }

        return false;
    }

    /**
     * Gets a role_id from a given key
     *
     * @static
     * @param string $Key
     * @return bool|int
     */
    static public function getByKey($Key){
        if ($Result=db_query_params("SELECT role_id FROM roles WHERE key=$1",array($Key))){
            return (int)db_result($Result,0,'role_id');
        }

        return false;
    }
}
?>