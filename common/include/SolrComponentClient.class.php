<?php 
require_once 'SolrClient.class.php';

class SolrComponentClient extends SolrClient {
  
  const JAVASCRIPT_INC = '/include/scripts/SolrComponentClient.js';
  
  public function __construct($solrURL, $solrPort, $solrPath) {
    parent::__construct($solrURL, $solrPort, $solrPath, 'components');
  }
  
  public function updateCoreQueryParams(&$queryParams) {
    $queryParams['qf'] = 'component_name';
    $queryParams['fq'] = 'is_public:true';
  }
  
  public function writeJavaScriptInclude() {
    echo '<script type="text/javascript" src="'.self::JAVASCRIPT_INC.'"></script>'.PHP_EOL;
  }
  
  public function writeModal() {
    echo '<!--BEGIN: Component Modal-->'.PHP_EOL;
    echo '<div class="modal hide fade out" id="componentModal">'.PHP_EOL;
    echo '  <div class="modal-header">'.PHP_EOL;
    echo '    <a class="close" data-dismiss="modal">×</a>'.PHP_EOL;
    echo '    <h2 id="component_name"></h2>'.PHP_EOL;
    echo '    <h3 style="font-weight: 500;">'.PHP_EOL;
    echo '      <span style="font-weight: 300">in</span> <a id="group_name" href="#"></a>'.PHP_EOL;
    echo '    </h3>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-body">'.PHP_EOL;
    echo '    <div class="row">'.PHP_EOL;
    echo '      <div style="margin:10px;">'.PHP_EOL;
    echo '        <img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">'.PHP_EOL;
    echo '        <p id="component_desc"></p>'.PHP_EOL;
    echo '      </div>'.PHP_EOL;
    echo '    </div>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-footer">'.PHP_EOL;
    if (session_loggedin()) {
      echo '    <a id="component_url" href="#" class="btn">View Component</a>'.PHP_EOL;
      echo '    <div class="btn-group pull-right" style="margin-left: 10px;">'.PHP_EOL;
      $projects = session_get_user()->getOwnedProjects();
      if(count($projects) > 0) {
	echo '      <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" style="width:120px;">'.PHP_EOL;
	echo '        <span id="add_to_project_status">Add to Project</span>'.PHP_EOL;
	echo '        <span class="caret"></span>'.PHP_EOL;
	echo '      </a>'.PHP_EOL;
	echo '      <ul class="dropdown-menu">'.PHP_EOL;
	foreach ($projects as $p) {
	  echo '        <li><a href="#" class="add_to_project" group_id="'.$p->getID().'">'.$p->getPublicName().'</a></li>'.PHP_EOL;
	}
      } else {
      }
      echo '      </ul>'.PHP_EOL;
      echo '    </div>'.PHP_EOL;
    } else {
      echo '    <p>You must <a href="/account/login">login</a> to use this component.</p>'.PHP_EOL;
    }
    echo '  </div>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '<!--END: Component Modal-->'.PHP_EOL;
  }
  
  

}

?>