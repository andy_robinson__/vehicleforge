<?php
class UserRole  {
    private $UserID, $GroupID, $RoleID;

    public function __construct($UserID, $GroupID, $RoleID){
        $this->UserID=$UserID;
        $this->GroupID=$GroupID;
        $this->RoleID=$RoleID;
    }


    public function getUser(){
        return user_get_object($this->UserID);
    }

    public function getRole(){
        return new Role2($this->RoleID);
    }

    public function getGroup(){
        return group_get_object($this->GroupID);
    }

    static public function create($UserID, $GroupID, $RoleID){
        return db_query_params("INSERT INTO user_roles(user_id,group_id,role_id) VALUES($1,$2,$3)",array($UserID,$GroupID,$RoleID));
    }

    static public function delete($UserID, $GroupID, $RoleID){
        return db_query_params("DELETE FROM user_roles WHERE user_id=$1 AND group_id=$2 AND role_id=$3",array($UserID,$GroupID,$RoleID));
    }
}
?>