<?php
class UserMessage   {
    private $ID, $Data=null;

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data==null){
            $Result=db_query_params("SELECT * FROM user_messages WHERE message_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the id of the message
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets received messages
     *
     * @static
     * @param int $UserID
     * @param int $Limit
     * @param bool $Read
     * @return array of UserMessage objects
     */
    static public function getReceivedMessages($UserID, $Limit=5, $Read=false){

        $QB=new QueryBuilder;
        $QB->addSql("SELECT message_id FROM user_messages WHERE (")
            ->addParam("to_user_id",$UserID)
            ->addSql(" OR ")
            ->addParam("from_user_id",$UserID)
            ->addSql(") AND ");

        if ($Read===null){
            $QB->addParamArray("read",array(util_convert_bool(true), util_convert_bool(false)),' OR ');
        }else{
            $QB->addParam("read",util_convert_bool($Read));
        }

        $QB->addSql(" AND ")
            ->addParam("parent_message_id",0)
            ->addSql(" ORDER BY time_sent DESC LIMIT ".$Limit);

        $Return=array();

        $Result=db_query_params($QB->getQuery(), $QB->getParams());
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
            $Return[]=new UserMessage($Row['message_id']);
        }
        return $Return;
    }

    /**
     * Gets how may replies to a parent message have been unread by a user
     *
     * @param int $UserID
     * @return int
     */
    public function getUnreadReplyCount($UserID){
        $Result=db_query_params("SELECT count(message_id) FROM user_messages WHERE parent_message_id=$1 AND read=$2 AND to_user_id=$3",array($this->getID(),util_convert_bool(false), $UserID));
        $Row=db_fetch_array($Result,null,PGSQL_ASSOC);
        return (int)$Row['count'];
    }

    /**
     * Gets how many unread messages a user has
     *
     * @static
     * @param int $UserID
     * @return int
     */
    static public function getUnreadReceivedMessageCount($UserID){
        $Result=db_query_params("SELECT count(message_id) FROM user_messages WHERE to_user_id=$1 AND read=$2",array($UserID, util_convert_bool(false)));
        $Count=db_fetch_array($Result,'count',PGSQL_ASSOC);
        return (int)$Count['count'];
    }

    /**
     * Sends a message to a given user
     *
     * @static
     * @param int $From
     * @param int $To
     * @param string $Subject
     * @param string $Message
     * @param int $MessageParent
     * @return bool|int
     */
    static public function compose($From, $To, $Subject, $Message, $MessageParent=0){
        if ($Result=db_query_params("INSERT INTO user_messages(parent_message_id, from_user_id, to_user_id, message_body, time_sent, subject) VALUES($1,$2,$3,$4,$5,$6)",array($MessageParent, $From, $To, $Message, time(), $Subject))){
            return (int)db_insertid($Result,'user_messages','message_id');
        }else{
            return false;
        }
    }

    /**
     * Replies to a parent message with a new message
     *
     * @param string $ReplyMessage
     * @param int $UserID
     * @return bool|int
     */
    public function reply($ReplyMessage, $UserID){
        if ($UserID!=$this->getFromID()){
            $From=$this->getToID();
            $To=$this->getFromID();
        }else{
            $From=$this->getFromID();
            $To=$this->getToID();
        }
        return self::compose($From, $To, "", $ReplyMessage, $this->getID());
    }

    /**
     * Gets the actual message body
     *
     * @return string
     */
    function getMessage(){
        $this->fetchData();
        return $this->Data['message_body'];
    }

    /**
     * Gets who this message was from
     *
     * @return int
     */
    public function getFromID(){
        $this->fetchData();
        return (int)$this->Data['from_user_id'];
    }

    /**
     * Gets when the message was sent
     *
     * @return int
     */
    public function getTime(){
        $this->fetchData();
        return (int)$this->Data['time_sent'];
    }

    /**
     * Gets the subject of the message
     *
     * @return string
     */
    public function getSubject(){
        $this->fetchData();
        return $this->Data['subject'];
    }

    /**
     * Gets who this message is sent to
     *
     * @return int
     */
    public function getToID(){
        $this->fetchData();
        return (int)$this->Data['to_user_id'];
    }

    /**
     * Gets it message has been read yet
     *
     * @return bool
     */
    public function getRead(){
        $this->fetchData();
        return util_parse_bool($this->Data['read']);
    }

    /**
     * Marks a message as read
     * Only the person who received the message can mark it as read
     *
     * @param int $UserID
     * @return bool
     */
    public function markRead($UserID){
        if (!$this->getRead() && $UserID==$this->getToID()){
            if (db_query_params("UPDATE user_messages SET read=$1 WHERE message_id=$2",array(util_convert_bool(true), $this->getID()))){
                $this->Data['read']=true;
                return true;
            }
        }

        return false;
    }

    /**
     * Gets all replies to a parent message
     *
     * @return array of UserMessage objects
     */
    public function getReplies(){
        $Return=array();
        $Result=db_query_params("SELECT message_id FROM user_messages WHERE parent_message_id=$1",array($this->getID()));
        while ($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=new UserMessage($Row['message_id']);

        return $Return;
    }

    public function getParentMessageID(){
        $this->fetchData();
        return (int)$this->Data['parent_message_id'];
    }

    public function getParentMessage(){
        $Parent=$this->getParentMessageID();
        return ($Parent==0)?$this:new UserMessage($Parent);
    }

    public function getLastMessageSent(){
        $Result=db_query_params("SELECT time_sent FROM user_messages WHERE parent_message_id=$1 ORDER BY time_sent DESC LIMIT 1",array($this->getID()));
        $Row=db_fetch_array($Result,null,PGSQL_ASSOC);
        return (int)$Row['time_sent'];
    }

    public function formatReply($UserID){
        $Return='<hr />
<div class="reply">';


        $MessageAuthor=user_get_object($this->getFromID());
        $Return.='<b>'.$MessageAuthor->getRealName().'</b>';
        if (!$this->getRead() && $this->getFromID()!=$UserID)
            $Return.=' NEW!';
        $Return.='<br />'.$this->getMessage().'
</div>';

        return $Return;
    }
}
?>
