<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:06 PM 11/1/11
 */

require_once 'config.php';

$Repo=$_REQUEST['repo'];
$File=$_REQUEST['file'];

//Gets the filename - No path
$Filename=substr($File,strrpos($File,"/")+1);

//Execute the command
//Put file in a downloadable location (/var/www/html/svn_downloads/)
$CMD="svn export file://".$SVNRoot."/".$Repo."/".$File." /var/www/html/svn_downloads/".$Filename;
exec($CMD);

//Return the download location
echo "http://".$_SERVER['SERVER_NAME'].'/svn_downloads/'.$Filename;
?>