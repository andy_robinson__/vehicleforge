<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:54 AM 10/28/11
 */

require_once 'config.php';

$Repo=$_REQUEST['repo'];
$CurrentFolder=isset($_REQUEST['folder'])?$_REQUEST['folder']:"/";

//Get all files within the current folder
$CMD="svn list file://".$SVNRoot."/".$Repo.$CurrentFolder;
$Files=array();
exec($CMD,$Files,$Ret);

$Return=array();

//Loop through the files, get revision history of each
foreach ($Files as $i){
	$Output=array();

	//Get necessary data - Data parsed using 'grep' to only get revision data
	$CMD="svn info file://".$SVNRoot."/".$Repo.$CurrentFolder.$i." | grep 'Last Changed'";
	exec($CMD,$Output);

	//Get the file type (extension) - Convert to lowercase
	$FileType=(strpos($i,"."))?strtolower(substr($i,strrpos($i,".")+1)):"";

	//Files have 2 revision attributes (revision # and revision date)
	//Folders have 3 revision attributes (revision #, revision date, revision author)
	//Check what a particular file is (file or folder) by checking the size of the $Output array is
	if (sizeof($Output)==2){
		//File
		$Author=null;
		$Revision=explode(":",$Output[0]);
		$Date=explode(" ",$Output[1]);
	}else{
		//Folder
		$Author=explode(":",$Output[0]);
		$Revision=explode(":",$Output[1]);
		$Date=explode(" ",$Output[2]);
	}

	//Put all data into a single array and push this array to the final returned array
	$Return[]=array('name'=>$i,'type'=>$FileType,'rev'=>trim($Revision[1]),'date'=>$Date[3]." ".$Date[4],'author'=>trim($Author[1]));
}

//Return a json-encoded array
echo json_encode($Return);
?>