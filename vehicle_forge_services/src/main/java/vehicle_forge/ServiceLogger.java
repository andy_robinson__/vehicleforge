package vehicle_forge;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class ServiceLogger {

	private static FileHandler logFileHandler;
	private static SimpleFormatter logFileFormatter;
	private static Logger logger;
	private static Logger rootLogger;
	private final String LOGFILE = Config.LOG_FILE;
	private static ServiceLogger serviceLoggerInstance = null;

	protected ServiceLogger() {
		try {
			this.setup();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void log (String logTag, String message) {
		if (serviceLoggerInstance == null) {
			serviceLoggerInstance = new ServiceLogger();
		} 
	    logger.info(logTag + ": " + message);	
	}
	
	private void setup() throws IOException {
	    logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	    logger.setLevel(Level.INFO);
	    logger.setUseParentHandlers(false);
	    logFileHandler = new FileHandler(LOGFILE);  
        logger.addHandler(logFileHandler);
        logFileFormatter = new SimpleFormatter();  
        logFileHandler.setFormatter(logFileFormatter);  
	}
}
