package vehicle_forge;

public class Specification {

	private final String logTag = "SERVICE SPECIFICATION";
	private String description = "";
    private final int num_inputs;
    private final int num_outputs;
    private final Usage usage_stats;
    private final Trial trial_stats;

    public Specification(String desc, int inputs, int outputs, Usage usage, Trial trial) {
    	
    	this.description = desc;
        this.num_inputs = inputs; 
        this.num_outputs = outputs;
        this.usage_stats = usage;
        this.trial_stats = trial;
    }
    
    public String getDescription(){
    	return description;
    }
    
    public int getNumInputs() {
        return num_inputs;
    }
    
    public int getNumOutputs() {
    	return num_outputs;
    }
    
    public Usage getUsageStats(){
    	return usage_stats;
    }
   
    public Trial getTrialStats(){
    	return trial_stats;
    }
    
   
}