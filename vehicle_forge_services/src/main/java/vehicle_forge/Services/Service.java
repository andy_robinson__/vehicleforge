package vehicle_forge;

public class Service {

	private final String logTag = Role.class.getName();
	
    private final int id;          
    private final String title;    
    private final String description;   
    private final String owner;
    private final String releaseDate;
    private final String tags;
    
    private Service(ServiceBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.description = builder.description;
        this.owner = builder.owner;
        this.releaseDate = builder.releaseDate;
        this.tags = builder.tags;
    }
    
    public int getId() {
        return id;
    }
    
    public String getTitle() {
    	return title;
    }
    
    public String getDescription() {
    	return description;
    }
    
    public String getOwner() {
    	return owner;
    }
    
    public String getReleaseDate() {
    	return releaseDate;
    }
    
    public String getTags() {
    	return tags;
    }
    
    //Service Builder
    public static class ServiceBuilder {
    	
    	private final int id;
    	private final String title;
    	private String description;
    	private String owner;
    	private String releaseDate;
    	private String tags;
    	
    	
    	public ServiceBuilder(int id, String title) {
    		this.id = id;
    		this.title = title;
    	}  	
    	public ServiceBuilder description(String description) {
    		this.description = description;
    		return this;
    	}
    	
    	public ServiceBuilder owner(String owner) {
    		this.owner = owner;
    		return this;
    	}
    	
    	public ServiceBuilder releaseDate(String releaseDate) {
    		this.releaseDate = releaseDate;
    		return this;
    	}
    	
    	public ServiceBuilder tags(String tags) {
    		this.tags = tags;
    		return this;
    	}
    	
    	public Service build() {
    		return new Service(this);
    	}
    }
}