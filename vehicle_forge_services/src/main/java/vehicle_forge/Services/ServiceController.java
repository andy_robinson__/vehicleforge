package vehicle_forge;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


@RestController
public class ServiceController {

	private final String logTag = ServiceController.class.getName();
	
	private ServiceDao serviceDao = new ServiceDao(); 
    @RequestMapping(value = "/services/{id}", method = RequestMethod.GET)
    public Service getService(@PathVariable("id") int id) {
    	ServiceLogger.log(logTag, "getServie, id: " + id);
    	return serviceDao.getService(id);
    }
}