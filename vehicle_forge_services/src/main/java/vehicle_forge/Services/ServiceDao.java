package vehicle_forge;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ServiceDao {

	private final String logTag = ServiceDao.class.getName();
	private ResultSet resultSet;

	public Service getService(int requestId) {
		int id;
		String title, description, query;
		
		query = "SELECT interface_id AS id, alias AS title, description  FROM dome_interfaces WHERE interface_id = " + requestId;
		
		ServiceLogger.log(logTag, "getService, id: " + requestId);
		
		try {
			resultSet = DBConnector.executeQuery(query);
			while (resultSet.next()) {
				id = resultSet.getInt("id");
				title = resultSet.getString("title");
				description = resultSet.getString("description");
				//ServiceLogger.log(logTag, "Service Name - " + resultSet.toString());
				return new 
						Service.ServiceBuilder(id, title)
						.description(description)
						.build();
			}
		} catch (SQLException e) {
			ServiceLogger.log(logTag, e.getMessage());
		}
		return null;
	}
}