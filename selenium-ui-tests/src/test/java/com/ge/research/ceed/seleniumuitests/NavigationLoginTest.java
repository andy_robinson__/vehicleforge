package com.ge.research.ceed.seleniumuitests;

import org.junit.Test;
import org.openqa.selenium.By;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

/**
 * Unit test for testing navigation while logged in to the Forge.
 */
public class NavigationLoginTest extends BaseTest {

    /**
     * After logging in, test page main Tabs and icons at the top of the site.
     */
    @Test
    public void testTopNavigationMenu() throws Exception {

        testForgeLogin();
        
        /** Main navigation tabs. */
        driver.findElement(By.linkText("Home")).click();
        assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
        
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
        driver.findElement(By.linkText("Marketplace")).click();
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/services.php"));
        
        driver.findElement(By.linkText("Projects")).click();
        assertTrue(driver.getCurrentUrl().endsWith("softwaremap/full_list.php"));
        
        /**Test Icons on the upper right hand corner. */
        
        //Test Update under tool icon
        driver.findElement(By.cssSelector("i.icon-wrench.icon-white")).click();
		driver.findElement(By.id("check_finished_models")).click();
		
        //Test All History under tool icon
		driver.findElement(By.cssSelector("i.icon-wrench.icon-white")).click();
		driver.findElement(By.linkText("All History")).click();
		assertTrue(driver.getCurrentUrl().endsWith("account/run_history.php"));
        
        //Test Inbox under email icon
		driver.findElement(By.cssSelector("i.icon-envelope.icon-white")).click();
		driver.findElement(By.linkText("Inbox")).click();
		assertTrue(driver.getCurrentUrl().endsWith("account/messages.php?action=inbox"));
		
        //Test Create Message under email icon
		driver.findElement(By.cssSelector("i.icon-envelope.icon-white")).click();
		driver.findElement(By.linkText("Create Message")).click();
		assertTrue(driver.getCurrentUrl().endsWith("account/messages.php?action=compose"));
        
        //Test home icon
		driver.findElement(By.cssSelector("i.icon-home.icon-white")).click();
		assertTrue(driver.getCurrentUrl().endsWith("my/"));
		
		/** User full name. */
		driver.findElement(By.linkText(TestUtils.USER_FULL_NAME)).click();
		driver.findElement(By.linkText("My Account")).click();
		assertTrue(driver.getCurrentUrl().endsWith("account/"));
		
		driver.findElement(By.linkText(TestUtils.USER_FULL_NAME)).click();
		driver.findElement(By.linkText("Log Out")).click();
		assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
    }
    
    /**
     * After logging in, test sub-menu in the user profile page.
     */
    @Test
    public void testUserProfileNavigationMenu() throws Exception {
    
    	testForgeLogin();
        
        // Load the user profile page
        driver.findElement(By.cssSelector("i.icon-home.icon-white")).click();
        assertTrue(driver.getCurrentUrl().endsWith("my/"));
        
        //Test Dashboard sub tab
        driver.findElement(By.linkText("Dashboard")).click();
        assertTrue(driver.getCurrentUrl().endsWith("my/"));
        
        //Test work sub-tab
        driver.findElement(By.linkText("Work")).click();
        assertTrue(driver.getCurrentUrl().endsWith("my/dashboard.php"));
        driver.findElement(By.linkText("Artifacts")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Artifacts[\\s\\S]*$"));
        driver.findElement(By.linkText("Tasks")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Tasks[\\s\\S]*$"));
        
        //Test diary sub tab    
        driver.findElement(By.linkText("Diary")).click();
        assertTrue(driver.getCurrentUrl().endsWith("my/diary.php"));
        driver.findElement(By.xpath("//td/input")).clear();
        driver.findElement(By.xpath("//td/input")).sendKeys("demo");
        driver.findElement(By.xpath("//textarea")).click();  
        driver.findElement(By.xpath("//textarea")).clear();
        driver.findElement(By.xpath("//textarea")).sendKeys("This is a demo for test!");
        driver.findElement(By.xpath("//p/input")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Item Added[\\s\\S]*$"));
    
        //Test create project sub tab
        driver.findElement(By.linkText("Create Project")).click();
        assertTrue(driver.getCurrentUrl().endsWith("register/"));

        //Test Register Server sub tab
        driver.findElement(By.linkText("Register Server")).click();
        assertTrue(driver.getCurrentUrl().endsWith("register/server.php"));
        driver.findElement(By.xpath("//div/input")).click();
        driver.findElement(By.xpath("//div/input")).clear();
        driver.findElement(By.xpath("//div/input")).sendKeys("DEMO SERVER");
        driver.findElement(By.xpath("//div[2]/div/input")).clear();
        driver.findElement(By.xpath("//div[2]/div/input")).sendKeys("www.testurlfordemo");
        driver.findElement(By.xpath("//button[1]")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Server is not valid[\\s\\S]*$"));
        
        // Click on Public Profile sub-tab. Testing this tab last because it doesn't have the above tabs.
        driver.findElement(By.linkText("Public Profile")).click();
        assertTrue(driver.getCurrentUrl().endsWith("users/" + TestUtils.CREDENTIAL_FORGE_USER));
    }
    
    
    /**
     * After logging in, test sub-menu in the user Account page.
     */
    @Test
    public void testUserAccountNavigationMenu() throws Exception {
    
    	testForgeLogin();
        
        // Load the user profile page
        driver.findElement(By.cssSelector("i.icon-home.icon-white")).click();
        assertTrue(driver.getCurrentUrl().endsWith("my/"));
        
        //Test user account
        driver.findElement(By.xpath("//div[4]/ul/li[4]/a")).click();
        driver.findElement(By.xpath("//li[4]/ul/li/a")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/"));      
   
        //Test Edit Skills
        driver.findElement(By.xpath("//div[2]/div/div/nav/ul/li[3]/a")).click();
        assertTrue(driver.getCurrentUrl().endsWith("people/editprofile.php"));
        
        //Test Edit Servers
        driver.get(baseUrl + "account/");
        driver.findElement(By.xpath("//div[2]/div/div/nav/ul/li[4]/a")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/servers.php"));
        
        //Test Edit Public Keys
        driver.get(baseUrl + "account/");
        driver.findElement(By.xpath("//div[2]/div/div/nav/ul/li[5]/a")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/publickeys.php"));
        
        //Test Public Profile
        driver.get(baseUrl + "account/");
        driver.findElement(By.linkText("Public Profile")).click();
        //assertTrue(driver.getCurrentUrl().endsWith("users/fforgeadmin"));
	//	assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Member Since[\\s\\S]*$"));
    
       
     }
     
}
