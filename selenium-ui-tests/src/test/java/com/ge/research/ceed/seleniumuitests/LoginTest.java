package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Unit test for testing logins for Forge.
 */
public class LoginTest extends BaseTest{

	@Test
    public void testPublicLoginProtectionMethod() throws Exception {

        if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
	}
    
    /**
     * Test successful logging into the actual Forge system thru the base URL. 
     */
    @Test
    public void testForgeLoginSuccess() throws Exception {
        
        // Login into the public section of the site. 
    	if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }    
    	
        // Click on Log In in the page.
        WebElement element = driver.findElement(By.id("site_login"));
        element.click();   
        
        element = driver.findElement(By.name("form_loginname"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
        
        element = driver.findElement(By.name("form_pw"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
        
        element = driver.findElement(By.className("btn"));
        element.submit();
        
        assertTrue(driver.getPageSource().indexOf("Your account does not exist.") == -1);
        assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") == -1);
        assertTrue(driver.getCurrentUrl().endsWith("my/"));
    }
    
    /**
     * Test direct logging into the actual Forge system as an INVALID username and correct password.
     */
    @Test
    public void testForgeLoginFail01() throws Exception {

        if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }

        // Click on Log In in the page.
        WebElement element = driver.findElement(By.id("site_login"));
        element.click();   
        
        element = driver.findElement(By.name("form_loginname"));
        element.click();
        element.sendKeys("totally-fake-user");
        
        element = driver.findElement(By.name("form_pw"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
        
        element = driver.findElement(By.className("btn"));
        element.submit();
        
        assertTrue(driver.getPageSource().indexOf("Your account does not exist.") != -1);
        assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") != -1);
        assertTrue(driver.getCurrentUrl().endsWith("account/login.php"));
    }

    /**
     * Test direct logging into the actual Forge system as a VALID username and INVALID password.
     *
     */
    @Test
    public void testForgeLoginFail02() throws Exception {

    	if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }

        // Click on Log In in the page.
        WebElement element = driver.findElement(By.id("site_login"));
        element.click();   
        
        element = driver.findElement(By.name("form_loginname"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
        
        element = driver.findElement(By.name("form_pw"));
        element.click();
        element.sendKeys("fake-password");
        
        element = driver.findElement(By.className("btn"));
        element.submit();
        
        assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") != -1);
        assertTrue(driver.getCurrentUrl().endsWith("account/login.php"));
    }
}
