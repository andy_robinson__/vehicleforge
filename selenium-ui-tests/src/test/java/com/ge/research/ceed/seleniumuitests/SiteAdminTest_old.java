package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


/**
 * Unit test for testing Site Admin for Forge.
 * sub-topic: User Maintenence, Global roles and permissions, News, Stats,
 * Trove Project Tree, Projects, Site Utilities.
 */
/*
 public class SiteAdminTest extends BaseTest {
 
    
   // Test User Maintenence sub-topic
   
    
    @Test
    public void testUserMaintenence() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Display Full User List/Edit Users" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Display Full User List/Edit Users")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/userlist.php"));
         
         //Test navigation of Display Users Beginning with specific letter
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("A")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/userlist.php?user_name_search=A"));
         
         //Test search users by userid, username, realname, email.
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.cssSelector("input[name=\"search\"]")).clear();
         driver.findElement(By.cssSelector("input[name=\"search\"]")).sendKeys("selenium");
         driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/search.php"));
         
         //Test navigation of link "Register a New User" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Register a New User")).click();
         assertTrue(driver.getCurrentUrl().endsWith("account/register.php"));
         
         //Test navigation of link "Pending user"
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Pending users")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/userlist.php?status=P"));
    
    }
    
    

    //Test News sub-topic within Site Admin page
   
    
    @Test
    public void testNews() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Pending news (moderation for front-page" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Pending news (moderation for front-page)")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/pending-news.php"));
    }
    
    
    
    
    // Test Stats sub-topic within Site Admin page
   
    
    @Test
    public void testStats() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Site-Wide Stats" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Site-Wide Stats")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/"));
         
         driver.findElement(By.linkText("PROJECT STATS")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/projects.php"));
         
         driver.findElement(By.linkText("SITE GRAPHS")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/graphs.php"));
         
         driver.findElement(By.linkText("OVERVIEW STATS")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/index.php"));
         
         driver.findElement(By.linkText("I18n Statistics")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/i18n.php"));
    }
 
 
 
  
    //Test Trove Project Tree sub-topic within Site Admin page
    
    
    @Test
    public void testTroveProjectTree() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Display Trove Map" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Display Trove Map")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/trove/trove_cat_list.php"));
         
         //Test navigation of link "Add to the Trove Map" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Add to the Trove Map")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/trove/trove_cat_add.php"));
         
         driver.findElement(By.cssSelector("input[name=\"form_shortname\"]")).clear();
         driver.findElement(By.cssSelector("input[name=\"form_shortname\"]")).sendKeys("democategory");
         
         driver.findElement(By.cssSelector("input[name=\"form_fullname\"]")).clear();
         driver.findElement(By.cssSelector("input[name=\"form_fullname\"]")).sendKeys("democategoryfullname");
         
         driver.findElement(By.cssSelector("input[name=\"form_description\"]")).clear();
         driver.findElement(By.cssSelector("input[name=\"form_description\"]")).sendKeys("This is a test category");
         driver.findElement(By.cssSelector("input[name=\"submit\"]")).click();
         
         assertTrue(driver.getCurrentUrl().endsWith("admin/trove/trove_cat_list.php"));
         assertTrue(driver.getPageSource().indexOf("Missing category short name or full name") == -1);
         
    }
    
    
    
    
     
    // Test Projects sub-topic within Site Admin page
  
    
    @Test
    public void testProjects() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Display Full Project List/Edit Projects" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Display Full Project List/Edit Projects")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/grouplist.php"));
        
          //Test navigation of Display Projects Beginning with specific letter
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.xpath("//div[2]/ul[2]/li[2]/a")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/grouplist.php?group_name_search=A"));
         
         //Test search Project by groupid, project Unix name, project full name
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.cssSelector("form[name=\"gpsrch\"] > input[name=\"search\"]")).clear();
         driver.findElement(By.cssSelector("form[name=\"gpsrch\"] > input[name=\"search\"]")).sendKeys("template");
         driver.findElement(By.cssSelector("form[name=\"gpsrch\"] > input[type=\"submit\"]")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/search.php"));
         
         //Test navigation of link "Register New Project" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Register New Project")).click();
         assertTrue(driver.getCurrentUrl().endsWith("register/"));
         
         //Test navigation of link "Pending projects(new project approval)"
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Pending projects (new project approval)")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/approve-pending.php"));
         
         //Test search Project with status "Pending"/"Active"/"Hold"
         driver.findElement(By.linkText("Site Admin")).click();
         new Select(driver.findElement(By.cssSelector("select[name=\"status\"]"))).selectByVisibleText("Pending (P)");
         driver.findElement(By.cssSelector("form[name=\"projectsearch\"] > input[type=\"submit\"]")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/search.php?status=P&groupsearch=1&search=%25"));
         
         //Test navigation of link "Private Projects"
         //driver.findElement(By.linkText("Site Admin")).click();
         //driver.findElement(By.linkText("Private Projects")).click();
         //assertTrue(driver.getPageSource().indexOf("Group search with criteria") == -1);
         //assertTrue(driver.getCurrentUrl().endsWith("admin/search.php?groupsearch=1&search=%&is_public=0"));
         
    }
    
    
     
    
  
   // Test Site Utilities sub-topic within Site Admin page
   
    
    @Test
    public void testSiteUtilities() throws Exception {
        
         testForgeAdminLogin();
         
         //Test navigation of link "Mail Engine for Digital Manufacturing Commons Subscribers" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Mail Engine for Digital Manufacturing Commons Subscribers")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/massmail.php"));
         
         //Test navigation of link "Digital Manufacturing CommonsSite Mailings Maintenance" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Digital Manufacturing CommonsSite Mailings Maintenance")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/unsubscribe.php"));
  
         //Test navigation of link "Add, Delete, or Edit File Types" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Add, Delete, or Edit File Types")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/edit_frs_filetype.php"));
         
         //Test navigation of link "Add, Delete, or Edit Processors" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Add, Delete, or Edit Processors")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/edit_frs_processor.php"));
         
         //Test navigation of link "Add, Delete, or Edit Themes" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Add, Delete, or Edit Themes")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/edit_theme.php"));
         
         //Test navigation of link "Last Logins" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Last Logins")).click();
         assertTrue(driver.getCurrentUrl().endsWith("stats/lastlogins.php"));
        
         //Test navigation of link "Cron Manager" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Cron Manager")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/cronman.php"));
         
          //Test navigation of link "Plugin Manager" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Plugin Manager")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/pluginman.php"));
         
         
         //Test navigation of link "Config Manager" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Config Manager")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/configman.php"));
         
         //Test navigation of link "Job / Categories Administration" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("Job / Categories Administration")).click();
         assertTrue(driver.getCurrentUrl().endsWith("people/admin/"));
         
         //Test navigation of link "PHPinfo()" 
         driver.findElement(By.linkText("Site Admin")).click();
         driver.findElement(By.linkText("PHPinfo()")).click();
         assertTrue(driver.getCurrentUrl().endsWith("admin/pi.php"));
         
    }     
 
 }
 */
 