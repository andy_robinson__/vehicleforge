package com.ge.research.ceed.seleniumuitests;

import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for trying Forge navigation without being logged in. Testing the main tabs on the home page
 */
public class NavigationNoLoginTest extends BaseTest {

    /**
     * From the Forge homepage, test clicking the top 3 main menu links.
     */
    @Test
    public void testTopNavigationMenu() throws Exception {

        if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
      
        /** Main navigation tabs. */
        driver.findElement(By.linkText("Home")).click();
        assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
        
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
        driver.findElement(By.linkText("Marketplace")).click();
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/services.php"));
        
        driver.findElement(By.linkText("Projects")).click();
        assertTrue(driver.getCurrentUrl().endsWith("softwaremap/full_list.php"));
        
        driver.findElement(By.linkText("Register")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/register2.php"));
        
        driver.findElement(By.linkText("Log In")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/login.php"));
    }
    
    
    /**
       Test User register
    **/
    @Test
    public void testRegisterUserAccount() throws Exception {
    	
    	if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
    	
    	driver.findElement(By.linkText("Register")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/register2.php"));
        
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(TestUtils.USER_EMAIL);
		
		driver.findElement(By.id("unix_name")).clear();
		driver.findElement(By.id("unix_name")).sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
		
		driver.findElement(By.id("password1")).clear();
		driver.findElement(By.id("password1")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("password2")).clear();
		driver.findElement(By.id("password2")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("firstname")).clear();
		driver.findElement(By.id("firstname")).sendKeys("Selenium");
		
		driver.findElement(By.id("lastname")).clear();
		driver.findElement(By.id("lastname")).sendKeys("Tester");
		
		driver.findElement(By.id("accept_conditions")).click();
		
		driver.findElement(By.name("submit")).click();
    }
    
    
    
    /**
       Test User register
    **/
    /**
    @Test
    public void testRegisterNewUserAccount() throws Exception {
    	
    	if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
    	
    	driver.findElement(By.linkText("Register")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/register2.php"));
        
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(TestUtils.USER_EMAIL);
        
		driver.findElement(By.id("unix_name")).clear();
		driver.findElement(By.id("unix_name")).sendKeys(TestUtils.CREDENTIAL_FORGE_USER + TestUtils.ran);
		
		driver.findElement(By.id("password1")).clear();
		driver.findElement(By.id("password1")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("password2")).clear();
		driver.findElement(By.id("password2")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("firstname")).clear();
		driver.findElement(By.id("firstname")).sendKeys("Selenium" + TestUtils.ran);
		
		driver.findElement(By.id("lastname")).clear();
		driver.findElement(By.id("lastname")).sendKeys("Tester");
		
		driver.findElement(By.id("accept_conditions")).click();
		
		driver.findElement(By.name("submit")).click();
    }
    **/
    
    /**
       Test approve all pending users
    
    @Test
    public void testapprveSelenium() throws Exception {
    
     testForgeAdminLogin();
     
     //approve selenium tester.
     driver.get(baseUrl+"admin/quickpass_pending_user.php");
     driver.findElement(By.name("approve-all")).click();    
     
     
    
    
    }
    **/
    
    
    
}