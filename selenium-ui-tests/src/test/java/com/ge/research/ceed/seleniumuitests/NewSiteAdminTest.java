package com.ge.research.ceed.seleniumuitests;


import static org.junit.Assert.*;
import org.junit.Test;
import com.thoughtworks.selenium.*;
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import java.util.concurrent.TimeUnit;


/**
 * Unit test for testing New Site Admin for Forge.
 * sub-topic: Quick Pass, Project, User, News, Site Management, Site Stats.
 */
 
 public class NewSiteAdminTest extends BaseTest {
    /**
    *@author Junrong Yan
    * Test the sub-menu of Site Admin page.
    */
    @Test
    public void testSiteAminPageNavigation() throws Exception {
        testForgeAdminLogin();
        
        //Navigate to Site Admin Page.
        driver.findElement(By.linkText("Site Admin")).click();
        //click on the Quick Pass tab after login.
        driver.findElement(By.linkText("Quick Pass")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index.php"));
        
        //click on the Project tab of Site Admin
        driver.findElement(By.linkText("Project")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/admin_project.php"));
        
        //click on the User tab of Site Admin
        driver.findElement(By.linkText("User")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/admin_user.php"));
        
        //click on the News tab of Site Admin
        driver.findElement(By.linkText("News")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/admin_news.php"));
        
        //click on the Site Management tab of Site Admin
        driver.findElement(By.linkText("Site Management")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/site_management.php"));
        
        //click on the Site Stats tab of Site Admin
        driver.findElement(By.linkText("Site Stats")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/site_stats.php"));
    
    }
    
    @Test
    public void testPendingProjectPage() throws Exception{
        testForgeAdminLogin();
        
        //Navigate to Site Admin Page.
        driver.findElement(By.linkText("Site Admin")).click();
        //click on the Quick Pass tab after login.
        driver.findElement(By.linkText("Quick Pass")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index.php"));
        assertTrue(driver.getPageSource().indexOf("Pending projects") != -1);
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
       // WebElement element = driver.findElement(By.xpath("//div/input"));
        //element.click();
       // element.sendKeys("selenium");
        driver.findElement(By.id("search_query")).click();
        driver.findElement(By.id("search_query")).clear();
        driver.findElement(By.id("search_query")).sendKeys("selenium");
        driver.findElement(By.xpath("//button")).click();
        //assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Found projects matching[\\s\\S]*$"));
        //driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        //assertEquals("selenium(status=P)", driver.findElement(By.xpath("//em")).getText());
        
        
        driver.findElement(By.linkText("Quick Pass")).click();
        driver.findElement(By.id("sort_by")).click();
        new Select(driver.findElement(By.id("sort_by"))).selectByVisibleText("Time:Oldest");
        driver.findElement(By.cssSelector("option[name=\"2\"]")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index?order_by=2"));
        
        
        driver.findElement(By.linkText("Quick Pass")).click();
        driver.findElement(By.xpath("//button[2]")).click();
        driver.findElement(By.xpath("//textarea")).click();
        driver.findElement(By.xpath("//textarea")).clear();
        driver.findElement(By.xpath("//textarea")).sendKeys("test rejected project for selenium");
        driver.findElement(By.xpath("//button[2]")).click();
        //assertTrue(driver.getPageSource().indexOf("Rejected Project") != -1);
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Rejected Project[\\s\\S]*$"));
    }
    
     @Test
    public void testPendingUserPage() throws Exception{
    
    	if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
              testPublicLoginProtection();
          }
      	
      	driver.findElement(By.linkText("Register")).click();
          assertTrue(driver.getCurrentUrl().endsWith("account/register2.php"));
          
  		driver.findElement(By.id("email")).clear();
  		driver.findElement(By.id("email")).sendKeys(TestUtils.USER_EMAIL);
          
  		driver.findElement(By.id("unix_name")).clear();
  		driver.findElement(By.id("unix_name")).sendKeys(TestUtils.CREDENTIAL_FORGE_USER + TestUtils.ran);
  		
  		driver.findElement(By.id("password1")).clear();
  		driver.findElement(By.id("password1")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
  		
  		driver.findElement(By.id("password2")).clear();
  		driver.findElement(By.id("password2")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
  		
  		driver.findElement(By.id("firstname")).clear();
  		driver.findElement(By.id("firstname")).sendKeys("Selenium" + TestUtils.ran);
  		
  		driver.findElement(By.id("lastname")).clear();
  		driver.findElement(By.id("lastname")).sendKeys("Tester");
  		
  		driver.findElement(By.id("accept_conditions")).click();
  		
  		driver.findElement(By.name("submit")).click();
    
        testForgeAdminLogin();
        
        //Navigate to Site Admin Page.
        driver.findElement(By.linkText("Site Admin")).click();
        //click on the Quick Pass tab after login.
        driver.findElement(By.linkText("Quick Pass")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index.php"));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath("//select")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Pending Users");
        driver.findElement(By.xpath("//option[2]")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_pending_user"));
 
 
 
        driver.findElement(By.xpath("//button[2]")).click();
        driver.findElement(By.xpath("//textarea")).click();
        driver.findElement(By.xpath("//textarea")).clear();
        driver.findElement(By.xpath("//textarea")).sendKeys("test rejected user for selenium");
        driver.findElement(By.xpath("//button[2]")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Rejected User[\\s\\S]*$"));
        
        }
        
        
        
        @Test
    public void testRejectedProjectPage() throws Exception{
        testForgeAdminLogin();
        
        //Navigate to Site Admin Page.
        driver.findElement(By.linkText("Site Admin")).click();
        //click on the Quick Pass tab after login.
        driver.findElement(By.linkText("Quick Pass")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index.php"));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath("//select")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Rejected Projects");
        driver.findElement(By.xpath("//option[4]")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_reject_projects"));
        
        
        
        //driver.findElement(By.xpath("//div[10]/div/select")).click();
        //driver.findElement(By.xpath("//div[10]/div/select/option[2]")).click();
       // new Select(driver.findElement(By.xpath("//div[10]/div/select"))).selectByVisibleText("active");
        //driver.findElement(By.xpath("//option[2]")).click();
        //driver.findElement(By.xpath("//form-right/button")).click();
        //assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Successfully change the status of Project[\\s\\S]*$"));
        //assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Error when change status of Project[\\s\\S]*$"));
        
        }
        
        
         @Test
    public void testRejectedProjectButton() throws Exception{
        testForgeAdminLogin();
        
        //Navigate to Site Admin Page.
        driver.findElement(By.linkText("Site Admin")).click();
        //click on the Quick Pass tab after login.
        driver.findElement(By.linkText("Quick Pass")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/index.php"));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath("//select")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Rejected Projects");
        driver.findElement(By.xpath("//option[4]")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_reject_projects"));
        
        driver.findElement(By.xpath("//button")).click();
        assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_reject_projects.php"));
        }
    
 }