<?php
/**
 * FusionForge User's Personal Page
 *
 * Copyright 1999-2001, VA Linux Systems, Inc.
 * Copyright 2002-2004, GForge Team
 * Copyright 2009, Roland Mas
 * Copyright 2011, Franck Villaume - TrivialDev
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';

$user=session_get_user();
$UserID=user_getid();
$TableGen=new BsTable;
$Groups=$user->getGroups(false);

use_javascript('activity_comments.js');
use_javascript('time_countdown.js');
use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('/themes/gforge/css/feed.css');

$SubscriptionActivity=new SubscriptionActivity($UserID);

$JS='$(function(){
    '.$SubscriptionActivity->generateActivityUpdateJS($UserID, '#subscription_content').'
});';
add_js($JS);

if (!session_loggedin()) { // || $sf_user_hash) {
	exit_not_logged_in();
}

site_user_header(array('title'=>_('Dashboard')));
echo '<style>
.icon{float:left;margin-top:5px}
.time{font-size:10px}
.activity{clear:both;padding-bottom:10px}
</style>';
$Layout->col(2,true);
echo $user->getImageHTML(150,150);
$Layout->endcol()->col(7);
echo '<section>';
$HTML->heading(_('Stream'),2);
echo '<div id="subscription_content">';

echo $SubscriptionActivity->getContent();
echo '</div>
</section>';
$Layout->endcol()->col(3);

$Tasks=array();

if (forge_check_global_perm ('forge_admin') || forge_check_global_perm ('approve_projects') || forge_check_global_perm ('approve_news')) {
	if (forge_check_global_perm ('forge_admin')) {
		$res = db_query_params("SELECT count(*) AS count FROM users WHERE status='P' OR status='V' OR status='W'",array());
		$row = db_fetch_array($res);
		if ($row['count'])
			$Tasks[]=array('<a href="/admin/quickpass_pending_user.php">'._('Pending Users').'</a>',$row['count']);

		/*if ($GLOBALS['sys_user_approval'] == 1) {
			$res = db_query_params("SELECT count(*) AS count FROM users WHERE status='V' OR status='W'",array());
			$row = db_fetch_array($res);
			echo '<tr><td><a href="/admin/approve_pending_users.php?page=validated">'._('Users pending email activation').'</a></td><td>'.$row['count'].'</td></tr>';
		}*/
	}

	if (forge_check_global_perm ('approve_projects')) {
		$res = db_query_params("SELECT count(*) AS count FROM groups WHERE status='P'",array());
		$row = db_fetch_array($res);
		if ($row['count'])
			$Tasks[]=array('<a href="/admin/index.php">'._('Pending Projects').'</a>',$row['count']);
	}

	if (forge_check_global_perm ('approve_news')) {
		$old_date = time()-60*60*24*30;
		$res = db_query_params('SELECT groups.group_id,id,post_date,summary,
				group_name,unix_group_name
			FROM news_bytes,groups
			WHERE is_approved=0
			AND news_bytes.group_id=groups.group_id
			AND post_date > $1
			AND groups.status=$2
			ORDER BY post_date',array ($old_date, 'A')) ;
		$News=db_numrows($res);
		if ($News)
			$Tasks[]=array('<a href="/news/admin/quickpass_pending_news.php">'._('News Approvals').'</a>',$News);
	}
}

$Content='';
if (count($Tasks)){
	if ($Tasks){
        $Content='tasks';
        $TableGen->head(array('',''));
		foreach($Tasks as $i)
            $TableGen->col($i[0])->col($i[1]);
	}
}

if ($Content)
	echo $HTML->widget(_('Admin Tasks'),$TableGen->render());

$MyGroups=$user->getOwnedProjects();
if ($MyGroups){
    $List=array();
    foreach($MyGroups as $i)
        $List[]=$i->makeLink();

    echo $HTML->widget_list(_('My Projects').'<span class="label update pull-right"><a href="/register/">Create a Project</a></span>',$List);
}

if ($Groups){
	$List=array();
	foreach ($Groups as $i)
		$List[]=$i->makeLink();

	echo $HTML->widget_list(_('Projects'),$List);
}

$Subs=$user->getSubscriptionsLinks();
if ($Subs[0] || $Subs[1]){
	$List=array();
	$List=array_merge($Subs[0],$Subs[1]);

	echo $HTML->widget_list(_('Following'),$List);
}

$Servers=$user->getServers();
if ($Servers){
	$List=array();
	foreach($Servers as $i){
		if ($i->getAlias())
            $List[$i->getID()]=$i->getAlias();
        else
            $List[$i->getID()]=$i->getURL();
    }

	echo $HTML->widget_list(_('Registered Servers<span class="label update pull-right"><a href="/account/servers.php">Edit</a></span>'), $List);
}

/*
//Documents
$result=db_query_params('select DISTINCT groups.group_name, docdata_vw.group_id from groups, docdata_vw, docdata_monitored_docman where docdata_monitored_docman.doc_id = docdata_vw.docid and groups.group_id = docdata_vw.group_id and docdata_monitored_docman.user_id = $1',array($UserID));
$rows=db_numrows($result);
$Content='';
if ($result && $rows) {
	for ($j=0; $j<$rows; $j++) {
		$group_id = db_result($result,$j,'group_id');

		$sql2 = "select docdata_vw.docid, docdata_vw.doc_group, docdata_vw.filename, docdata_vw.filetype, docdata_vw.description from docdata_vw,docdata_monitored_docman where docdata_vw.docid = docdata_monitored_docman.doc_id and docdata_vw.group_id = $1 and docdata_monitored_docman.user_id = $2 limit 100";
		$result2 = db_query_params($sql2,array($group_id,$UserID));
		$rows2 = db_numrows($result2);

		$Content.='<a href="javascript:void(0)" class="expand_button" data-elem="docs_'.$group_id.'"><img src="'.$HTML->imgroot.'pointer_down.png" /></a> <a href="/docman/?group_id='.$group_id.'">'.db_result($result,$j,'group_name').'</a> ['.$rows2.']
		<div id="docs_'.$group_id.'" class="hide">';

		for ($i=0; $i<$rows2; $i++) {
			$doc_group = db_result($result2,$i,'doc_group');
			$docid = db_result($result2,$i,'docid');

			$Content.='<a href="/docman/?group_id='.$group_id.'&amp;view=listfile&amp;dirid='.$doc_group.'">'.stripslashes(db_result($result2,$i,'filename')).'</a>';
			$Content.=' <a href="/docman/?group_id='.$group_id.'&amp;action=monitorfile&amp;option=remove&amp;view=listfile&amp;dirid='.$doc_group.'&amp;fileid='.$docid.'"><img src="'.$HTML->imgroot.'ic/trash.png" height="16" width="16" border="0" alt="'._("STOP MONITORING").'" /></a>';
		}
		$Content.='</div>';
	}
}


//Bookmarks
$result = db_query_params("SELECT bookmark_url, bookmark_title, bookmark_id from user_bookmarks where ".
            "user_id=$1 ORDER BY bookmark_title",array( user_getid() ));
$rows = db_numrows($result);
if ($result && $rows) {
	$HTML->heading(_('Bookmarks'),3);
	echo '<p>';
	for ($i=0; $i<$rows; $i++) {
		echo '<a href="'. db_result($result,$i,'bookmark_url') .'">'. db_result($result,$i,'bookmark_title') .'</a>
		<a href="/my/bookmark_delete.php?bookmark_id='. db_result($result,$i,'bookmark_id').'" onClick="return confirm(\''._("Delete this bookmark?").'\')"><img src="'.$HTML->imgroot.'ic/trash.png" height="16" width="16" border="0" alt="DELETE" /></a>';
	}
	echo '</p>';
}

//Forums
$sql="SELECT DISTINCT groups.group_id, groups.group_name ".
             "FROM groups,forum_group_list,forum_monitored_forums ".
             "WHERE groups.group_id=forum_group_list.group_id ".
             "AND groups.status = 'A' ".
            "AND forum_group_list.is_public <> 9 ".
             "AND forum_group_list.group_forum_id=forum_monitored_forums.forum_id ".
             "AND forum_monitored_forums.user_id=$1 ";
$um = UserManager::instance();
$current_user = $um->getCurrentUser();
if ($current_user->getStatus()=='S') {
	$projects = $current_user->getProjects();
	$sql .= "AND groups.group_id IN (". implode(',', $projects) .") ";
}
//$sql .= "GROUP BY groups.group_id ORDER BY groups.group_id ASC LIMIT 100";
$sql .= "ORDER BY groups.group_id ASC LIMIT 100";

$result=db_query_params($sql,array(user_getid()));
$rows=db_numrows($result);
if ($result && $rows > 1) {
	if (!$HasMonitors){
		$HTML->boxTop()->heading(_('Monitors'),2);
		$HasMonitors=true;
	}

	$HTML->heading(_('Forums'),3);
	echo '<p>';
	for ($j=0; $j<$rows; $j++) {
		$sql2="SELECT forum_group_list.group_forum_id,forum_group_list.forum_name ".
			"FROM groups,forum_group_list,forum_monitored_forums ".
			"WHERE groups.group_id=forum_group_list.group_id ".
			"AND groups.group_id=$1".
			"AND forum_group_list.is_public <> 9 ".
			"AND forum_group_list.group_forum_id=forum_monitored_forums.forum_id ".
			"AND forum_monitored_forums.user_id=$2 LIMIT 100";

		$result2 = db_query_params($sql2,array($group_id,$UserID));
		$rows2 = db_numrows($result2);

		echo '<a href="javascript:void(0)" class="expand_button" data-elem="forums_'.$group_id.'"><img src="'.$HTML->imgroot.'pointer_down.png" /></a> <a href="/forum/?group_id='.$group_id.'">'.db_result($result,$j,'group_name').'</a> ['.$rows2.']
		<div id="forums_'.$group_id.'" class="hide">';

		for ($i=0; $i<$rows2; $i++) {
				$group_forum_id = db_result($result2,$i,'group_forum_id');
				echo '<a href="/forum/forum.php?forum_id='.$group_forum_id.'">'.stripslashes(db_result($result2,$i,'forum_name')).'</a>
				<a href="/my/stop_monitor.php?forum_id='.$group_forum_id.
					'" onClick="return confirm(\''._("Stop monitoring this Forum?").'\')">'.
					'<img src="'.$HTML->imgroot.'ic/trash.png" height="16" width="16" '.
					'border="0" alt="'._("Stop monitoring").'" /></a>';
		}
		echo '</div>';
	}
	echo '</p>';
}

//Surveys
sortProjectList ($Groups) ;
$tmp = array () ;
$Surveys=array();
foreach ($Groups as $p) {
	$sf = new SurveyFactory($p);
	foreach ($sf->getSurveys() as $s) {
		$Surveys[]=$s;
		$tmp[] = $p ;
		break ;
	}
}
$projects = $tmp ;

if (count($projects)) {
	if (!$HasMonitors){
		$HTML->boxTop()->heading(_('Monitors'),2);
		$HasMonitors=true;
	}
	echo '<p>';
	$HTML->heading(_('Surveys'),3);
	foreach ($projects as $project) {
		$group_id = $project->getID() ;
		$surveyfacto = new SurveyFactory($project);
		$surveys = $surveyfacto->getSurveys();

		echo '<div><a href="javascript:void(0)" class="expand_button" data-elem="survey_'.$group_id.'"><img src="'.$HTML->imgroot.'pointer_right.png" /></a> <a href="/survey/?group_id='.$group_id.'">'.$project->getPublicName().'</a> ['.count($surveys).']
		<div id="survey_'.$group_id.'" class="hide">';

		foreach ($surveys as $survey) {
			$group_survey_id= $survey->getId();
			$survey_title = $survey->getTitle();
			$devsurvey_is_active = $survey->isActive();
			if($devsurvey_is_active == 1 ) {
				echo '<a href="/survey/survey.php?group_id='.$group_id.'&amp;survey_id='.$group_survey_id.'">'.$survey_title.'</a>';
			}
		}
		echo '</div></div>';
	}
	echo '</p>';
}
if ($HasMonitors){
	$HTML->boxBottom();
}*/

$Layout->endcol();

site_user_footer();

db_display_queries();
?>
