<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/CEM.class.php';
require_once $gfcommon.'include/DOMEInterface.class.php';
$ServerID=getIntFromRequest('server_id');

$InterfaceData=getStringFromRequest('data');
$CemID=getIntFromRequest('cem_id');
$Alias=getStringFromRequest('alias');

$Return=array('error'=>false);

$DOMEInterface=new DOMEInterface;
if ($InterfaceID=DOMEInterface::create($CemID, $InterfaceData, $ServerID, $Alias)){
	$Return['interface_id']=$InterfaceID;
}else{
	$Return['error']=true;
	$Return['msg']='Error';//$DOMEInterface->getErrorMessage();
}

echo json_encode($Return);
?>
