<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/DOMEApi.class.php';
require_once $gfcommon.'include/Server.class.php';
$Server=new Server(getIntFromRequest('server_id'));
$URL=$Server->getURL();

$DOMEApi=new DOMEApi($URL, false);

$Version=$_POST['version'];
if (array_key_exists('model_id', $_POST)) {
	$ModelID=$_POST['model_id'];
	$idType = 0;
} else {
	$ModelID=$_POST['project_id'];
	$idType = 1;
}
$Name=$_POST['name'];
//$FolderID=$_POST['folder_id'];
$Path=$_POST['path'];
$InterfaceID=$_POST['interface_id'];

$Return=array('error'=>false);
if ($Model=$DOMEApi->getModel($Version, $ModelID, $InterfaceID, $Name, $Path, $idType)){
	$Return['data']=$Model;
}else{
	$Return['error']=true;
	$Return['msg']=$DOMEApi->getErrorMessage();
}

echo json_encode($Return);
?>
