<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:36 PM 10/28/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfplugins.'scmsvn/common/SVNPlugin.class.php';

$SVNPlugin=new SVNPlugin;

if (isset($Group)){
	$Repo=$Group->getUnixName();
}else{
	$Repo=getStringFromRequest("repo");
}

$Folder=isset($_REQUEST['folder'])?getStringFromRequest("folder"):'/';

$Tree=$SVNPlugin->treeGetChildren($Repo,$Folder);

//Prints JSON encoded Array
echo json_encode($Tree);
?>