<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:59 AM 11/29/11
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/HomeComments.class.php';
require_once $gfcommon.'include/SiteActivity.class.php';

$ObjectID=getIntFromRequest('rid');
$TypeID=getIntFromRequest('tid');

$HomeComments=new HomeComments($ObjectID,$TypeID);
$Post=$HomeComments->post(session_get_user()->getID(),getStringFromRequest('comment'));
if ($Post['error']){
	echo json_encode(array('error'=>true,'msg'=>$Post['msg']));
}else{
	if ($TypeID==1){
        //user
		$UserActivity=new UserActivity($ObjectID);
		$NewComment=$UserActivity->formatData(SiteActivity::getSingle($Post['activity_id']));
	}else{
		//project
		$ProjectActivity=new ProjectActivity($ObjectID);
		$NewComment=$ProjectActivity->formatData(SiteActivity::getSingle($Post['activity_id']));
	}

	echo json_encode(array('error'=>false,'comment'=>$NewComment,'aid'=>$Post['last_id']));
}
?>