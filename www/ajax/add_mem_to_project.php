<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 1:56 PM 10/10/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

$group_id = getIntFromRequest('group_id');
$group = group_get_object($group_id);

$Return=array('status'=>false);

$form_unix_name = getStringFromRequest('form_unix_name');
$user_object = &user_get_object_by_name($form_unix_name);
if ($user_object === false) {
	$Return['msg']=_('No Matching Users Found');
} else {
	$role_id = getIntFromRequest('role_id');
	if (!$role_id) {
		$Return['msg']= _('Role not selected');
	} else {
		$user_id = $user_object->getID();
		if (!$group->addUser($form_unix_name,$role_id)) {
			$Return['msg']= $group->getErrorMessage();
		} else {
			$Return['status']=true;
			$Return['msg']= _("Member Added Successfully");
			//if the user have requested to join this group
			//we should remove him from the request list
			//since it has already been added
			$gjr=new GroupJoinRequest($group,$user_id);
			if ($gjr || is_object($gjr) || !$gjr->isError()) {
				$gjr->delete(true);
			}
		}
	}
}

echo json_encode($Return);
?>