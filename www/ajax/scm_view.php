<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:31 PM 10/28/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfplugins.'scmsvn/common/SVNPlugin.class.php';

$SVNPlugin=new SVNPlugin;
$Repo=getStringFromRequest("repo");
$File=isset($_REQUEST['file'])?getStringFromRequest("file"):'/';

echo $SVNPlugin->view($Repo,$File);
?>