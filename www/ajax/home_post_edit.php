<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Content=getStringFromRequest('content');
$ActivityID=getIntFromRequest('aid');

$Return=array('error'=>false);

if (!HomeComments::edit($ActivityID, $Content))
    $Return['error']=true;

echo json_encode($Return);
?>