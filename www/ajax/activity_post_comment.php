<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:55 AM 12/5/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/SiteActivity.class.php';
$SiteActivity=new SiteActivity;

$ActivityID=getIntFromRequest('aid');
$Comment=getStringFromRequest('comment');

$Return=array();
$Return['error']=false;
if ($ReplyID=$SiteActivity->submitReply($ActivityID,$Comment)){
	$Return['msg']=$SiteActivity->formatReply(array('activity_comment_id'=>$ReplyID,'activity_id'=>$ActivityID,'comment'=>$Comment,'time_posted'=>time(),'user_id'=>user_getid()));
	$Return['rid']=$ReplyID;
}else{
	$Return['error']=true;
	$Return['msg']="Unable to post to database";
}

echo json_encode($Return);
?>