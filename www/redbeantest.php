<?php
require_once 'env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once APP_PATH.'common/include/rb.php';
require_once APP_PATH.'common/include/CEM.class.php';
R::setup('pgsql:host=ip-10-12-158-132.ec2.internal;dbname=gforgedev','gforge','');

$Steps=10;

//Test 1

$Start=microtime(true);
for($i=0;$i<$Steps;$i++){
    if ($ID=CEM::create(117,'test',0)){
        $CEM=new CEM($ID);
        $CEM->update(120,'new test', $CEM->getParent());
        $CEM->destroy();
    }
}
echo '1: '.(microtime(true)-$Start).'<br>';

$Start=microtime(true);
for($i=0;$i<$Steps;$i++){
    $CEM2=R::dispense('rb_cem_object');
    $CEM2->group_id=117;
    $CEM2->name='test';
    $CEM2->parent_cem_id=0;
    $ID=R::store($CEM2);
    R::load('rb_cem_object', $ID);
    R::trash($CEM2);
}
echo '2: '.(microtime(true)-$Start);


//Test 2
/*
$Start=microtime(true);
for($i=0;$i<$Steps;$i++)
    db_query_params('SELECT * FROM activity_log LIMIT 2000');
echo '1: '.(microtime(true)-$Start).'<br>';

$Start=microtime(true);
for($i=0;$i<$Steps;$i++)
    R::exec('SELECT * FROM activity_log LIMIT 2000');
echo '2: '.(microtime(true)-$Start);*/


//Test 3
/*$Start=microtime(true);
for($i=0;$i<$Steps;$i++)
    db_query_params('SELECT * FROM site_activity WHERE (type_id=1 AND object_id=105) OR ((type_id=1 OR type_id=2 OR type_id=3 OR type_id=4 OR type_id=5 OR type_id=6 OR type_id=7 OR type_id=8 OR type_id=9 OR type_id=10 OR type_id=11) AND (object_id=6 OR object_id=28 OR object_id=70)) OR assigned_to_id=105 ORDER BY time_posted DESC LIMIT 3000');
echo '1: '.(microtime(true)-$Start).'<br>';

$Start=microtime(true);
for($i=0;$i<$Steps;$i++)
    R::exec('SELECT * FROM site_activity WHERE (type_id=1 AND object_id=105) OR ((type_id=1 OR type_id=2 OR type_id=3 OR type_id=4 OR type_id=5 OR type_id=6 OR type_id=7 OR type_id=8 OR type_id=9 OR type_id=10 OR type_id=11) AND (object_id=6 OR object_id=28 OR object_id=70)) OR assigned_to_id=105 ORDER BY time_posted DESC LIMIT 3000');
echo '2: '.(microtime(true)-$Start);*/

R::close();
?>