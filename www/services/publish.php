<?php
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';

// get group
$group_id=getIntFromRequest('group_id');
$group = group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

if (!$group || !is_object($group)) {
    exit_no_group();
}

site_project_header(array('group'=>$group_id,'title'=>'Service View:'),2);


$Layout->col(3,true,true); // BEGIN left hand column
include 'service_nav.php';
?>


<?php
$Layout->endcol();	// END left hand column
$Layout->col(9); // BEGIN main content
?>

	Coming soon!

<?php 
$Layout->endcol(); 	// END main content
site_project_footer();
?>
