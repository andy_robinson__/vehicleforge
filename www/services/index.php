<?php
  //header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';

// get group
$group_id=getStringFromRequest('group_id');
$group = group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

if (!$group || !is_object($group)) {
    exit_no_group();
}

// get dome interface
$InterfaceID=getIntFromRequest('diid');
$RunID=getIntFromRequest('runid', false);
$DOMEInterface=new DOMEInterface($InterfaceID);
/* unused
$ModelGraphSettings = new ModelGraphSettings($InterfaceID);
$ModelGraphSettings->fetch_data();
*/

if ($DOMEInterface->getID() != 0){
  $Title=$DOMEInterface->getName();
} else {
	$Title='';
}

// add bootstrap tab plugin
//use_javascript('bootstrap.js');

site_project_header(array('group'=>$group_id,'title'=>'Service View: '.$Title), 2);
?><script>

    // ie9 shim's
  
    if (!CSSStyleDeclaration) {
    	var CSSStyleDeclaration = function() {};
    }
    if (!document.createElementNS) {
        document.createElementNS = function(uri, name) {
    		return document.createElement(name);
    	};
    }
     
    CSSStyleDeclaration.prototype.getProperty = function(a) {
        return this.getAttribute(a);
    };
    CSSStyleDeclaration.prototype.setProperty = function(a,b) {
        return this.setAttribute(a,b);
    }
    CSSStyleDeclaration.prototype.removeProperty = function(a) {
        return this.removeAttribute(a);
    }
   
    if (!Array.prototype.reduce) {
    	  Array.prototype.reduce = function reduce(accumulator){
    	    if (this===null || this===undefined) throw new TypeError("Object is null or undefined");
    	    var i = 0, l = this.length >> 0, curr;
    	 
    	    if(typeof accumulator !== "function") // ES5 : "If IsCallable(callbackfn) is false, throw a TypeError exception."
    	      throw new TypeError("First argument is not callable");
    	 
    	    if(arguments.length < 2) {
    	      if (l === 0) throw new TypeError("Array length is 0 and no second argument");
    	      curr = this[0];
    	      i = 1; // start accumulating at the second element
    	    }
    	    else
    	      curr = arguments[1];
    	 
    	    while (i < l) {
    	      if(i in this) curr = accumulator.call(undefined, curr, this[i], i, this);
    	      ++i;
    	    }
    	 
    	    return curr;
    	  };
    	}
  </script><?php

if ($DOMEInterface->getID() != 0) {
    include 'show.php';
} else {

	/* this is a killer query... need to add group_id to the service_cem map */
	function findServices($componentArray) {
		$s = array();
		foreach ($componentArray as $c) {
			foreach($c->getInterfaces() as $i) {
				$s[]= array("component"=>$c, "interface"=>$i);
			}
			$s = array_merge($s, findServices($c->getSubComponents()));
		}
		return $s;
	}
	
	$services = findServices($group->getComponents());
	
	$Layout->col(3,true,true);
	include 'service_nav.php';
	$Layout->endcol();
	
	$Layout->col(9);
	?>
	<h2 style="border-bottom: 1px solid #AAA;">Services in <?=$group->getPublicName()?></h2><br/>
	
	<h3>Service Subscriptions</h3>
	<ul class="unstyled">
		<?php foreach($services as $s) { ?>
			<li>
				<a href="/services/?group_id=<?=$group->getID()?>&diid=<?=$s['interface']->getID()?>"><?=$s['interface']->getName()?></a>
				in
				<a href="/components/?group_id=<?=$group->getID()?>&cid=<?=$s['component']->getID()?>"><?=$s['component']->getName()?></a>
			</li>
		<?php } ?>
	</ul>
	
	<h3>Your Services</h3>
	No services registered yet.

	<?php
	$Layout->endcol();
}

site_project_footer();
?>
