<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$ModelID=getIntFromRequest('model_id');

$Return=array('error'=>false);
//$Return['msg'] = ModelGraphSettings::entryExists($ModelID);


if (ModelGraphSettings::entryExists($ModelID)) {
	$ModelGraphSettings = new ModelGraphSettings($ModelID);
	$ModelGraphSettings->fetch_data();
	$Return['msg'] = $ModelGraphSettings->getGraphSettings();
}
else {
	$Return['error'] = true;
}

//$Return['msg'] = array($ModelID, $GraphSettings);
echo json_encode($Return);
?>