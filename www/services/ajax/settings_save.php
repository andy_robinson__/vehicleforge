<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$DIID=getIntFromRequest('diid');
$Alias=getStringFromRequest('alias');
$Return=array('error'=>false);

$DOMEInterface=new DOMEInterface($DIID);
$Return['error']=!$DOMEInterface->setSettings($Alias, getStringFromRequest('desc'));

echo json_encode($Return);
?>