<?php
/*
 * Local variables:
 *      $RunID - The ID of the run. False if no run ID is specified
 */

if ($RunID){
    $CEMRun=new CEMRunning($RunID);
    $Messages=$CEMRun->getMessages(true);

    $CEMRun->setViewStatus(true);

    //var_dump($Messages);

    $StepCount=0;
    $ParamValues=array();
    $ParamChangeStep=array();

    foreach($Messages as $i){
        if ($i->event=="class mit.cadlab.dome3.api.ParameterValueChangeEvent"){
            $ParamChangeStep[$StepCount]=$i->param;
            $oldVal = $i->old_val;
            if (count($i->old_val) > 0) {
            	$oldVal = json_encode($i->old_val[0]);
            }
            else if (!is_array($i->old_val)) {
            	$oldVal = $i->old_val;
            }
            $newVal = $i->new_val[0];
            if (isset($i->new_val[0]->data)) {
            	$newVal = json_encode($i->new_val[0]->data);
            } else if (!is_array($i->new_val)) {
            	$newVal = $i->new_val;
            }
            $ParamValues[$StepCount]=array($oldVal, $newVal);
            $StepCount++;
        }else if (($i->event=="class mit.cadlab.dome3.api.ParameterStatusChangeEvent") &&
        		 ($i->new_val=="FAILURE")){
        	echo 'Error: '.$i->id->idString;
        }

        //var_dump($i);
        //echo $i->event.': ('.$i->param.')='.$i->new_val[0];
        //echo '<br><br>';
    }

    //echo 'Step '.$StepCount.' of '.$StepCount;

    for($i=0; $i<$StepCount; $i++){
        echo 'Step '.($i+1).': '.$ParamChangeStep[$i].' = '.$ParamValues[$i][0].' -> '.$ParamValues[$i][1];
        echo '<br>';
    }
}
?>