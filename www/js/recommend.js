$(function(){
	$("button.recommend-yes").click(function(){
		var $me=$(this),
				type=$me.parent().data("type"),
				user=$me.parent().data("user");

		$.getJSON("/ajax/recommend.php",{'value':1,'type_id':type,'user_id':user},function(msg){
			if (msg.error==false){
				$me.addClass("success").children(".count").text(msg.yes);
				$me.siblings(".recommend-no").removeClass("error").children(".count").text(msg.no);
			}
		});
	});

	$("button.recommend-no").click(function(){
		var $me=$(this),
				type=$me.parent().data("type"),
				user=$me.parent().data("user");

		$.getJSON("/ajax/recommend.php",{'value':0,'type_id':type,'user_id':user},function(msg){
			if (msg.error==false){
				$me.addClass("error").children(".count").text(msg.no);
				$me.siblings(".recommend-yes").removeClass("success").children(".count").text(msg.yes);
			}
		});
	});
});