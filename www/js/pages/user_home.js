$(function(){
    var s=[["Charisma",Math.random()*20],["Skill Level",Math.random()*20],["Teamwork",Math.random()*20],["Availability",Math.random()*20]];

    $.jqplot("rating",[s],{
        grid:{
            drawBorder:false,
            drawGridLines:false,
            shadow: false,
            background: "#FFF"
        },
        axesDefaults:{},
        seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            rendererOptions:{
                showDataLabels:true,
                padding:10
            }
        },
        legend: {
            show: true,
            rendererOptions: {
                numberRows: 2
            },
            location: "s"
        }
    });

    var editing=false;

    var $edit_button=$("#edit_mode"),
        $about_me=$("#about_me"),
        $image_overlay=$("#profile_image_overlay"),
        $image=$("#profile_image"),
        $skills=$("#skills"),
        $skills_overlay=$("#skills_overlay"),
        $skills_modal=$("#modal_skills"),
        $cancel_button=$("#edit_cancel"),
        $subscribe_button=$("#subscribe_button");

    $subscribe_button.hover(function(){
        if ($subscribe_button.hasClass('subscribed')){
            $subscribe_button.removeClass('btn-success').addClass('btn-danger');
            $subscribe_button.children('span').text('Unsubscribe');
            $subscribe_button.children('i').removeClass('icon-ok-sign').addClass('icon-remove-sign');
        }else{
            $subscribe_button.removeClass('btn-info').addClass('btn-success');
            $subscribe_button.children('span').text('Subscribe');
            $subscribe_button.children('i').removeClass('icon-info-sign').addClass('icon-plus-sign');
        }
    },function(){
        if ($subscribe_button.hasClass('subscribed')){
            $subscribe_button.removeClass('btn-danger').addClass('btn-success');
            $subscribe_button.children('span').text('Subscribed');
            $subscribe_button.children('i').removeClass('icon-remove-sign').addClass('icon-ok-sign');
        }else{
            $subscribe_button.removeClass('btn-success').addClass('btn-info');
            $subscribe_button.children('span').text('Not Subscribed');
            $subscribe_button.children('i').removeClass('icon-plus-sign').addClass('icon-info-sign');
        }
    }).click(function(){
        var $me=$(this);
        $.ajax({
            url         : '/user/ajax/channel_subscribe.php',
            dataType    : 'json',
            type        : 'post',
            data        : {
                rid     : $me.data('rid'),
                tid     : 1
            }
        }).done(function(msg){
            if (msg.status==true){
                $me.addClass('subscribed btn-success').removeClass('btn-info btn-danger');
                $me.children('span').text('Subscribed');
                $me.children('i').removeClass('icon-plus-sign icon-info-sign').addClass('icon-ok-sign');
            }else{
                $me.removeClass('subscribed btn-danger').addClass('btn-info');
                $me.children('span').text('Not Subscribed');
                $me.children('i').removeClass('icon-remove-sign').addClass('icon-info-sign');
            }
        });
    });

    function stopEditing(){
        editing=false;
        $edit_button.removeClass("editing btn-success").html('<i class="icon-pencil icon-white"></i> Editing Mode');

        $about_me.replaceWith(function(){
            return '<p id="about_me">'+$(this).val()+'</p>';
        });

        $image_overlay.hide();
        $image.css('cursor','auto');

        $skills_overlay.hide();
        $skills.css('cursor','auto');

        $cancel_button.hide();

        $about_me=$("#about_me");
    }

    $cancel_button.click(function(){
        stopEditing();
    });

    $edit_button.click(function(){
        if (editing){
            $.post("/user/ajax/update_profile.php",{
                uid:$edit_button.data('uid'),
                about:$about_me.val()
            });

            stopEditing();
        }else{
            editing=true;
            $edit_button.addClass("editing btn-success").html('<i class="icon-ok icon-white"></i> Save Changes');
            $cancel_button.show();

            //$.post('/user/ajax/get_skills.php',{uid:$edit_button.data('uid')});

            $about_me.replaceWith(function(){
                return '<textarea id="about_me" placeholder="Enter some text about yourself here" style="width:100%">'+$about_me.text()+"</textarea>"
            });

            $image_overlay.show();
            $image.css('cursor','pointer');

            $skills_overlay.show();
            $skills.css('cursor','pointer');
        }

        $about_me=$("#about_me");
    });

    $image.hover(function(){
        $image_overlay.children('i').addClass('icon-white');
    },function(){
        $image_overlay.children('i').removeClass('icon-white');

    });

    $image.click(function(){
        if (editing){
            window.open('/account/change_profile_image.php');
        }
    });

    $skills.hover(function(){
        $skills_overlay.children('i').addClass('icon-white');
    },function(){
        $skills_overlay.children('i').removeClass('icon-white');
    });

    $skills.click(function(){
        if (editing)
            window.open('/people/editprofile.php');
    });
});