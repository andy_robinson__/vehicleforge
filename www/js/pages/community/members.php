<?php
require_once '/community/community_util.php';
$MemberCount=getMemberCount();
header('Content-Type', 'text/javascript');
?>

$(function(){
    var members=[],
        interval,
        page=1,
        member_area_height=$("#members").height(),
        member_cache=[],
        last_page= 1,
        member_count=<?=$MemberCount?>;

    $("#members").height(member_area_height);

    function fadeOut(Callback){
        if (members.length){
            members[0].fadeOut(function(){$(this).remove();});
            members.splice(0,1);

            if (members.length===0){
                clearInterval(interval);
                Callback.call();
            }
        }
    }

    function fadeIn(){
        if (members.length){
            members[0].fadeIn();
            members.splice(0,1);

            if (members.length===0)
                clearInterval(interval);
        }
    }

    function fadeInMembers(){
        if (members.length)
            interval=setInterval(fadeIn, 50);
    }

    function fadeOutMembers(Callback){
        loadMemberArray();

        members.reverse();
        interval=setInterval(fadeOut, 50, Callback);
    }

    function load(){
        if (member_cache[page]){
            $("#members").html(member_cache[page]);
            loadMemberArray();
            interval=setInterval(fadeIn, 50);
        }else{
            $.ajax({
                url         : "ajax/browse_users.php",
                type        : "post",
                dataType    : "json",
                data        : {
                    page    : page
                }
            }).done(function(data){
                    $("#members").html(data.html);
                    loadMemberArray();

                    member_cache[page]=data.html;

                    fadeInMembers();
                });
        }
    }

    $("#page_nav").change(function(){
        page=$(this).val();
        fadeOutMembers(function(){load();});
    });

    $(".page_next").click(function(){
        console.log(member_count);
        if (page*20<member_count){
            page++;
            fadeOutMembers(function(){load();});
            $("#page_nav").val(page);
        }
    });

    $(".page_prev").click(function(){
        if (page>1){
            page--;
            fadeOutMembers(function(){load();});
            $("#page_nav").val(page);
        }
    });

    $("#search").submit(function(){
        var q=$("#search_query").val(),
            type=$(".search_type:checked").val();

        $.ajax({
            dataType    : "json",
            url         : "ajax/search_users.php",
            data        : {
                type    : type,
                q       : q
            }
        }).done(function(data){
                last_page=page;
                $("#clear_search").show();
                fadeOutMembers(function(){
                    $("#members").html(data.html);
                    loadMemberArray();
                    fadeInMembers();
                });
            });

        return false;
    });

    function loadMemberArray(){
        members=[];
        $("#members").children("li").each(function(){
            members.push($(this));
        });
    }

    $(document).on("click", ".user_image", function(){
        var uid=$(this).parent().data("uid");

        $.ajax({
            url         : "ajax/get_user_data.php",
            dataType    : "json",
            data        : {
                uid     : uid
            }
        }).done(function(data){
                $("#modal_user").find("h3").text(data.realname);
                $("#modal_user").find(".image").html(data.image);
                $("#modal_user").find(".about_me").html(data.about);
                var html="<b>Skills</b><br />";

                for(var i=0, l=data.skills.length, el; i < l; i++) {
                    el=data.skills[i];
                    html+=el[0]+"<br /><i>"+el[1].join(", ")+"</i><br />";
                }

                $("#modal_user").find(".skills").html(html);

                $("#modal_user").modal("show");
            });
    });

    $("#clear_search").click(function(){
        page=last_page;
        fadeOutMembers(function(){
            load();
        });
    });
});
