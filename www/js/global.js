$(function(){
	$.fn.errorStyle = function() {
        this.replaceWith(function(i,html){
        	var bold=this.getAttribute("data-title") || "Error";
            var StyledError = "<div class=\"ui-state-error ui-corner-all\" style=\"padding: .2em .7em;"+this.style.cssText+"\">";
            StyledError += "<span class=\"ui-icon ui-icon-alert\" style=\"display:inline-block;margin-right: .3em;\"></span>";
            StyledError+="<strong>"+bold+": </strong>";
            StyledError += html;
            StyledError += "</div>";
            return StyledError;
        });
    };
	
	$.fn.highlightStyle = function() {
        this.replaceWith(function(i,html){
        	var bold=this.getAttribute("data-title") || "Notice";
            var StyledError = "<div class=\"ui-state-highlight ui-corner-all\" style=\"padding: .2em .7em;"+this.style.cssText+"\">";
            StyledError += "<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\">";
            StyledError += "</span><strong>"+bold+" </strong>";
            StyledError += html;
            StyledError += "</p></div>";
            return StyledError;
        });
    };

	$.fn.focusChange=function(){
		focusBlink(this,0);
		this.focus();
	};

	function focusBlink(Element,BlinkCount,OldBorder){
		BlinkCount++;
		var $me=$(Element);
		
		if (!OldBorder)OldBorder=$me.css('border');

		if ($me.hasClass('focusBlink') && BlinkCount<4){
			$me.css({'border':OldBorder}).removeClass('focusBlink');
		}else{
			$me.css({'border':'1px solid #309CD6'}).addClass('focusBlink');
		}

		if (BlinkCount<4){
			setTimeout(function(){focusBlink(Element,BlinkCount,OldBorder);},300);
		}else{
			$me.css({'border':OldBorder}).removeClass('focusBlink');
		}
	}
	
	function rotate(degree){
		$('.rotate').css({ '-moz-transform': 'rotate(' + degree + 'deg)','transform':'rotate(' + degree + 'deg)','-ms-transform':'rotate(' + degree + 'deg)','-webkit-transform':'rotate(' + degree + 'deg)','-o-transform':'rotate(' + degree + 'deg)'}); 
		setTimeout(function(){rotate(++degree)},80);
	}
	
	rotate(0);

	/*
	$('#dialog_login').dialog({
		autoOpen: false,
		modal: true,
		show: 'fade',
		hide: 'fade',
		position: ['center',100],
		buttons: {
			"Log In": function(){},
			"Cancel": function(){
				$(this).dialog('close');
			}
		}
	});
	
	$('#login').click(function(){
		$('#dialog_login').dialog('open');
	});*/

	$('#dialog_feedback').dialog({
		autoOpen: false,
		modal: true,
		show: 'fade',
		hide: 'fade',
		minWidth: 400,
		position: ['center',100],
		buttons: {
			"Send":function(){
				var $this=$(this);
				//console.log(window.location.pathname);
				$.post('/ajax/feedback.php',{'text':$('#feedback').val(),'page':window.location.pathname},function(){
					$('#feedback').val('');
					$this.dialog('close');
				});
			},
			"Cancel":function(){
				$(this).dialog('close');
			}
		}
	});

	$('#open_dialog_feedback').click(function(){
		$('#dialog_feedback').dialog('open');
	});

	$(".alert a.close").click(function(){
		$(this).parent().slideUp(function(){
			$(this).remove();
		});
	});

	$(".dropdown-toggle").dropdown();

	/*var bh=$("#body").height();
	$("#body").height(Math.max(bh,$(document).height()-$("#footer").height()));*/

	$("#check_finished_models").click(function(){
		$.ajax({
			url:'/ajax/check_finished_models.php',
			dataType:'json',
			success:function(msg){
				if (!msg.error){
                    var RunningCount=parseInt($("#running_model_count").text()),
                        FinishedCount=parseInt($("#finished_model_count").text());
                    for(var i in msg.finished){
                        $("#interface_run_list").children('.interface_run').each(function(){
                            if (msg.finished[i]==$(this).data('run-id')){
                                RunningCount--;
                                FinishedCount++;
                                $(this).find('.run_status').removeClass('btn-warning').addClass('btn-success').text('Done');
                            }
                        });
                    }

                    $("#running_model_count").text(RunningCount);
                    $("#finished_model_count").text(FinishedCount);

                    if (!RunningCount)
                        $("#running_model_count").hide();

                    if (FinishedCount)
                        $("#finished_model_count").show();
				}
			}
		});
	});
});