function makeProjectThumb(doc, resultList) {
    var group_id = doc.id;
    var group_img_src = '<img src="https://forge.research.ge.com/images/project_profile.png" alt="">';
    jQuery.ajax({
	    url: '/project/project_picture_decoder.php?group_id=' + group_id, 
		success: function(data) {
		group_img_src = data.group_img_src;
		$(html).find('.projectImage').html('<img src="' + group_img_src + '" width=100 height=100 alt="">');
	    },
		dataType: 'json'
		});
    var html = $(getProjectThumbPlaceholder());
    var desc = '';
    // set attributes
    if(doc.hasOwnProperty('short_description')) {
	desc = doc.short_description[0];
    }
    $(html).data('group_name', doc.group_name);
    $(html).data('unix_group_name', doc.unix_group_name);
    $(html).data('group_id', doc.id);
    $(html).data('short_description', desc);
    $(html).data('img_src', group_img_src);

    // set html
    $(html).find('.group_name').html(doc.group_name);
    $(html).find('.group_desc').html(desc);
		
    // append results
    $('#' + resultList).append(html);
	  
    // add a listener
    $(html).on('click', showProjectModal);
}	

function searchProjects(resultList, resultSummary) {
    query = getParameterByName("q");
    
    // execute ajax request
    $.ajax({
	    type: 'GET',
		url: "/solrSearch/searchProjects.php?q=" + query,
		dataType: "json",
		success: function(json) {
		// remove old results
		$('#' + resultList).children().remove();
		
		// add new results
		var response = json.response, doc;
		for (var i=0; i<response.numFound; i++) {
		    doc = response.docs[i];
		    makeProjectThumb(doc, resultList);
		}

		// update results summary
		if (query == '') {
		    $('#' + resultSummary).html('<h4>Projects</h4>' + response.numFound + ' results found.');
		} else {
		    $('#' + resultSummary).html('<h4>Projects</h4>' + response.numFound + ' results found for search: "' + query + '".');
		}
	    }   
	});
}

function getProjectThumbPlaceholder() {
    var thumb = '';
    thumb = thumb + '<li class="span2">';
    thumb = thumb + '<div class="thumbnail">';
    thumb = thumb + '<center><div class="projectImage">';
    thumb = thumb + '<img src="https://forge.research.ge.com/images/project_profile.png" alt="">';
    thumb = thumb + '</div></center>';
    thumb = thumb + '<div class="caption" style="padding:5px;">';
    thumb = thumb + '<h5 id="group_name" class="group_name"></h5>';
    thumb = thumb + '<p id="group_desc" class="group_desc"></p>';
    thumb = thumb + '</div>';
    thumb = thumb + '</div>';
    thumb = thumb + '</li>';
    return thumb;
}

function showProjectModal(event) {
    // update modal
    var name = $(this).data('group_name');
    var id = $(this).data('group_id');
    var desc = $(this).data('short_description');
    var unix_group_name = $(this).data('unix_group_name');
    var img_src = $(this).data('img_src');
    
    $('#project_name').html(name);
    $('#project_desc').html(desc);
    $('#project_image').attr('src',img_src);
    $('#project_url').attr('href', '/projects/' + unix_group_name);

    // show modal
    $('#projectModal').modal();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null)
	return "";
    else
	return decodeURIComponent(results[1].replace(/\+/g, " "));
}
