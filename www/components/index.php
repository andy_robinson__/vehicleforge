<?php
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';
require_once GIT_CONFIG_FILE;
/*require_once $gfcommon.'include/CEM.class.php';*/

$group_id=getStringFromRequest('group_id');

$group = group_get_object($group_id);
$CemID = getIntFromRequest('cid');
$CEM = new CEM($CemID);

session_require_perm ('project_read', $group_id) ;

if (!$group || !is_object($group)) {
    exit_no_group();
}

$JS='$(function(){
    var $save_git=$("#save_git_access"),
        $access_control=$("#git_access_control tbody"),
        $repo_status=$("#repo_status");

    $save_git.click(function(){
        var uid,read,write,rewind,row,
            access=[];
        $access_control.children("tr").each(function(){
            row=$(this).children("td");
            uid=row.first().data("uid");
            read=row.eq(1).children("input").prop("checked")?1:0;
            write=row.eq(2).children("input").prop("checked")?1:0;
            rewind=row.eq(3).children("input").prop("checked")?1:0;

            access.push([uid,read,write,rewind]);
        });

        $.ajax({
            url: "/components/ajax/save_git_access.php",
            type: "POST",
            dataType: "json",
            data: {
                data:access,
                cid:'.$CemID.'
            },
            success:function(msg){
                $repo_status.removeClass("btn-success btn-danger");
                if (msg.status==2){
                    $repo_status.addClass("btn-warning").html("<b>Status: Pending Creation</b>");
                }else{
                    $repo_status.addClass("btn-info").html("<b>Status: Pending Changes</b>");
                }
            }
        });
    });
});';
add_js($JS);

site_project_header(array('group'=>$group_id,'title'=>'Component View'), 1);

$Layout->col(3,true);
include 'component_nav.php';
$Layout->endcol();

$Layout->col(9);
include 'show.php';
$Layout->endcol();

?>

<div class="modal fade hidden" id="addComponentModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Add a Component</h3>
  </div>
  <div class="modal-body">
    <form class="addComponent" action="/project/new_component.php" method="post">
			<fieldset>
				<input type="hidden" name="group_id" value="<?=$group->getID()?>" />
				<div class="control-group">
					<label class="control-label" for="">Component Name</label>
					<div class="controls">
						<input type="text" autocomplete="off" id="cname" name="cname" />
					</div>
				</div>
				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Add</button>
					<button class="btn" type="reset" data-dismiss="modal">Cancel</button>
				</div>
			</fieldset>

		</form>
  </div>
</div>
<?php
if ($CemID){
?>
<div class="modal fade hidden" id="addSubComponentModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Add Sub-Component to <?= $CEM->getName() ?></h3>
  </div>
  <div class="modal-body">
    <form class="addComponent" action="/project/new_component.php" method="post">
			<fieldset>
				<input type="hidden" name="group_id" value="<?=$group->getID()?>" />
				<input type="hidden" name="cid" value="<?= $CEM->getID() ?>" />
				<div class="control-group">
					<label class="control-label" for="">Component Name</label>
					<div class="controls">
						<input type="text" autocomplete="off" id="sub_cname" name="cname" />
					</div>
				</div>
				<div class="form-actions">
					<button class="btn btn-primary" type="submit">Add</button>
					<button class="btn" type="reset" data-dismiss="modal">Cancel</button>
				</div>
			</fieldset>

		</form>
  </div>
</div>

<div class="modal fade hidden" id="gitAccessModal">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Git Access to <?= $CEM->getName() ?></h3>
    </div>
    <div class="modal-body">
<?=$git_server_user?>@<?=$git_server?>:<?=$CEM->getRepoName()?>
    </div>
    <div class="modal-footer">
        <a href="#" data-dismiss="modal" class="btn btn-primary">OK</a>
    </div>
</div>

<div class="modal fade hidden" id="accessControlModal">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Git Access Control to  <?= $CEM->getName() ?></h3>
  </div>
  <div class="modal-body">
<?php
require_once APP_PATH.'common/include/BsForm.class.php';

$Table=new BsTable(array('class'=>'table table-striped','id'=>'git_access_control'));
$Table->head(array('Member','Read','Write','Rewind'));



$Members=$group->getMembers();
foreach($Members as $i){
    $Permissions=Gitolite::getUserPermissions($i->getID(), $CemID);


    $Table->col($i->getFirstName().' '.$i->getLastName(),array('data-uid'=>$i->getID()))
        ->col('<input type="checkbox" autocomplete="off" class="read" '.($Permissions['read']?'checked="checked"':'').' />')
        ->col('<input type="checkbox" autocomplete="off" class="write" '.($Permissions['write']?'checked="checked"':'').' />')
        ->col('<input type="checkbox" autocomplete="off" class="rewind" '.($Permissions['rewind']?'checked="checked"':'').' />');
}

echo $Table->render();
?>
  </div>
    <div class="modal-footer">
        <a class="btn" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
        <a class="btn btn-primary" data-dismiss="modal" href="javascript:void(0)" id="save_git_access">Save Changes</a>
    </div>
</div>

<?php
}
include $gfwww.'help/help.php';
site_project_footer();

//db_display_queries();
?>
