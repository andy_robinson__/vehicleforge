<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
/*require_once $gfcommon.'include/CEMTag.class.php';*/

$cem_id=getIntFromRequest('cem_id');
$tag_name=getStringFromRequest('name');

$Return=array('error'=>false);

if ($Tags=CEMTag::search($tag_name)){
    $Return['tags']=$Tags;
}else{
    $Return['error']=true;
}

echo json_encode($Return);
?>