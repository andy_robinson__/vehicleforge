<?php
/* Forge specific includes */
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';

$cem_id=getIntFromRequest('cem_id');
$group_id=getIntFromRequest('group_id');

$CEM = new CEM($cem_id);
if ($CEM->destroy()) {
	$Return['error'] = false;
} else {
  $Return['error']=true;
  $Return['msg']='An error occurred deleting the component';
}

echo json_encode($Return);
?>