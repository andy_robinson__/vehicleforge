<?php

/*
 *	component_nav.php
 *	Show a navigable tree of components
 *	Jeff Mekler (2/11/2012)
 *
 *	local variables:
 *		$group - Group object
 */

function getComponents($components) {
	$out = array();
	foreach($components as $c) {
		$out[]= $c;
		$out = array_merge($out, getComponents($c->getSubComponents()));
	}
	return $out;
}

function makeComponentNav($group) {
	$CEMs = $group->getComponents();
	
	// iterate through all CEMs
	if (count($CEMs) > 0) {			
		echo '<div id="search-results"><h4>Search results</h4><p id="result-summary"></p><ul id="result-list"></ul><br/></div>';

		echo '<h4>All Components</h4>';
		echo '<ul class="nav-tree">';
		foreach ($CEMs as $cem) {
			renderSubComponents($group, $cem);
		}
		echo '</ul>';
	}
}

// recursive method to generate subcomponent navigation
function renderSubComponents($group, $cem) {
	
	$id = $cem->getID();
	$name = $cem->getName();

	$subs = $cem->getSubComponents();
	if (count($subs) == 0) {
		echo '<li class="component" cid='.$id.'>';
		
		/* @Author: Amine Chigani
		 * Changed how the URL gets composed to included the group_id because it's used to identify
		* whether user has admin access and show the admin tab.
		*/
        echo '<a href="'. '/components/?group_id=' . $group->getID() .'&cid='.$id .'">'.$name.'</a>';
        //echo '<a href="'. Router::route('/components/?group_id='.$group->getUnixName().'&cid='.$id) .'">'.$name.'</a>';
		echo '</li>';
				
	} else {
	
		echo '<li class="component hasSubList" cid=' . $id . '>';
			echo '<span class="tree-toggle"> &#9654; </span>';
			
			/* @Author: Amine Chigani
			 * Changed how the URL gets composed to included the group_id because it's used to identify
			 * whether user has admin access and show the admin tab.
			*/ 
			echo '<a href="'. '/components/?group_id=' . $group->getID() . '&cid='.$id .'">'.$name.'</a>';
			//echo '<a href="'.Router::route('/components/?group_id='.$group->getUnixName().'&cid='.$id).'">'.$name.'</a>';

			echo '<ul class="nav-tree hide">';
			foreach ($subs as $sub) {
				renderSubComponents($group, $sub);
			}
			echo '</ul>';
		echo '</li>';
	}
	

}

/* 	$selected_CEM = $CEM->getID(); */
	$components = getComponents($group->getComponents());
	$names = function($component) {return $component->getName();};
	$ids = function($component) {return $component->getID();};
	
?>

<div class="well" id="component-nav" helptip-text="Navigate to a component using this list.  Sub-components can be accessed by clicking on the arrows." helptip-position="top">
	<form id="component-search">
		<input type="text" name="q" class="search-query" placeholder="Find Components" data-provide="typeahead" data-source='<?= json_encode(array_map($names,$components)) ?>'/>
	</form>
	
	<?php makeComponentNav($group); ?>
	
<!-- 	<a class="btn btn-small center" href="/graphs/components.php?group_id=<?=$group->getID() ?>" helptip-text="Visual representation of your component heirarchy" helptip-position="bottom"><i class="icon-eye-open"></i> Graph View</a> -->
	<a class="btn btn-small center" href="#" data-toggle="modal" id="addComponent" data-target="#addComponentModal"><i class="icon-plus"></i> Component</a>
<!-- 	<a class="btn btn-small center" href="#" id="edit-component"><i class="icon-move"></i> edit</a> -->
</div>

<script>
	$(document).ready( function() {
        $("#addComponent").click(function(){
            $("#cname").focus();
        });

		// open component tree structure to reveal currently selected component		
		$('.component[cid="<?=$CEM->getID()?>"]').addClass('active');

		var listsToOpen = $('.component.active').parentsUntil('#component-nav', '.component');
		listsToOpen.each( function() {
			$(this).children('ul').removeClass('hide');
			$(this).children('span').html('&#9660;');
		});
		
		// add listener to tree toggles
		$('.tree-toggle').bind('click', function(event) {		
			var li = $(this).closest('li');
			var ul = $(li).children('ul');
			
			if ($(ul).hasClass('hide')){
				$(li).children('span').html('&#9660;');
				$(ul).removeClass('hide');
			} else {
				$(li).children('span').html('&#9654;');
				$(ul).addClass('hide');
			}
		});
		
		// search
		$('#search-results').hide();
		$('#component-search').bind('submit', function(e) {
			searchComponents($(this).children('input[name="q"]').val());
			return false;
		});
		
	});


	
// ajax query to return search results
	function searchComponents(query) {
		// execute ajax request
		$.ajax({
		  type: 'GET',
		  url: "/solrSearch/proxy.php/select",
		  data: {
		    q: "*" + query + "*",
		    defType: 'edismax',
		    qf: 'component_name',
		    core: 'components',
		    fq: 'group_id:'+<?= $group->getID() ?>,
		    start: 0,
		    rows: 50
		  },
		  dataType: "json",
		  success: function(json) {
		  	// remove old results
		  	$('#result-list').children().remove();

				// parse response
		  	var response = json.response;
		  	
		  	if (response.numFound == 1) {
		  		// redirect to component
		  		var doc = response.docs[0];
					var group_id = doc.group_id;
					var comp_name = doc.component_name;
					var comp_id = doc.id;
					
		  		window.location = '?group_id=' + group_id +'&cid=' + comp_id
		  		
		  	} else {
					$('#result-summary').html(response.numFound + ' results for: "' + query + '".');
		  	
		  		// show list of components
			  	for (var i=0; i<response.numFound; i++) {
						var doc = response.docs[i];
						var group_id = doc.group_id;
						var comp_name = doc.component_name;
						var comp_id = doc.id;
						
						$('#result-list').append('<li><a href="?group_id=' + group_id +'&cid=' + comp_id +'">' + comp_name + '</a></li>');
					}
		  	}
				
				if (query == "") {
					$('#search-results').slideUp(300);
				} else {
					$('#search-results').slideDown(300);
				}
		  }   
		});
	}
	
</script>

<style>
	
	#component-search > input {
		width: 85%;
	}
	
	ul.nav-tree {
		margin-left: 15px;
	}

	.component {
		position: relative;
		line-height:1.6em;
	}

	.component.hasSubList {
		list-style: none;	
	}
	
	.component.active > a{
		font-weight: bold;
	}
	
	.tree-toggle {
		cursor: pointer;
		position: absolute;
		left: -15px;
		font-size: 0.8em;
	}
	
	.center {
		margin-left: 20%;
		width: 50%;
		margin-bottom: 10px;
	}
</style>