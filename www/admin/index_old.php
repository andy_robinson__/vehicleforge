<?php
/**
 * Site Admin main page
 *
 * This pages lists all global administration facilities for the
 * site, including user/group properties editing, maintanance of
 * site meta-information (Trove maps, metadata for file releases),
 * etc.
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) FusionForge Team
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'include/role_utils.php';

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));
$warning_msg = htmlspecialchars(getStringFromRequest('warning_msg'));

site_admin_header(array('title'=>_('Site Admin')));

$abc_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
$abc_count=count($abc_array);
$Layout->col(12,true);
echo '<div class="row"><div class="span6">';
$HTML->heading(_('User Maintenence'),2);
?>
	<ul>
	<li>
<?php
$res=db_query_params ('SELECT count(*) AS count FROM users WHERE status=$1',
	array('A')) ;

$row = db_fetch_array($res);
printf(_('Active site users: <strong>%1$s</strong>'), $row['count']);
?>
	</li>
</ul>
<ul>
	<li><a href="userlist.php"><?= _('Display Full User List/Edit Users');?></a>&nbsp;&nbsp;</li>
	<li>
<?php
	echo _('Display Users Beginning with:').' ';
	$List="";
	for ($i=0; $i < $abc_count; $i++) {
		$List.='<a href="userlist.php?user_name_search='.$abc_array[$i].'">'.$abc_array[$i].'</a>|';
	}
	echo substr($List,0,strlen($List)-1);
?>
	<br />
		<form name="usersrch" action="search.php" method="post">
		<?=_('Search <em>(userid, username, realname, email)</em>');?>:
		<input type="text" name="search" />
		<input type="hidden" name="substr" value="1" />
		<input type="hidden" name="usersearch" value="1" />
		<input type="submit" value="<?php echo _('get'); ?>" />
		</form>
	</li>
    <li><a href="../account/register.php"><?=_('Register a New User');?></a>
    </li>
	<li><a href="userlist.php?status=P"><?=_('Pending users');?></a></li>
</ul>
<?php
if (USE_PFO_RBAC) { 
echo '<h2>'._('Global roles and permissions').'</h2>
<ul>
<li>
<form action="globalroleedit.php" method="post"><p>'.global_role_box('role_id').'&nbsp;
<input type="submit" name="edit" value="'._("Edit Role").'" /></p></form>
</li>
<li>
<form action="globalroleedit.php" method="post"><p>
<input type="text" name="role_name" size="10" value="" />
&nbsp;<input type="submit" name="add" value="'._("Create Role").'" /></p></form>
</li></ul>';
}
?>
</div><div class="span6">
<?php
$HTML->heading(_('Projects'),2);
?>
<!--<a href="update_models.php">Update Model DB</a>-->
<ul>
	<li><?php
		$res=db_query_params ('SELECT count(*) AS count FROM groups',array()) ;

		$row = db_fetch_array($res);
		printf(_('Registered projects: <strong>%1$s</strong>'), $row['count']);
	?></li>
	<li><?php
		$res=db_query_params ('SELECT count(*) AS count FROM groups WHERE status=$1',array('A')) ;

		$row = db_fetch_array($res);
		printf(_('Active projects: <strong>%1$s</strong>'), $row['count']);
	?></li>
	<li><?php
		$res=db_query_params ('SELECT count(*) AS count FROM groups WHERE status=$1',array('P')) ;

		$row = db_fetch_array($res);
		printf(_('Pending projects: <strong>%1$s</strong>'), $row['count']);
	?></li>
</ul>
<ul>
	<li><a href="grouplist.php"><?=_('Display Full Project List/Edit Projects'); ?></a></li>

	<li>
	<?=_('Display Projects Beginning with:').' ';
	$List="";
	for ($i=0; $i < $abc_count; $i++)
		$List.='<a href="grouplist.php?group_name_search='.$abc_array[$i].'">'.$abc_array[$i].'</a>|';
	echo substr($List,0,strlen($List)-1);
?>
	<br />
		<form name="gpsrch" action="search.php" method="post">
		<?= _('Search <em>(groupid, project Unix name, project full name)</em>'); ?>:
		<input type="text" name="search" />
		<input type="hidden" name="substr" value="1" />
		<input type="hidden" name="groupsearch" value="1" />
		<input type="submit" value="<?= _('Search'); ?>" />
		</form>
	</li>
</ul>
<ul>
	<li><?=util_make_link ('/register/',_('Register New Project')); ?></li>
	<li><a href="approve-pending.php"><?= _('Pending projects (new project approval)'); ?></a></li>
	<li><form name="projectsearch" action="search.php">
	<?= _('Projects with status'); ?>
	<select name="status">
			<option value="A"><?=_('Active (A)'); ?></option>
			<option value="H"><?=_('Hold (H)'); ?></option>
			<option value="P"><?=_('Pending (P)'); ?></option>
	</select>
	<input type="hidden" name="groupsearch" value="1"/>
	<input type="hidden" name="search" value="%"/>
	<input type="submit" value="<?=_('Submit');?> "/>
	</form></li>
	<li><a href="search.php?groupsearch=1&amp;search=%&amp;is_public=0"><?=_('Private Projects'); ?></a></li>
</ul>
</div>
</div>
<div class="row">
    <div class="span6">
<?php
$HTML->heading(_('News'),2);
?>
<ul>
	<li><?=util_make_link ('/admin/pending-news.php',_('Pending news (moderation for front-page)')); ?></li>
</ul>

<h2><?=_('Stats'); ?></h2>
<ul>
	<li><?=util_make_link ('/stats/',_('Site-Wide Stats')); ?></li>
</ul>

<h2><?= _('Trove Project Tree'); ?></h2>
<ul>
	<li><a href="trove/trove_cat_list.php"><?=_('Display Trove Map'); ?></a></li>
	<li><a href="trove/trove_cat_add.php"><?=_('Add to the Trove Map'); ?></a></li>
</ul>
</div>
    <div class="span6">
<h2><?= _('Site Utilities'); ?></h2>
<ul>
	<li><a href="massmail.php"><?php printf(_('Mail Engine for %1$s Subscribers'), forge_get_config ('forge_name')); ?></a></li>
	<li><a href="unsubscribe.php"><?=forge_get_config ('forge_name')._('Site Mailings Maintenance'); ?></a></li>
	<li><a href="edit_frs_filetype.php"><?=_('Add, Delete, or Edit File Types'); ?></a></li>
	<li><a href="edit_frs_processor.php"><?=_('Add, Delete, or Edit Processors'); ?></a></li>
	<li><a href="edit_theme.php"><?=_('Add, Delete, or Edit Themes'); ?></a></li>
	<li><a href="<?=util_make_url ('/stats/lastlogins.php'); ?>"><?=_('Last Logins'); ?></a></li>
	<li><a href="cronman.php"><?=_('Cron Manager'); ?></a></li>
	<li><a href="pluginman.php"><?= _('Plugin Manager'); ?></a></li>
	<li><a href="configman.php"><?=_('Config Manager'); ?></a></li>
	<li><a href="pi.php">PHPinfo()</a></li>
	<?php plugin_hook("site_admin_option_hook", false); ?>
</ul>
<?php


if(forge_get_config('use_project_database') || forge_get_config('use_project_vhost') || forge_get_config('use_people')) {
	echo '<ul>';
	if(forge_get_config('use_project_vhost')) 
		echo '<li><a href="vhost.php">'._('Virtual Host Admin Tool').'</a></li>';
	if(forge_get_config('use_project_database'))
		echo '<li><a href="database.php">'._('Project Database Administration').'</a></li>';
	if(forge_get_config('use_people'))
        echo '<li><a href="'.util_make_url ('/people/admin/').'">'._('Job / Categories Administration').'</a></li>';
	echo '</ul>';
}
echo '</div></div>';
        $Layout->endcol();
site_admin_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
