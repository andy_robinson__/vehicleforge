<?php
/**
 *  quickpass.php
 * 	author: Junrong Yan
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;


require_once ('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once $gfwww . 'admin/admin_utils.php';
require_once $gfwww . 'project/admin/project_admin_utils.php';
use_stylesheet ( '/admin/admin.css' );

session_require_global_perm ( 'forge_admin' );

//Purpose: approve project
//Given:   group_id
//Return:  if this project has been approved, then return true. Otherwise, return false.

function update_user_status(&$user, $new_status) {
	global $feedback;
	global $error_msg;
	if (! $user || ! is_object ( $user )) {
		exit_error(_('Could Not Get User'),'admin');
		return false;
	} else if ($user->isError ()) {
		exit_error ( $user->getErrorMessage (), 'admin' );
		return false;
	}

	db_begin();
	if (!$user->setStatus($new_status)) {
		$error_msg .= $group->getErrorMessage();
		db_rollback();
		$error_msg .= sprintf ( _ ( 'Error when change status of User: %1$s' ), $user->getUnixName () ) . '<br />';
		return false;
	}else{
		$feedback .= sprintf ( _ ( 'Successfully change the status of User: %1$s' ), $user->getUnixName ());
	}
	db_commit();
	$feedback .= _('Updated ').$user->getUnixName()._('status to ').$new_status;
	return true;
}

if (getStringFromRequest ( 'change_status', false )) {
	$user_id = getIntFromRequest ('user_id');
	//echo $user_id;
	$user = user_get_object ($user_id );
	$new_status = getStringFromRequest ($user_id);
	//echo $new_status;
	update_user_status($user,$new_status);
	
}	
	




//Purpose: once click reject button, show the hidden textarea
//         & change button color to red.
$JS='$(function(){
    $("body").on("change","select",function(e){
		var btn_id=e.target.name;
			
        var me=document.getElementById(btn_id);
		var s=me.value;	
		if(me.className!= "btn btn btn-success"){		     
		     me.className="btn btn btn-success";
		     return false;
     }
})
});';
add_js ( $JS );


//Purpose: once change the task option of drop-list, update the page and
//         reload the content of the request.
$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})        
   
});';
add_js($JS_dropdown_list);


//Purpose: once change the sort option of drop-list, updage the page and
//         reload the content of the request..
$JS_sort_by = '$(function(){
    $("#sort_by").change(function(){
		var $new_rank_page=$(this).val();
		location.href=$new_rank_page;
})

});';
add_js($JS_sort_by);
			

site_admin_header ( array ('title' => _ ( 'Quick Pass Reject News' )), 0 );
echo '<div class="page">
		<div class="row">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="index">Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news" selected>Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form action="quickpass_reject_news.php" method="post">
							<div class="input-append">
                				<input id="search_query" size="16" name="search_key" type="text" style="width: 108px;">
								<input type="hidden" name="groupsearch" value="1" />
								<button class="btn btn-primary" type="submit">Search</button>
            				</div>
						</form>
					</div>
				</div> 		
';


site_admin_footer(array());
?>
                      