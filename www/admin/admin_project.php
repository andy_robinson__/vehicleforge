<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'include/html.php';
require_once $gfwww.'include/role_utils.php';
//use_stylesheet ( '/admin/admin.css' );

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));
$warning_msg = htmlspecialchars(getStringFromRequest('warning_msg'));



$HTML->addStylesheet('admin.css');
//site_admin_header(array('title'=>'Quick Pass'));
site_admin_header(array('title'=>_('Project')), 1);
$Layout->col(12,true);


?>
<div class="widget">
	<div class="widget-header">
		<form name="gpsrch" action="admin_project.php" method="post" >
		      <input type="text" name="search_key"  placeholder="search..." class="form-control" style="float:left; width:90%;"/>
		      <input type="hidden" name="groupsearch" value="1" />
		      <button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
	<div class="widget-body">
	<div class="table-responsive">
		<table class="table table-hover" singleSelect="true">
    		
    		<?php 
    		function  Property($project){
    			if($project->isPublic()){
    				return "Public";
    			}
    			return "Private";
    		}
    		
    		$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='D'", array ());
    		$rows = db_numrows( $res_grp );

    		echo '<tr>	  
					<td><b>Project Name</b></td>
		            <td><b>Project Admin</b></td>
  					<td><b>Project Status</b></td>
  					<td><b>Created Time</b></td>
  					<td><b>Project Property</b></td>
				 </tr>';
    		if($rows){
    		while($row_grp = db_fetch_array($res_grp)){
			if($row_grp['user_id']!=null){
    			$u = user_get_object($row_grp['user_id'] );
    		
    		$project = group_get_object($row_grp['group_id'] );
    		
    		    echo '<tr>';
    			echo '<td>'.$row_grp['group_name'].'</td>';
    			echo '<td>'.$u->getUnixName().'</td>';
    			echo '<td>'.$project->getStatus().'</td>';
    			echo '<td>'.date('Y-m-d H:i',$row_grp['register_time']).'</td>';
    			echo '<td>'.Property($project).'</td>';
    			echo '</tr>';
    			}
    		}
    		}
    		?>
  				
  				
			
			
  		</table>
	</div>
	<table class="table table-bordered">
		<tr>
			<td>Template Project</td>
		</tr>
		<tr>
			<td>Project ID</td>
			<th rowspan="11">Picture</th>
		</tr>
		<tr>
			<td>Project Name</td>
		</tr>
		<tr>
			<td>Project Admin</td>
		</tr>
		<tr>
			<td>Created Time</td>
		</tr>
		<tr>
			<td>Description</td>
		</tr>
		<tr>
			<td>Project Status</td>
		</tr>
		<tr>
			<td>Set as Public?</td>
		</tr>
		<tr>
			<td>Enable/Disable</td>
		</tr>
		<tr>
			<td>Set as Template?</td>
		</tr>
		<tr>
			<td>HTTP Domain:</td>
		</tr>
		<tr>
			<td>I want to delete the project</td>
		</tr>
		<tr>
			<td></td>
			<td>button button</td>
		</tr>
 
	</table>
	<hr />
	<div style="width:100%; height:auto; clear:both; margin-bottom: 50px;">
	<textarea class="form-control" style="width :98%;" rows="3"></textarea>
	<input  type="submit" name="post_comments" class="btn" value="send"/>
	</div>
	<hr />
	<table class="table table-bordered">
		<tr>
			<th rowspan="2">Picture</th>
			<td style="width:70%;">Selenium Tester</td>
			<td>Sep 22,2014 at 7:01am</td>
		</tr>
		<tr>
			<td>Project ID</td>
			<td>
				<input type="submit" name="delete_comment" class="btn" value="Delete"/>
				<input style="float:right;" type="submit" name="reply_comment" class="btn" value="Reply"/>
			</td>
		</tr>
	</table>
	
	</div> 
</div>



<?php
// user_get_object(id)
// group_get_object(id)
// comon include
// GF User
// GF Group

$Layout->endcol();
include $gfwww.'help/help.php';
site_admin_footer(array());
?>