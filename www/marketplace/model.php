<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once($gfwww.'include/model.php');
require_once($gfwww . 'include/LayoutGen.php');
$Model=new model(0);
$Layout=new LayoutGen;

function strToID($String){
	return str_replace(" ","_",$String);
}

//session_require_perm ('project_read', $group_id) ;
$ModelID=getIntFromRequest("mid");
$ModelData=$Model->get_def($ModelID);
$HasModels=false;

foreach ($ModelData->outParams as $i){
	if ($i->name=="VRML file"){
		$HasModels=true;
		break;
	}
}
//print_r($ModelData);

$GroupID=getIntFromRequest("group_id")?getIntFromRequest("group_id"):1;
site_header(Array('title'=>_('Model: '.$ModelData->modelDef->name)));

?>
<style>
#model_nav, #filler	{
	width: 150px;
	margin: -4px 0 -4px -4px;
	float: left;
}

#model_nav li	{
	list-style: none;
	height: 25px;
/*
	border-style: solid;
	border-color: #BEBEBE;
	border-width: 0 1px 1px 0;
*/
	font-size: 18px;
	cursor: pointer;
	padding: 4px;
	color: #999;
/* 	background: #FFF; */
}

#model_nav .current	{
	color: #309CD6;
	border-right: 0;
}

/*
#model_content  {
	margin-left: 155px;
}

#filler	{
	clear:both;
	margin: 4px 0 0 -5px;
	border-right: 1px solid #BEBEBE;
}
*/

.module .content {
	padding-bottom: 0;
}
</style>
<script>
function strToID(Str){
	return Str.replace(/ /g,"_");
}

$(function(){
var h2_height=$('#content h2').height()+9;
$('#filler').height($('#attr').height()-$('#model_nav').height()+h2_height);

$('#run').click(function(){
	var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(){
		$(this).prop('disabled',true);
		Data[$(this).data('name')]=$(this).val();
	});

	var $me=$(this);

	$me.prop('disabled',true);

	Data=JSON.stringify(Data);
	$.post('model_run.php',{'modelID':<?=$ModelID;?>,'data':Data},function(msg){
		msg=$.parseJSON(msg);
		var outParams=msg.pkg[0].outParams;
		for(var i in outParams){
			if (outParams[i].type=="File"){
				if (outParams[i].name=="VRML file"){
					$('#3dModel').attr('src','<?=$DOME_URL?>'+outParams[i].value);
				}
			}else{
				var $e=$('#'+strToID(outParams[i].name));
				if (outParams[i].type=="Vector"){
					console.log(outParams[i].value);
					//var Vectors=outParams[i].value.split(',');
					var size=outParams[i].value.length;
					var HTML='<textarea id="'+strToID(outParams[i].name)+'" cols="23" rows="'+(size-1)+'">';
					for(var ii=0;ii<size;ii++){
						HTML+=outParams[i].value[ii]+",";
						if (ii>=size-1)
							HTML=HTML.substr(0,HTML.length-1);
						else
							HTML+="\n";
					}
					HTML+='</textarea>';
					$e.replaceWith(HTML);
				}else{
					$e.val(outParams[i].value);
				}
			}
		}

		$Inputs.each(function(){
			$(this).prop('disabled',false);
		});

		$me.prop('disabled',false);
	});
});

$('#model_nav li').click(function(){
	var $me=$(this),
	$cur=$('#model_nav li.current'),
	curview=$cur.data('view'),
	myview=$me.data('view');
	if (curview!=myview){
		$cur.removeClass('current');
		$me.addClass('current');
		$('#filler').animate({'height':$('#'+myview).height()-$('#model_nav').height()+h2_height});
		$('#'+curview).slideUp(function(){
			$('#'+myview).slideDown();
		});
	}
});
});
</script>
<?php
$Layout->col(3,true);
?>

<div id="model_nav">
	<ul>
	<li class="current" data-view="attr">Attributes</li>
	<li data-view="desc">Description</li>
	<!--<li data-view="graphs">Graphs</li>-->

<?php
if ($HasModels)
	echo '<li data-view="3d">3D Models</li>';
?>
	</ul>
</div>
<?php
$Layout->endcol()->col(13);
?>
<!-- <div id="filler"></div> -->
<div id="model_content">
	<?='<h2>'.$ModelData->modelDef->name.'</h2>'; ?>
	<div id="attr" class="view">
		<div id="inputs">
			<h3>Inputs</h3>
<?php
foreach ($ModelData->inParams as $i){
	echo '<div><label>'.$i->name.'</label><input type="text" value="';
	switch($i->type){
		case 'Vector':
			echo $Model->format_value($i->value,"VectorString");
			break;

		case 'Matrix':
			echo $Model->format_value($i->value,"MatrixString");
			break;

		default:
			echo $Model->format_value($i->value,$i->type);
	}
	echo '" class="input" data-name="'.$i->name.'" /><span class="xput_type">';
	if ($i->unit!='no unit')echo $i->unit;
	echo '  ('.$i->type.')</span></div>';
}
?>
		</div>
		<button id="run" class="btn primary">Run Model</button><br /><br />
		<div id="outputs">
			<h3>Outputs</h3>
<?php
foreach ($ModelData->outParams as $i){
	echo '<div';
	if ($i->type=="File")echo ' class="hide"';
	echo '><label>'.$i->name.'</label> ';
	if ($i->type=="Vector"){
		echo '<textarea cols="23" rows="'.(sizeof($i->value)-1).'"';
	}else{
		echo '<input';
	}

	echo ' autocomplete="false" readonly="readonly" id="'.strToID($i->name).'"';

	switch ($i->type){
		case 'Vector':
			echo '>'.$Model->format_value($i->value,"VectorString").'</textarea>';
			break;

		case 'Matrix':
			echo ' value="'.$Model->format_value($i->value,"MatrixString").'" />';
			break;

		default:
			echo ' value="'.$Model->format_value($i->value,$i->type).'" />';
	}

	 echo '<span class="xput_type">';
	if ($i->unit!='no unit')echo $i->unit;
	echo '  ('.$i->type.')</span></div>';
}
?>
		</div>
	</div>
	<div id="desc" class="view hidden"><?=$ModelData->modelDef->desc;?></div>
	<div id="graphs"  class="view hidden">Graphs</div>
	<div id="3d"  class="view hidden">
		<embed id="3dModel" src="plate.wrl" type="application/x-cortona" pluginspage="http://www.cortona3d.com/cortona" width="640" height="480"
		       ContextMenu="false"
		       vrml_background_color="#000077"
		       vrml_dashboard="true"
		       ShowFps="true"
		       AnimateViewports="true"
		       NavigationMode="3"
		       RendererMaxTextureSize="256" />
	</div>
</div><!--end content-->


<?php
$Layout->endcol();
site_footer(array());
?>

