<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
include($gfwww.'include/model.php');
$Model=new model(0);

function toArray($data) {
    if (is_object($data)) $data = get_object_vars($data);
    return is_array($data) ? array_map(__FUNCTION__, $data) : $data;
}

$ModelData=toArray($Model->get_def($_POST['modelID'])); //$_POST['modelDef'];
//print_r($ModelData);
$ModelData['modelDef']['dateModified']=$Model->format_value($ModelData['modelDef']['dateModified'],'Int');
$Data=json_decode($_POST['data'],true);
//print_r($Data);

$Size=sizeof($Data);
$InParams=$ModelData['inParams'];
for($i=0;$i<$Size;$i++){
	while ($DataParam=pos($Data)){
		//echo key($Data).'='.$InParams[$i]['name'];

		if (key($Data)==$InParams[$i]['name']){
			//If keys match, format values correctly and change to user-inputted value
			//Removes quotes from ints if necessary
			//Changes string vectors/matrices to arrays
			$InParams[$i]['value']=$Model->format_value($DataParam,$InParams[$i]['type']);
		}

		next($Data);
	}
	reset($Data);
}

//print_r($InParams);

//Go through all outParams to convert them to correct format
/*for($i=0;$i<count($ModelData['outParams']);$i++){
	if ($ModelData['outParams'][$i]['type']=="Vector"){
		$Vector=$ModelData['outParams'][$i]['value'];
		for($ii=0;$ii<count($Vector);$ii++){
			//Convert all values to floats
			//$ModelData['outParams'][$i]['value'][$ii]=floatval($Vector[$ii]);
		}
	}
}*/

//Replace old inParams with user-inputted inParams
$ModelData['inParams']=$InParams;

$PKGData=str_replace('\/','/',json_encode($ModelData));
//echo $PKGData;
//$PKGData=substr($PKGData,1,strlen($PKGData)-2);

//{"modelDef":{"name":"service resputation calculator","guid":"1","desc":"a model that combines a user community trust score with a validation score from the curator oft the model portal","dateShared":1311051600000},"inParams":[{"name":"community trust score","type":"real","unit":"none","val":"12"},{"name":"curator validation score","type":"real","unit":"none","val":"34"}],"outParams":[{"name":"service reputation score","type":"real","unit":"none","val":"1"}]}

echo $Model->run($PKGData);
?>
