<?php
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$Model1=array(
	'inputs'=>array(
		'Air Pressure'=>'psi',
		'Weight'=>'kg',
		'Volume'=>'m3'
	),
	'outputs'=>array(
		'Force'=>'N',
		'Air Pressure'=>'psi'
	)
);

$Model2=array(
	'inputs'=>array(
		'Air Pressure1'=>'psi',
		'Weight'=>'kg',
		'Force'=>'N',
		'Volume'=>'m3',
		'Air Pressure2'=>'psi'
	),
	'outputs'=>array(
		'Power'=>'W'
	)
);

$Units=array();
foreach ($Model1['inputs'] as $name=>$unit)
	if (!in_array($unit,$Units))
		$Units[]=$unit;

foreach ($Model1['outputs'] as $name=>$unit)
	if (!in_array($unit,$Units))
		$Units[]=$unit;

foreach ($Model2['inputs'] as $name=>$unit)
	if (!in_array($unit,$Units))
		$Units[]=$unit;

foreach ($Model2['outputs'] as $name=>$unit)
	if (!in_array($unit,$Units))
		$Units[]=$unit;

$JS='$(function(){
	var links=[],
	lines=[];

	$("#filter").change(function(){
		var unit=$(this).val();
		if (unit=="*"){
			$(".units li").slideDown(function(){
				updateLines();
			});

			for(var i in lines)
				lines[i].show();
		}else{
			//Hide all lines - may have to redraw some
			for(var i in lines)
				lines[i].hide();

			//Hide filtered units
			$(".units li[data-unit!=\'"+unit+"\']").slideUp(function(){
				//Show unfiltered units
				$(".units li[data-unit=\'"+unit+"\']").slideDown(function(){
					updateLines();
				});

				//Show lines that have not been filtered
				var $e;
				for(var i in links){
					$e=$("#"+links[i]);
					if ($e.data("unit")==unit){
						lines[i].show();
					}
				}
			});
		}
	});

	$("li.ui-draggable").draggable({
		revert:"invalid",
		cursor:"move",
		helper:"clone",
		opacity:0.7
	});

	$("li.ui-droppable").droppable({
		greedy: true,
		accept: function(draggable){
			var myunit=$(this).data("unit"),
				droppableunit=draggable.data("unit");
			if (myunit==droppableunit){
				$(this).css("background","#6F4");
				return true;
			}else{
				return false;
			}
		},
		deactivate: function(ev, ui){
			$("li.ui-droppable").css("background","#FFF");
		},
		drop: function(ev, ui){
			var $draggable=$(ui.draggable);
			$draggable.draggable("widget");
			links.push([$draggable.attr("id"), $(this).attr("id")]);
			lines.push(drawLine(getLeftLinePoint($draggable), getRightLinePoint($(this)), paper));
		}
	});

	function getLeftLinePoint(Object){
		var x=0,
			y=Object.position().top-paperPos.y+Object.height()/2;

		return {x: x, y: y};
	}

	function getRightLinePoint(Object){
		var x=paperDim.width,
			y=Object.position().top-paperPos.y+Object.height()/2;

		return {x: x, y: y};
	}

	function drawLine(Pos1, Pos2, Context){
		//console.log(Pos1, Pos2);
		return Context.path("M"+Pos1.x+" "+Pos1.y+"+ L"+Pos2.x+" "+Pos2.y);
	}

	function updateLines(){
		var p1, p2, path;
		for(var i in lines){
			p1=getLeftLinePoint($("#"+links[i][0]));
			p2=getRightLinePoint($("#"+links[i][1]));
			//path="M"+p1.x+" "+p1.y+" L"+p2.x+" "+p2.y;
			//console.log(path);
			lines[i].animate({
				path: "M"+p1.x+" "+p1.y+" L"+p2.x+" "+p2.y
			}, 500, "linear");
		}
	}

	var $middle=$("#middle"),
		paper=Raphael($middle.position().left, $middle.position().top+$("#filter_container").height(), $middle.outerWidth(true)+parseInt($middle.css("margin-left")), $(".page").height());
		$paper=$("svg"),
		paperPos={x: $paper.position().left, y: $paper.position().top},
		paperDim={width: $paper.width(), height: $paper.height()};
});';
add_js($JS);

use_javascript('jquery-ui-1.8.16.custom.min.js');
use_javascript('raphael.js');

site_header();
?>
<style>
ul.units li  {
	list-style: none;
	height: 30px;
	border: 1px solid #000;
	border-bottom: 2px solid #000;
	margin-bottom: 5px;
	background: #FFF;
	padding: 3px;
	margin-left: -25px;
}

.ui-draggable    {
	cursor: pointer;
}

.ui-droppable   {
	cursor: not-allowed;
}
</style>
<div class="row page">
	<div class="span4">
		<h2>Model 1</h2>
		<strong>Inputs</strong>
		<ul class="units_model1">
<?php
foreach ($Model1['inputs'] as $name=>$unit)
	echo '<li data-unit="'.$unit.'">'.$name.' ('.$unit.')</li>';
echo '</ul>
<hr />
<strong>Ouputs</strong>';

echo '<ul class="units">';
foreach ($Model1['outputs'] as $name=>$unit)
	echo '<li data-unit="'.$unit.'" class="ui-draggable model1" id="m1_'.rand(0,100).'">'.$name.' ('.$unit.')</li>';
echo '</ul>';
?>
	</div>
	<div class="span3" id="middle">
<?php



echo '<div id="filter_container">Filter: <select id="filter" autocomplete="off">
	<option selected value="*">All Units</option>';
foreach ($Units as $i)
	echo '<option value="'.$i.'">'.$i.'</option>';
echo '</select></div>';


?>
</div><div class="span4">
<?php
echo '<h2>Model 2</h2>
<strong>Inputs</strong>
<ul class="units">';
foreach ($Model2['inputs'] as $name=>$unit)
	echo '<li data-unit="'.$unit.'" id="m2_'.rand(0,100).'" class="ui-droppable model2">'.$name.' ('.$unit.')</li>';
echo '</ul>
<hr />
<strong>Ouputs</strong>';

echo '<ul class="units_model2">';
foreach ($Model2['outputs'] as $name=>$unit)
	echo '<li data-unit="'.$unit.'">'.$name.' ('.$unit.')</li>';
echo '</ul>';
?>
	</div>
</div>
<?php
site_footer();
?>