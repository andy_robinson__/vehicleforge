<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('new-marketplace_style.css');
//use_javascript('bootstrap/transition.js');
use_javascript('bootstrap/collapse.js');
use_javascript('bootstrap/button.js');
site_header(array('title'=>'Vehicle Forge Marketplace'));

$Layout->col(3,true);
?>

<script>
    $(function(){
        $(".collapse").collapse();
        $('.collapse').on('hide', function () {
            $(this).prev().removeClass('out');
        });
        $('.collapse').on('show', function () {
            $(this).prev().addClass('out');
        });

        $('#show-list').click(function(){
            $('#list').show();
            $('#thumbnails').hide();
        });

        $('#show-thumbs').click(function(){
            $('#thumbnails').show();
            $('#list').hide();
        });

    });
</script>

    <div class="container">
            <div class="row">
                <div class="span12">
                    <button type="submit" class="btn btn-success btn-large pull-right" style="margin-top: 9px;"><i class="icon-share icon-white"></i> Share Component</button>
                    <h2 style="color:#08C">Welcome to the Marketplace!</h2>
                    <h4 style="margin-bottom: 10px;">your one-stop shop for shared blueprints, models, concepts & code</h4>

                </div>
            </div>
            <div class="row">
                <div class="span12">
                    <form class="well form-search" style="text-align:center;">
                        <label for="searchQuery"><strong>What are you looking for?</strong></label>
                        <!-- TODO: Search components for keywords -->
                        <div class="input-append">
                            <input class="span6" id="appendedInputButton" size="16" type="text" style="height:27px"><button class="btn btn-primary btn-large" type="button"><i class="icon-search icon-white"></i> Search</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="row">
                <div class="span3">
					<style>
						.sub    {
							list-style: none;
							margin-left: 10px;
						}
						.nav-list > li > a.section {
							background-image: url('http://107.21.100.110/themes/gforge/images/expand.png');
							background-repeat: no-repeat;
							background-position: 4px 7px;
						}
						
						.nav-list > li > a.section.active {
							background-image: url('http://107.21.100.110/themes/gforge/images/collapse.png');
							background-repeat: no-repeat;
							background-position: 4px 7px;
						}
						.nav .nav-header {
							padding: 3px 0px;
						}
						.well hr {
							margin: 12px 0 5px;
							border-top: 1px solid #DDD;
						}
					</style>
					<div class="well">
                        <h6>Component Tags</h6>
                            <ul id="tag-cloud">
                                <li><a href="#" class="tag4">CAD model</a></li>
                                <li><a href="#" class="tag3">Java</a></li>
                                <li><a href="#" class="tag5">script</a></li>
                                <li><a href="#" class="tag7">simulation</a></li>
                                <li><a href="#" class="tag2">MathLab</a></li>
                                <li><a href="#" class="tag2">automotive</a></li>
                                <li><a href="#" class="tag5">formula</a></li>
                                <li><a href="#" class="tag3">prototyping</a></li>
                                <li><a href="#" class="tag4">visualization</a></li>
                                <li><a href="#" class="tag3">iFab</a></li>
                                <li><a href="#" class="tag5">signals</a></li>
                                <li><a href="#" class="tag2">concept</a></li>
                                <li><a href="#" class="tag7">mechanical</a></li>
                                <li><a href="#" class="tag2">rotor</a></li>
                                <li><a href="#" class="tag5">hinges</a></li>
                                <li><a href="#" class="tag3">data</a></li>
                            </ul>
						<hr />
                        <!-- TODO: Add dynamic generation of categories? -->
						<h6>Filter Components</h6>
                        <div id="accordion-filters">
                            <a class="accordion-toggle out" data-toggle="collapse" href="#collapseCategory">
                                Domain Area
                            </a>
                            <div id="collapseCategory" class="accordion-body collapse" style="height: auto; ">
                                <!-- Mockup Data -->
                                <a href="#">All              <span class="badge light pull-right">102</span></a>
                                <a href="#">Electrical       <span class="badge light pull-right">1</span></a>
                                <a href="#">hydraulic         <span class="badge light pull-right">14</span></a>
                                <a href="#">Mechanical       <span class="badge light pull-right">61</span></a>
                                <a href="#">Thermal          <span class="badge light pull-right">6</span></a>
                                <a href="#">Electromagnetic  <span class="badge light pull-right">3</span></a>
                                <a href="#">Aero             <span class="badge light pull-right">1</span></a>
                                <a href="#">Software         <span class="badge light pull-right">32</span></a>
                                <!-- end Mockup Data -->
                            </div>

                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseType">
                                Category
                            </a>
                            <div id="collapseType" class="accordion-body collapse in" style="height: auto; ">
                                <!-- Mockup Data -->
                                <a href="#">All              <span class="badge light pull-right">102</span></a>
                                <a href="#">Concept          <span class="badge light pull-right">82</span></a>
                                <a href="#">META Model       <span class="badge light pull-right">4</span></a>
                                <a href="#">iFab Model       <span class="badge light pull-right">6</span></a>
                                <a href="#">Visualization    <span class="badge light pull-right">2</span></a>
                                <a href="#">Mathlab Script   <span class="badge light pull-right">2</span></a>
                                <!-- end Mockup Data -->
                            </div>

                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseFunction">
                                Function
                            </a>
                            <div id="collapseFunction" class="accordion-body collapse in" style="height: auto; ">
                                <!-- Mockup Data -->
                                <a href="#">All                 <span class="badge light pull-right">102</span></a>
                                <a href="#">Rapid Prototyping   <span class="badge light pull-right">1</span></a>
                                <a href="#">Statistics          <span class="badge light pull-right">14</span></a>
                                <a href="#">Control Systems     <span class="badge light pull-right">61</span></a>
                                <a href="#">Data Acquisition    <span class="badge light pull-right">6</span></a>
                                <a href="#">Signal processing   <span class="badge light pull-right">3</span></a>
                                <a href="#">Test/Measurement    <span class="badge light pull-right">1</span></a>
                                <a href="#">CAD Geometry        <span class="badge light pull-right">32</span></a>
                                <a href="#">Analytics        <span class="badge light pull-right">2</span></a>
                                <!-- end Mockup Data -->
                            </div>

                        </div>
					</div>
					
                </div> <!-- end col-->
                <div class="span9">
                    <hr>
                    <div class="btn-toolbar">
                        <div class="btn-group pull-left" data-toggle="buttons-radio">
                            <button class="btn active">Most Popular</button>
                            <button class="btn">Recently Added</i></button>
                            <button class="btn">My Projects</button></i></button>
                        </div>
                        <div class="btn-group pull-right" data-toggle="buttons-radio">
                            <button class="btn active" id="show-thumbs"><i class="icon-th-large"></i></button>
                            <button class="btn" id="show-list"><i class="icon-th-list"></i></button>
                        </div>
                    </div>
                    <hr>
                    <ul class="thumbnails" id="thumbnails">
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                        <li class="col4">
                            <a href="components/index.php" class="thumbnail component">
                                <img src="http://placehold.it/150x103" alt="" />
                                <h4>Model Name</h4>
                                <p><small><i>Creator's Name</i> <br /> 47 subscribers | 3 comments</small></p>
                            </a>
                        </li>
                    </ul>
                    <div id="list" class="hidden">
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                        <div class="feature-block">
                            <img class="pull-left" src="http://107.21.100.110/mock_images/drivetrain.jpg" width="50px">
                            <div class="pull-left description">
                                <h4><a href="">Model Name</a></h4>
                                <p><small><i>Creator's Name</i> | updated May 29, 2012 12PM | 47 subscribers | 3 comments</small><br/>
                                    <small class="description">This is the description of the model.</small></p>
                            </div>
                            <div class="pull-right" style="text-align: center">
                                <a class="btn btn-info" href="">More Info</a><br>
                                <a class="sub-link" href=""><small>Add to Project</small></a>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-->
            </div><!-- end row-->
    </div>

<?php
$Layout->endcol();
site_community_footer();
?>
