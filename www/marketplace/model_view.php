<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once($gfwww . 'marketplace/model_utils.php');
require_once $gfwww . 'include/LayoutGen.php';
$Layout=new LayoutGen();

// Include javascript for rating mechanism
use_javascript('raty/jquery.raty.min.js');
use_javascript('raty.js');

// Include css/less for marketplace //TODO: include this in main CSS
//$HTML->addStylesheet('/themes/gforge/css/marketplace.css');

// Get the model id and associated model instance
$mid = getIntFromRequest('mid');
$model = getModel($mid);

// Create the site header
site_header(array('title'=>$model['name']));

// Create hand column
$Layout->col(3,true);

// Create model description box

echo '<div class="modelImage"><img src="http://placehold.it/150&text=Model Img"/></div>';

$Layout->endcol()->col(9);
echo '<div class="modelText">';


$HTML->heading($model['name'], 2);

echo '<p>' . $model['desc'] . '</p>';
echo '<p>by: ' . $model['author'] . '</p>';

echo '</div>'; // end modelInfo

$HTML->heading("Comments and Reviews", 3);
echo '<p>No one has made a comment or review yet.</p>';


$Layout->endcol()->col(4);


$HTML->heading("Stats", 3);
echo '<div class="raty" style="margin:auto;" data-value="3.5"></div>';

$HTML->heading("Use Service", 3);
?>
<ul>
	<li>
		<button class="btn small" onclick="window.location.href='/marketplace/model.php?mid=<?=$model['id']?>'">Run Service</button>
	</li>
	<li>
		<button class="btn small disabled">Download</button>
	</li>
	<li>
		<button class="btn small disabled">Add to Project</button>
	</li>
</ul>
<?php
$Layout->endcol();

// Create site footer
site_footer(array());
?>