<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

if (isset($_POST['message_body'])){
    $Message=getStringFromRequest('message_body');
    $To=getIntFromRequest('to');

    session_get_user()->sendMessage($To, $Message, getStringFromRequest('subject'));
}

$Title='';
$JS='$(function(){
';

switch(getStringFromRequest('action')){
    case 'view':
        $Message=new UserMessage(getIntFromRequest('id'));
        $Message->markRead(session_get_user()->getID());
        $Title='View Message';
$JS.='$("#reply_toggle").click(function(){
    $(this).hide();
    $("#reply_panel").slideDown();
});

$("#reply_cancel").click(function(){
    $("#reply_toggle").show();
    $("#reply_panel").slideUp();
});

$("#reply_send").click(function(){
    $("#reply_panel").slideUp();
    $.post("ajax/message_reply.php",{mid:'.getIntFromRequest('id').', message: $("#reply_message").val()},function(msg){
        msg=$.parseJSON(msg);
        if (!msg.error){
            var $e=$("<div class=\"hidden\">"+msg.html+"</div>");
            $("#replies").append($e).children(".hidden").slideDown(function(){
                $(this).children(".reply").unwrap();
            });

            $("#reply_toggle").show();
        }
    });
});';
        break;

    case 'inbox':
        $Title='Inbox';
        break;

    case 'compose':
        $Title='Compose a New Message';
        break;
}

$JS.='
});';
add_js($JS);

site_user_header(array('title'=>$Title));
$Layout->col(12,true);

switch(getStringFromRequest('action')){
    case 'compose':
        echo '<a href="?action=inbox" class="btn btn-large">Back to Inbox</a><br /><br />';
        $ToUser=user_get_object(getIntFromRequest('to'));
        $Form=new BsForm();
        echo $Form->init('?action=inbox','post',array('class'=>'form-horizontal'))
            ->head('Compose a New Message')
            ->group('To',
                new Text(array(
                    'value'=>$ToUser->getRealName(),
                    'disabled'=>true
                )),
                new Hidden(array(
                   'value'=>$ToUser->getID(),
                   'name'=>'to'
                ))
            )
            ->group('Subject',
                new Text(array(
                    'name'=>'subject'
                ))
            )
            ->group('Message',
                new Textarea('',array(
                    'name'=>'message_body'
                ))
            )
            ->actions(
                new Submit('Send',array(
                    'class'=>'btn-primary'
                )),
                new Reset()
            )
            ->render();
        break;

    case 'view':
        require_once APP_PATH.'common/include/BsForm.class.php';

        $Message=new UserMessage(getIntFromRequest('id'));
        $User=user_get_object($Message->getFromID());
        echo '<a href="?action=inbox" class="btn btn-large">Back to Inbox</a><br /><br />';
        echo 'From: '.$User->getRealName();
        echo '<h1>'.$Message->getSubject().'</h1>'.$Message->getMessage().'<div id="replies">';
        foreach($Message->getReplies() as $i){
            echo $i->formatReply(session_get_user()->getID());
            $i->markRead(session_get_user()->getID());
        }
        echo '</div>';
?>
        <button id="reply_toggle" class="btn">Reply</button>
        <div id="reply_panel" class="hidden">
            <textarea id="reply_message" cols="30" rows="10"></textarea>
            <button id="reply_send" class="btn btn-primary">Send</button>
            <button id="reply_cancel" class="btn">Cancel</button>
        </div>
<?php
        break;

    case 'inbox':
        echo '<a href="?action=compose" class="btn btn-large">Compose Message</a><br /><br />';
        $Messages=session_get_user()->getMessages(100,null);
        $Table=new BsTable(array('class'=>'table table-striped'));
        $Table->head(array('Title','From','To','Last Message Sent'));

        foreach($Messages as $i){
            $ReplyCount=$i->getUnreadReplyCount(session_get_user()->getID());
            $FromUser=user_get_object($i->getFromID());
            $ToUser=user_get_object($i->getToID());
            $Table->col('<a href="/account/messages.php?action=view&id='.$i->getID().'">'.$i->getSubject().'</a>'.($ReplyCount?' <span class="badge badge-info">+'.$ReplyCount.'</span>':''))
                ->col(($i->getFromID()==session_get_user()->getID())?'You':$FromUser->getRealName())
                ->col(($i->getToID()==session_get_user()->getID())?'You':$ToUser->getRealName())
                ->col(date('M j, Y',$i->getLastMessageSent()).' at '.date('g:ia',$i->getLastMessageSent()));
        }
        echo $Table->render();
        break;
}

$Layout->endcol();
site_user_footer();

db_display_queries();
?>
