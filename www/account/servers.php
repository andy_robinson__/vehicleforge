<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$User=session_get_user();

site_user_header(array('title'=>'My Servers'));
$Layout->col(12,true);
$HTML->tertmenu_add('Account Settings','/account/');
$HTML->tertmenu_add('Public Profile',$User->getURL());
if(forge_get_config('use_people'))
    $HTML->tertmenu_add('Edit Skills','/people/editprofile.php');
$HTML->tertmenu_add('Edit Servers','/account/servers.php');
$HTML->tertmenu_add('Edit Public Keys','/account/publickeys.php');
echo $HTML->tertiary_menu(3);

?>
<script>
$(function(){
    $("#update").click(function(){
        var servers=[];

        $(".server_row").each(function(){
            var $me=$(this);

            servers.push({
                id:$me.data('server_id'),
                alias:$me.children('td').children('.alias').val(),
                url:$me.children('td').children('.url').val()
            });
        });

        servers=JSON.stringify(servers);

        $.post('ajax/server_update.php',{servers:servers},function(msg){

        });
    });
});
</script>
<?php
echo '<table class="table table-striped table-bordered">
<thead>
<tr><th>Server Alias</th><th>Server URL</th><th>Actions</th></tr>
</thead><tbody>';
$Servers=$User->getServers();
foreach($Servers as $i){
    echo '<tr class="server_row" data-server_id="'.$i->getID().'"><td><input type="text" value="'.$i->getAlias().'" class="alias" /></td><td><input type="text" value="'.$i->getURL().'" class="url" /></td><td></td></tr>';
}
echo '</tbody></table>
<button id="update" class="btn btn-primary">Update</button>';

$Layout->endcol();
site_user_footer();
?>