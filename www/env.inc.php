<?php
$START_PAGE_TIME=explode(" ",microtime());
$START_PAGE_TIME=$START_PAGE_TIME[1]+$START_PAGE_TIME[0];

$gfcgfile = '/etc/gforge/local.inc';
$gfconfig = '/etc/gforge/';
$vf_db_config='/etc/gforge/db.inc';
$vf_idp_config='/etc/gforge/idp_vars.php';
$vf_git_config='/etc/gforge/git.inc';
$vf_solr_config='/etc/gforge/solr.inc';
$vf_email_config='/etc/gforge/email.inc';

$vf_messageque_server='tcp://mqserver:port';

define('APP_PATH',dirname(__DIR__).DIRECTORY_SEPARATOR);
define('DB_CONFIG_FILE', $vf_db_config);
define('IDP_CONFIG_FILE', $vf_idp_config);
define('GIT_CONFIG_FILE', $vf_git_config);
define('SOLR_CONFIG_FILE', $vf_solr_config);
define('EMAIL_CONFIG_FILE', $vf_email_config);

require_once APP_PATH.'common/include/env.inc.php';
require_once APP_PATH.'common/include/BsForm.class.php';

$JQUERY_VERSION="1.9.1";
$JQUERYUI_VERSION="1.10.2";
$LESS_VERSION="1.1.5.min";

$Layout=new LayoutGen;
?>
