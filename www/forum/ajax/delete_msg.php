<?php
require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'forum/Forum.class.php';
require_once $gfcommon.'forum/ForumMessage.class.php';

$ForumID=getIntFromRequest("fid");
$MsgID=getIntFromRequest("mid");
$GroupID=getIntFromRequest("gid");

$Group=group_get_object($GroupID);
$Forum=new Forum($Group,$ForumID);

$Msg=new ForumMessage($Forum, $MsgID);

$Return['error']=false;
$Return['msg']='';

if (!$Msg->delete()){
	$Return['error']=true;
	$Return['msg']=$Msg->getErrorMessage();
}

echo json_encode($Return);
?>