<?php
/**
 * Forums Facility
 *
 * Copyright 1999-2001, Tim Perdue - Sourceforge
 * Copyright 2002, Tim Perdue - GForge, LLC
 * Copyright 2010 (c) Franck Villaume - Capgemini
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'forum/ForumHTML.class.php';
/*require_once $gfcommon.'forum/ForumFactory.class.php';
require_once $gfcommon.'forum/Forum.class.php';*/

$group_id = getStringFromRequest('group_id');
$Group=group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

forum_header();
$Layout->col(12,true);
$HTML->tertiary_menu();

echo util_make_breadcrumbs(array(
    'Message Boards Home'       => '/forum/?group_id='.$group_id
));

$Table=new BsTable(array('class'=>'table table-striped'));
$Table->head(array('Forum','Description','Last Activity'));
foreach($Group->getForums() as $i){
    $Table->col($i->makeLink())
        ->col($i->getDescription())
        ->col(date('M d, Y g:ia',$i->getLastActivity()));
}

  echo $Table->render();

$Layout->endcol();
forum_footer();

/*if ($group_id) {
	$g = group_get_object($group_id);
	if (!$g || !is_object($g) || $g->isError()) {
		exit_no_group();
	}

	$ff=new ForumFactory($g);
	if (!$ff || !is_object($ff) || $ff->isError()) {
		exit_error($ff->getErrorMessage(),'forums');
	}

	$farr = $ff->getForums();

	if ( $farr !== false && count($farr) == 1 ) {
		//If only one forum exists, redirect to that forum
        session_redirect('/forum/forum.php?forum_id='.$farr[0]->getID());
	}

	forum_header(array());

	$Layout->col(12,true);
	echo $HTML->tertiary_menu();

	if ($ff->isError()) {
        echo '<div class="error">'. $ff->getErrorMessage().'</div>';
		forum_footer(array());
		exit;
    } else if ( count($farr) < 1) {
		echo '<div class="warning_msg">'.sprintf(_('No Forums Found for %1$s'), $g->getPublicName()) .'</div>';
		forum_footer(array());
		exit;
	}

	//echo '<a href="/forum/myforums.php?group_id=$group_id">'._("My Monitored Forums").'</a><br /><br />';
	*/?><!--
<h1>Forums</h1>
<table class="zebra-striped">
	<tr>
		<th><?/*=_('Forum')*/?></th>
		<th><?/*=_('Description')*/?></th>
		<th><?/*=_('Topics')*/?></th>
		<th><?/*=_('Comments')*/?></th>
		<th><?/*=_('Last Post')*/?></th>
	</tr>
	<?php
/*	for ($j = 0; $j < count($farr); $j++) {
		if (!is_object($farr[$j])) {
			//just skip it - this object should never have been placed here
		} elseif ($farr[$j]->isError()) {
			echo $farr[$j]->getErrorMessage();
		} else {
			switch ($farr[$j]->getModerationLevel()) {
				case 0 : $modlvl = _('No Moderation');break;
				case 1 : $modlvl = _('Anonymous & Non Project Users');break;
				case 2 : $modlvl = _('All Except Admins');break;
			}
			*/?>
			<tr>
				<td><?/*=util_make_link('/forum/forum.php?forum_id='.$farr [$j]->getID(),$farr [$j]->getName())*/?></td>
				<td><?/*=$farr [$j]->getDescription()*/?></td>
				<td><?/*=$farr [$j]->getThreadCount()*/?></td>
				<td><?/*=$farr [$j]->getMessageCount()*/?></td>
				<td><?/*=date(_('M/d/Y @ G:i'),$farr [$j]->getMostRecentDate())*/?></td>
			</tr>
			<?php
/*		}
	}
	*/?>
	</table>
	--><?php
/*
	$Layout->endcol();
	forum_footer(array());
} else {
	exit_no_group();
}*/
?>
