<?php
require_once 'Error.class.php';

class CEMRunning extends Error  {
	private $ID, $Data;

	function __construct($ID=null){
		if ($ID){
			$this->ID=$ID;
			$this->fetch_data();
		}
	}

	private function fetch_data(){
		$Result=db_query_params("SELECT * FROM cem_runnables_running WHERE running_id=$1", array($this->getID()));
		$this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
	}

	function getID(){
		return $this->ID;
	}

	function getQueueURL(){
		return $this->Data['queue_url'];
	}

	function getOwnerID(){
		return (int)$this->Data['owner_id'];
	}

    /**
     * @return mixed
     *
     * @deprecated
     */
	function getRunnableID(){
		return $this->Data['runnable_id'];
	}

    function getInterfaceID(){
        return (int)$this->Data['interface_id'];
    }

	function getStatus(){
        if ($this->Data['finished']=='t'){
            return true;
        }else if ($this->Data['finished']=='f'){
            return false;
        }

        throw new Exception('Cannot get status');
	}

	function setStatus($FinishedStatus){
		db_query_params("UPDATE cem_runnables_running SET finished=$1 WHERE running_id=$2", array($FinishedStatus, $this->getID()));
	}

	static function create($InterfaceID, $UserID, $QueueURL){
        if (!is_int($InterfaceID) || !is_int($UserID) || !is_string($QueueURL)){
            throw new Exception('Check arguments in CEMRun::create');
        }

		if ($Result=db_query_params("INSERT INTO cem_runnables_running(interface_id, owner_id, queue_url) VALUES($1,$2,$3)", array($InterfaceID, $UserID, $QueueURL))){
			return (int)db_insertid($Result,'cem_runnables_running','running_id');
		}else{
			return false;
		}
	}

	/**
	 * @static
	 * @param $RunningID
	 * @return bool
	 *
	 * @deprecated - These should no longed be deleted
	 */
	static function delete($RunningID){
		if (db_query_params("DELETE FROM cem_runnables_running WHERE running_id=$1", array($RunningID))){
			return true;
		}else{
			self::setError('Unabled to delete running cem object');
			return false;
		}
	}

	static function getByUser($UserID){
		$Return=array();
		$Result=db_query_params("SELECT * FROM cem_runnables_running WHERE owner_id=$1 AND NOT finished", array($UserID));
		while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
			$Return[]=new CEMRunning($Row['running_id']);
		}

		return $Return;
	}
}
?>