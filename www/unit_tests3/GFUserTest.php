<?php
require_once 'db_inc.inc';
require_once APP_PATH.'common/include/utils.php';
use Assert as is;

class GFUserTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var GFUser
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new GFUser;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers GFUser::create
     * @todo   Implement testCreate().
     */
    public function testCreate()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::sendRegistrationEmail
     * @todo   Implement testSendRegistrationEmail().
     */
    public function testSendRegistrationEmail()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::delete
     * @todo   Implement testDelete().
     */
    public function testDelete()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }



    /**
     * @covers GFUser::update
     * @todo   Implement testUpdate().
     */
    public function testUpdate()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::fetchData
     * @todo   Implement testFetchData().
     */
    public function testFetchData()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getID
     * @todo   Implement testGetID().
     */
    public function testGetID()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getStatus
     * @todo   Implement testGetStatus().
     */
    public function testGetStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setStatus
     * @todo   Implement testSetStatus().
     */
    public function testSetStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::isActive
     * @todo   Implement testIsActive().
     */
    public function testIsActive()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAdminTaskCount
     * @todo   Implement testGetAdminTaskCount().
     */
    public function testGetAdminTaskCount()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getTaskCount
     * @todo   Implement testGetTaskCount().
     */
    public function testGetTaskCount()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixStatus
     * @todo   Implement testGetUnixStatus().
     */
    public function testGetUnixStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setUnixStatus
     * @todo   Implement testSetUnixStatus().
     */
    public function testSetUnixStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixName
     * @todo   Implement testGetUnixName().
     */
    public function testGetUnixName()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixPasswd
     * @todo   Implement testGetUnixPasswd().
     */
    public function testGetUnixPasswd()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixBox
     * @todo   Implement testGetUnixBox().
     */
    public function testGetUnixBox()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getMD5Passwd
     * @todo   Implement testGetMD5Passwd().
     */
    public function testGetMD5Passwd()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUserPw
     * @todo   Implement testGetUserPw().
     */
    public function testGetUserPw()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getImage
     * @todo   Implement testGetImage().
     */
    public function testGetImage()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getImageHTML
     * @todo   Implement testGetImageHTML().
     */
    public function testGetImageHTML()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getConfirmHash
     * @todo   Implement testGetConfirmHash().
     */
    public function testGetConfirmHash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getEmail
     * @todo   Implement testGetEmail().
     */
    public function testGetEmail()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAbout
     * @todo   Implement testGetAbout().
     */
    public function testGetAbout()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::isSkillsPublic
     * @todo   Implement testIsSkillsPublic().
     */
    public function testIsSkillsPublic()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSubscriptionStatus
     * @todo   Implement testGetSubscriptionStatus().
     */
    public function testGetSubscriptionStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setSubscriptionStatus
     * @todo   Implement testSetSubscriptionStatus().
     */
    public function testSetSubscriptionStatus()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSubscriptions
     * @todo   Implement testGetSubscriptions().
     */
    public function testGetSubscriptions()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSubscriptionsLinks
     * @todo   Implement testGetSubscriptionsLinks().
     */
    public function testGetSubscriptionsLinks()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSubscriptionData
     * @todo   Implement testGetSubscriptionData().
     */
    public function testGetSubscriptionData()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSingleSubscriptionData
     * @todo   Implement testGetSingleSubscriptionData().
     */
    public function testGetSingleSubscriptionData()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::formatSubscriptionContent
     * @todo   Implement testFormatSubscriptionContent().
     */
    public function testFormatSubscriptionContent()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::displaySubscriptionContent
     * @todo   Implement testDisplaySubscriptionContent().
     */
    public function testDisplaySubscriptionContent()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getWorkCount
     * @todo   Implement testGetWorkCount().
     */
    public function testGetWorkCount()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSkills
     * @todo   Implement testGetSkills().
     */
    public function testGetSkills()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getSha1Email
     * @todo   Implement testGetSha1Email().
     */
    public function testGetSha1Email()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getNewEmail
     * @todo   Implement testGetNewEmail().
     */
    public function testGetNewEmail()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getServers
     * @todo   Implement testGetServers().
     */
    public function testGetServers()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getGateway
     * @todo   Implement testGetGateway().
     */
    public function testGetGateway()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setEmail
     * @todo   Implement testSetEmail().
     */
    public function testSetEmail()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setNewEmailAndHash
     * @todo   Implement testSetNewEmailAndHash().
     */
    public function testSetNewEmailAndHash()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getRealName
     * @todo   Implement testGetRealName().
     */
    public function testGetRealName()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getFirstName
     * @todo   Implement testGetFirstName().
     */
    public function testGetFirstName()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getLastName
     * @todo   Implement testGetLastName().
     */
    public function testGetLastName()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAddDate
     * @todo   Implement testGetAddDate().
     */
    public function testGetAddDate()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getTimeZone
     * @todo   Implement testGetTimeZone().
     */
    public function testGetTimeZone()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getCountryCode
     * @todo   Implement testGetCountryCode().
     */
    public function testGetCountryCode()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getShell
     * @todo   Implement testGetShell().
     */
    public function testGetShell()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setShell
     * @todo   Implement testSetShell().
     */
    public function testSetShell()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixUID
     * @todo   Implement testGetUnixUID().
     */
    public function testGetUnixUID()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getUnixGID
     * @todo   Implement testGetUnixGID().
     */
    public function testGetUnixGID()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getLanguage
     * @todo   Implement testGetLanguage().
     */
    public function testGetLanguage()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getJabberAddress
     * @todo   Implement testGetJabberAddress().
     */
    public function testGetJabberAddress()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getJabberOnly
     * @todo   Implement testGetJabberOnly().
     */
    public function testGetJabberOnly()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAddress
     * @todo   Implement testGetAddress().
     */
    public function testGetAddress()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAddress2
     * @todo   Implement testGetAddress2().
     */
    public function testGetAddress2()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getPhone
     * @todo   Implement testGetPhone().
     */
    public function testGetPhone()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getFax
     * @todo   Implement testGetFax().
     */
    public function testGetFax()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getTitle
     * @todo   Implement testGetTitle().
     */
    public function testGetTitle()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getGroups
     * @todo   Implement testGetGroups().
     */
    public function testGetGroups()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getAuthorizedKeys
     * @todo   Implement testGetAuthorizedKeys().
     */
    public function testGetAuthorizedKeys()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setAuthorizedKeys
     * @todo   Implement testSetAuthorizedKeys().
     */
    public function testSetAuthorizedKeys()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setLoggedIn
     * @todo   Implement testSetLoggedIn().
     */
    public function testSetLoggedIn()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::isLoggedIn
     * @todo   Implement testIsLoggedIn().
     */
    public function testIsLoggedIn()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::deletePreference
     * @todo   Implement testDeletePreference().
     */
    public function testDeletePreference()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setPreference
     * @todo   Implement testSetPreference().
     */
    public function testSetPreference()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getPreference
     * @todo   Implement testGetPreference().
     */
    public function testGetPreference()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setPasswd
     * @todo   Implement testSetPasswd().
     */
    public function testSetPasswd()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::usesRatings
     * @todo   Implement testUsesRatings().
     */
    public function testUsesRatings()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getPlugins
     * @todo   Implement testGetPlugins().
     */
    public function testGetPlugins()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::usesPlugin
     * @todo   Implement testUsesPlugin().
     */
    public function testUsesPlugin()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setPluginUse
     * @todo   Implement testSetPluginUse().
     */
    public function testSetPluginUse()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getMailingsPrefs
     * @todo   Implement testGetMailingsPrefs().
     */
    public function testGetMailingsPrefs()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::unsubscribeFromMailings
     * @todo   Implement testUnsubscribeFromMailings().
     */
    public function testUnsubscribeFromMailings()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getThemeID
     * @todo   Implement testGetThemeID().
     */
    public function testGetThemeID()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::setUpTheme
     * @todo   Implement testSetUpTheme().
     */
    public function testSetUpTheme()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getRole
     * @todo   Implement testGetRole().
     */
    public function testGetRole()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getRoles
     * @todo   Implement testGetRoles().
     */
    public function testGetRoles()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::isMember
     * @todo   Implement testIsMember().
     */
    public function testIsMember()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::getURL
     * @todo   Implement testGetURL().
     */
    public function testGetURL()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    /**
     * @covers GFUser::addServiceSubscription
     * @todo   Implement testAddServiceSubscription().
     */
    public function testAddServiceSubscription()
    {
        $this->markTestSkipped('Deprecated');
    }

    /**
     * @covers GFUser::removeServiceSubscription
     * @todo   Implement testRemoveServiceSubscription().
     */
    public function testRemoveServiceSubscription()
    {
        $this->markTestSkipped('Deprecated');
    }

    public function testGetRunningModels()
    {
        $user=new GFUser(105);

        is::arr($user->getRunningModels());
    }

    public function testCheckFinishedModels()
    {
        $this->markTestSkipped('unknown error when dealing with Amazon code locally');
    }

    public function testSendText()
    {
        $this->markTestSkipped('no mail server set up locally');
    }
}
