<?php
require_once 'db_inc.inc';

use Assert as is;

class CEMTagTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var CEMTag
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new CEMTag;
        db_begin();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        db_rollback();
    }

    /**
     * @covers CEMTag::getID
     * @todo   Implement testGetID().
     */
    public function testGetID()
    {
        $Tag=new CEMTag(12);

        is::int($Tag->getID());
    }

    /**
     * @covers CEMTag::getName
     * @todo   Implement testGetName().
     */
    public function testGetName()
    {
        $Tag=new CEMTag(12);

        is::string($Tag->getName());
    }

    /**
     * @covers CEMTag::create
     * @todo   Implement testCreate().
     */
    public function testCreate()
    {
        //Create new tag
        is::int(CEMTag::create('tag_'.rand(1,100)));

        //Get old tag
        is::int(CEMTag::create('test'));
    }

    /**
     * @covers CEMTag::search
     * @todo   Implement testSearch().
     */
    public function testSearch()
    {
        is::arr(CEMTag::search('tes'));
    }
}
