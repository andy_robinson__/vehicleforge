<?php
require_once 'db_inc.inc';

use Assert as is;

class CEMRunningTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var CEMRunning
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new CEMRunning;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers CEMRunning::getID
     * @todo   Implement testGetID().
     */
    public function testGetID()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);

        $ID=CEMRunning::create($DIID,105,"");

        $Run=new CEMRunning($ID);

        is::int($Run->getID());

        db_rollback();
    }

    /**
     * @covers CEMRunning::getQueueURL
     * @todo   Implement testGetQueueURL().
     */
    public function testGetQueueURL()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        $ID=CEMRunning::create($DIID,105,"QUEUE URL");

        $Run=new CEMRunning($ID);

        is::string($Run->getQueueURL());

        db_rollback();
    }

    /**
     * @covers CEMRunning::getOwnerID
     * @todo   Implement testGetOwnerID().
     */
    public function testGetOwnerID()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        $ID=CEMRunning::create($DIID,105,"");

        $Run=new CEMRunning($ID);

        is::int($Run->getOwnerID());

        db_rollback();
    }

    /**
     * @covers CEMRunning::getRunnableID
     * @todo   Implement testGetRunnableID().
     */
    public function testGetRunnableID()
    {
        // Remove the following lines when you implement this test.
        $this->markTestSkipped('deprecated');
    }

    /**
     * @covers CEMRunning::getInterfaceID
     * @todo   Implement testGetInterfaceID().
     */
    public function testGetInterfaceID()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        $ID=CEMRunning::create($DIID,105,"");

        $Run=new CEMRunning($ID);

        is::int($Run->getInterfaceID());

        db_rollback();
    }

    /**
     * @covers CEMRunning::getStatus
     * @todo   Implement testGetStatus().
     */
    public function testGetStatus()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        $ID=CEMRunning::create($DIID,105,"");

        $Run=new CEMRunning($ID);

        is::false($Run->getStatus());

        db_rollback();
    }

    /**
     * @covers CEMRunning::setStatus
     * @todo   Implement testSetStatus().
     */
    public function testSetStatus()
    {
        db_begin();
        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        $ID=CEMRunning::create($DIID,105,"");

        $Run=new CEMRunning($ID);

        is::true($Run->setStatus(true));
        is::true($Run->setStatus(false));

        db_rollback();
    }

    /**
     * @covers CEMRunning::create
     * @todo   Implement testCreate().
     */
    public function testCreate()
    {
        db_begin();

        $Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);
        $CEMID=CEM::create(28,"test");
        $DIID=DOMEInterface::create($CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $Server);


        //Positive
        is::int(CEMRunning::create($DIID,105,""));

        //Negative
        //Wrong argument types
        $this->setExpectedException('InvalidArgumentException');
        CEMRunning::create('','',234);

        //Invalid interface ID
        $this->setExpectedException('InvalidArgumentException');
        CEMRunning::create(1,105,'');

        db_rollback();
    }

    /**
     * @covers CEMRunning::delete
     * @todo   Implement testDelete().
     */
    public function testDelete()
    {
        $this->markTestSkipped('should not be used');
    }
}
