<?php
require_once 'db_inc.inc';
require_once APP_PATH.'common/include/GFUser.class.php';

use Assert as is;

class GitoliteTest extends PHPUnit_Framework_TestCase{
    protected $object, $iobject;

    protected function setUp(){
        db_begin();
        $this->object=new Gitolite;
    }

    protected function tearDown(){
        db_rollback();
    }

    public function testSetPermissions(){
        is::false($this->object->setPermissions('1',1,'RW'));
        is::false($this->object->setPermissions(1,'2','RW'));
        is::false($this->object->setPermissions(1,1,1));

        is::true($this->object->setPermissions(105, 2237, 'R'));
        is::true($this->object->setPermissions(105, 2237, 'R'));
        is::true($this->object->setPermissions(105, 2237, 'RW'));
    }

    public function testRemoveAllPermissions(){
        is::true($this->object->removeAllPermissions(105, 2237));
    }

    public function testGetUserPermissions(){
        $this->object->setPermissions(108, 2237, 'RW');

        $x=Gitolite::getUserPermissions(108, 2237);

        is::arr($x);
        is::true($x['read']);
        is::true($x['write']);
        is::false($x['rewind']);
    }

    public function testExportFile(){
        $this->markTestSkipped();
    }
}
?>