<?php
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon . 'include/Group.class.php';

$GroupID=getIntFromRequest('group_id');
$Group=new Group($GroupID);

$Name=$Group->getPublicName();
if (strlen($Name)>15){
	$Name=substr($Name,0,12).'...';
}

$JS='
var json={
	id:"p'.$Group->getID().'",
	name:\''.$Group->getImageHTML(60,60).'<br />'.$Name.'\',
	children:[]
};

function init(){
	var st = new $jit.ST({
	injectInto: "infovis",
	constrained:false,
	duration: 300,
	transition: $jit.Trans.Quart.easeInOut,
	levelDistance: 25,
	levelsToShow:2,
	Events:{
	    enable:true,
		onClick:function(node,ev,e){
			if(node){
				$("#component_link").attr("href","/components/?group_id='.$GroupID.'&cid="+node.id.substr(1));
				$("#component_name").text(node.name);
			}
		}
	},
	Tree:{
		orientation:"top"
	},
	request: function(nodeId, level, onComplete)    {
		var type;
		if (nodeId.substr(0,1)=="p"){
			type="project";
		}else{
			type="component";
		}
		var id=nodeId.substr(1),
				j={id:nodeId},
				children=[],
				name;
		$.getJSON("ajax/get_component_graph.php",{id:id,type:type},function(msg){
			for(var i=0;i<msg.length;i++){
				name=msg[i].name;
				/*if (name.length>15){
					name=name.substr(0,12)+"...";
				}*/
				children.push({
					id:"c"+msg[i].cid,
					name:name,
				});
			}

			j.children=children;

			onComplete.onComplete(nodeId,j);
		});
	},
	Navigation: {
		enable:true,
		panning:true,
		zooming: 100
	},
	Node: {
		autoHeight:true,
		autoWidth:true,
		type: "rectangle",
		color: "#CCC",
		overridable:true
	},
	Edge: {
		type: "bezier",
		overridable: true
	},
	onCreateLabel: function(label, node){
		label.id = node.id;
		label.innerHTML = node.name;
		label.onclick = function(){
			st.onClick(node.id,{
				Move:{
					enable:false
				}
			});
		};
		var style = label.style;
		style.cursor = "pointer";
		style.color = "#000";
		style.fontSize = "0.8em";
		style.textAlign= "center";
		style.padding = "3px";
	},
	onBeforePlotNode: function(node){
	  if (node.selected) {
	      node.data.$color = "#8ACFF2";
	  }else {
	      delete node.data.$color;
	      /*if(!node.anySubnode("exist")) {
	          var count = 0;
	          node.eachSubnode(function(n) { count++; });
	          node.data.$color = ["#aaa", "#baa", "#caa", "#daa", "#eaa", "#faa"][count];
	      }*/
	  }
	},
	onBeforePlotLine: function(adj){
		if (adj.nodeFrom.selected && adj.nodeTo.selected) {
		  adj.data.$color = "#000";
		  adj.data.$lineWidth = 3;
		}else {
		  delete adj.data.$color;
		  delete adj.data.$lineWidth;
		}
	}
	});
	st.loadJSON(json);
	st.compute();
	st.geom.translate(new $jit.Complex(-200, 0), "current");
	st.onClick(st.root);
}

$(function(){
	init();

	$("#infovis-canvas").mousewheel(function(ev, delta){
		var size=parseInt($("#infovis").css("font-size"));
		if (delta>0){
			$("#infovis").css("font-size",size+2);
		}else{
			$("#infovis").css("font-size",size-2);
		}
	});
});';
add_js($JS);
use_javascript('jit.min.js');
use_javascript('jquery.mousewheel.min.js');

site_project_header(array('group'=>$GroupID,'title'=>'Component Graph'));
$Layout->col(8,true);
?>
<style>#infovis{font-size:12px}</style>
<div id="infovis" style="height:400px;border:2px solid #000;overflow:hidden;background:#FFF"></div>
<?php
$Layout->endcol()->col(4);
?>
<strong id="component_name"></strong><br />
<a href="javascript:void(0)" id="component_link">Go to component page</a>
<?php
$Layout->endcol();
site_project_footer();
?>