<?php
/**
 * FusionForge : Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfwww.'include/note.php';
require_once $gfcommon.'reporting/report_utils.php';
require_once $gfcommon.'reporting/Report.class.php';

use_javascript('jquery-ui-1.8.16.custom.min.js');
use_javascript('jquery-timepicker.js');
use_stylesheet('/themes/gforge/css/jquery-ui-1.8.16.custom.css');
use_stylesheet('/themes/css/timepicker.css');

date_default_timezone_set('America/New_York');

$JS='
Date.prototype.getWeek = function() {
    var determinedate = new Date();
    determinedate.setFullYear(this.getFullYear(), this.getMonth(), this.getDate());
    var D = determinedate.getDay();
    if(D == 0) D = 7;
    determinedate.setDate(determinedate.getDate() + (4 - D));
    var YN = determinedate.getFullYear();
    var ZBDoCY = Math.floor((determinedate.getTime() - new Date(YN, 0, 1, -6)) / 86400000);
    var WN = 1 + Math.floor(ZBDoCY / 7);
    return WN;
}

$(function(){
	$("#timetracker").datepicker({
		showWeek: true,
		firstDay: 1,
		onSelect: function(date,instance){
			var date=new Date(date);
			$("#day").val(date.getTime()/1000);

			/*var year=Date.parse("Jan 1, "+date.getFullYear())/1000;

			var week=date.getWeek()*60*60*24*6;
			console.log(date.getWeek(),week);
			console.log(date.getFullYear(),year);
			console.log(Date.parse("Nov , 2011")/1000-18000,week+year);
			$("#week").val(week);*/
		}
	});

	$("#start_date").datepicker({
		showWeek: true,
		firstDay: 1,
		altField: "#actual_start_date",
		altFormat: "@",
		showButtonPanel: true
	});

	$("#end_date").datepicker({
		showWeek: true,
		firstDay: 1,
		altField: "#actual_end_date",
		altFormat: "@",
		showButtonPanel: true
	});

    $("#start_time, #end_time").timepicker({
        showNowButton: true
    });
});';

add_js($JS);

pm_header(array('title'=>_('Modify Task'),'pagename'=>'pm_modtask','group_project_id'=>$group_project_id));
$Layout->col(12,true);
echo $HTML->tertiary_menu();

$Categories=$pg->getCategories();
$CategoryOptions=array_combine(util_result_column_to_array($Categories,0),util_result_column_to_array($Categories,1));

$SubProjects=$pg->getSubProjects();
$SubProjectOptions=array_combine(util_result_column_to_array($SubProjects,0),util_result_column_to_array($SubProjects,1));

$PercentOptions=array(0=>'Not Started');
for($i=5;$i<=100;$i+=5)
    $PercentOptions[$i]=$i.'%';

$PriorityOptions=array();
for($i=1;$i<=5;$i++){
    if ($i==1)
        $PriorityOptions[$i]='1 - Lowest';
    elseif ($i==5)
        $PriorityOptions[$i]='5 - Highest';
    else
        $PriorityOptions[$i]=$i;
}

$PossibleAssignedTo=$pt->getPossibleAssignedTo();
$AssignedToOptions=array(100=>'Nobody');
foreach($PossibleAssignedTo as $i)
    $AssignedToOptions[$i->getID()]=$i->getRealName();

$PossibleDependsOn=$pt->getOtherTasks();
$DependsOnOptions=array(100=>'Nothing')+array_combine(util_result_column_to_array($PossibleDependsOn,0),util_result_column_to_array($PossibleDependsOn,1));

$Statuses=$pg->getStatuses();
$StatusOptions=array_combine(util_result_column_to_array($Statuses,0),util_result_column_to_array($Statuses,1));

$Form=new BsForm;
echo $Form->init(getStringFromServer('PHP_SELF')."?group_id=$group_id&amp;group_project_id=$group_project_id",'post',array('class'=>'form-horizontal'))
    ->head('Task Details')
    ->group('Submitted By',
        new Hidden(array('name'=>'func','value'=>'postmodtask')),
        new Hidden(array('name'=>'project_task_id','value'=>$project_task_id)),
        new Custom($pt->getSubmittedRealName())
    )
    ->group('Category',
        new Dropdown($CategoryOptions,$pt->getCategoryID(),array(
            'name'=>'category_id',
            'autocomplete'=>false
        ))
    )
    ->group('Subproject',
        new Dropdown($SubProjectOptions,$group_project_id,array(
            'name'=>'new_group_project_id',
            'autocomplete'=>false
        ))
    )
    ->group('Priority',
        new Dropdown($PriorityOptions,$pt->getPriority(),array(
            'name'=>'priority',
            'autocomplete'=>false
        ))
    )
    ->group('Percent Complete',
        new Dropdown($PercentOptions,$pt->getPercentComplete(),array(
            'name'=>'percent_complete',
            'autocomplete'=>false
        ))
    )
    ->group('Summary',
        new Text(array(
            'value'=>$pt->getSummary(),
            'name'=>'summary'
        ))
    )
    ->group('Add Comment',
        new Textarea('',array(
            'name'=>'details'
        ))
    )
    ->group('Start Time',
        new Text(array(
            'id'=>'start_date',
            'class'=>'span2',
            'autocomplete'=>false,
            'value'=>date('m/d/Y', $pt->getStartDate())
        )),
        new Text(array(
            'id'=>'start_time',
            'name'=>'start_time',
            'class'=>'span1',
            'autocomplete'=>false,
            'value'=>date('G:i', $pt->getStartDate())
        )),
        new Hidden(array(
            'id'=>'actual_start_date',
            'name'=>'actual_start_date',
            'autocomplete'=>false,
            'value'=>mktime(0,0,0,date('n',$pt->getStartDate()),date('j',$pt->getStartDate()),date('Y',$pt->getStartDate()))
        )),
        new Custom(date('m/d/Y G:i',$pt->getStartDate()).'='.$pt->getStartDate())
    )
    ->group('End Time',
        new Text(array(
            'id'=>'end_date',
            'class'=>'span2',
            'autocomplete'=>false,
            'value'=>date('m/d/Y', $pt->getEndDate())
        )),
        new Text(array(
            'id'=>'end_time',
            'name'=>'end_time',
            'class'=>'span1',
            'autocomplete'=>false,
            'value'=>date('G:i', $pt->getEndDate())
        )),
        new Hidden(array(
            'id'=>'actual_end_date',
            'name'=>'actual_end_date',
            'autocomplete'=>false,
            'value'=>$pt->getEndDate()
        ))
    )
    ->group('Assigned To',
        new Dropdown($AssignedToOptions,array_values($pt->getAssignedTo()),array(
            'multiple'=>true,
            'name'=>'assigned_to[]'
        ))
    )
    ->group('Depends on',
        new Dropdown($DependsOnOptions,array_keys($pt->getDependentOn()),array(
            'multiple'=>true,
            'name'=>'dependent_on[]',
            'class'=>'span4'
        ))
    )
    ->group('Estimated Hours',
        new Text(array(
            'value'=>$pt->getHours(),
            'name'=>'hours',
            'class'=>'span1'
        ))
    )
    ->group('Status',
        new Dropdown($StatusOptions, $pt->getStatusID(),array(
            'name'=>'status_id',
            'autocomplete'=>false
        )),
        new Hidden(array(
            'name'=>'parent_id',
            'value'=>0
        )),
        new Hidden(array(
            'name'=>'duration',
            'value'=>0
        ))
    )
    ->actions(
        new Submit('Submit',array('name'=>'submit','class'=>'btn btn-primary')),
        new Reset('Reset',array('class'=>'btn'))
    )
    ->render();
?>

<h3><?php echo _('Time tracking'); ?></h3>

<?php
$title_arr = array();
$title_arr[]=_('Date');
$title_arr[]=_('Estimated Hours');
$title_arr[]=_('Category');
$title_arr[]=_('User');
$title_arr[]=' ';

$xi = 0;

$report=new Report();
if ($report->isError()) {
	exit_error($report->getErrorMessage(),'pm');
}

echo '<form action="/reporting/timeadd.php" method="post">
	<input type="hidden" name="project_task_id" value="'.$project_task_id.'" />
	<input type="hidden" name="submit" value="1" />';
echo $HTML->listTableTop ($title_arr);
echo '<tr '.$HTML->boxGetAltRowStyle($xi++).'>
		<!--<td style="text-align:center">'. report_weeks_box($report, 'week') .'</td>
		<td style="text-align:center">'. report_day_adjust_box($report, 'days_adjust') .'</td>-->
		<td><input type="text" id="timetracker" autocomplete="off" /><input type="hidden" name="week" id="week" value="1" /><input type="hidden" name="days_adjust" id="day" /></td>
		<td style="text-align:center"><input type="text" autocomplete="off" name="hours" value="" size="3" maxlength="3" /></td>
		<td style="text-align:center">'.report_time_category_box('time_code',false).'</td>
		<td>&nbsp;</td>
		<td style="text-align:center"><input type="submit" name="add" value="'._('Add').'" /><input type="submit" name="cancel" value="'._('Cancel').'" /></td>
	</tr>';
	
//setenv("TZ=" . $user_timezone); //restore the user's timezone
	
//
//	Display Time Recorded for this task
//


$res=db_query_params ('SELECT users.realname, rep_time_tracking.report_date, rep_time_tracking.hours, rep_time_category.category_name
	FROM users,rep_time_tracking,rep_time_category
	WHERE 
	users.user_id=rep_time_tracking.user_id
	AND rep_time_tracking.time_code=rep_time_category.time_code
	AND rep_time_tracking.project_task_id=$1',
			array($project_task_id));
$total_hours =0;
for ($i=0; $i<db_numrows($res); $i++) {

	echo '
	<tr '.$HTML->boxGetAltRowStyle($xi++).'>
	<td>'.date(_('Y-m-d H:i'),db_result($res,$i,'report_date')).'</td>
	<td>'.db_result($res,$i,'hours').'</td>
	<td>'.db_result($res,$i,'category_name').'</td>
	<td>'.db_result($res,$i,'realname').'</td>
	<td>&nbsp;</td></tr>';
	$total_hours += db_result($res,$i,'hours');
	
}

echo '
<tr '.$HTML->boxGetAltRowStyle($xi++).'>
<td><strong>'._('Total').':</strong></td>
<td>&nbsp;</td>
<td>'.$total_hours.'</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>';

echo $HTML->listTableBottom();
echo "</form>\n";

$Layout->endcol();
pm_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
