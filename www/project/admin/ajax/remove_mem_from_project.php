<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:19 PM 10/10/11
 */

require_once('../../../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$group_id = getIntFromRequest('group_id');
$group = group_get_object($group_id);

$user_id = getIntFromRequest('user_id');
$role_id = getIntFromRequest('role_id');
$role = RBACEngine::getInstance()->getRoleById($role_id) ;

$group->requirePerm('project_admin');

//UserRole::delete($user_id, $group_id, $role_id);



if (!$role->removeUser (user_get_object ($user_id))) {
	$error_msg = $role->getErrorMessage() ;
} else {
	$feedback = _("Member Removed Successfully");
}

echo true;
?>