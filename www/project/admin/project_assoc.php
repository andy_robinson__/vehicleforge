<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:57 AM 12/19/11
 */

require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'project/admin/project_admin_utils.php';
require_once $gfwww.'include/TableGen.php';
$TableGen=new TableGen;

$group_id = getIntFromRequest('group_id');
session_require_perm ('project_admin', $group_id) ;

$group = group_get_object($group_id);
if (!$group || !is_object($group)) {
    exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'admin');
}

$JS='$(function(){
	$(".assoc").click(function(){
		$.post("/ajax/project_assoc.php",{"pid":'.$group_id.',"aid":$(this).data("id")},function(){

		});
	});
});';
add_js($JS);

project_admin_header(array('title'=>_('Project Associations'), 'group'=>$group_id));
$Layout->col(16,true);
$HTML->tertiary_menu();

$HTML->heading(_('Projects'));

$TableGen->top(array(array(_('Project Name'),50),array(_('Associated?'),50)),'%');
$Result=db_query_params("SELECT * FROM groups WHERE status=$1 AND is_public=$2 AND group_id!=$3 AND group_type=$4 ORDER by group_name ASC",array('A',true,$group_id,1));
while($GroupRow=db_fetch_array($Result,null,PGSQL_ASSOC)){
	$TableGen->col('<a href="/projects/'.$GroupRow['unix_group_name'].'">'.$GroupRow['group_name'].'</a>')
			->col('<input type="checkbox" autocomplete="off" class="assoc" data-id="'.$GroupRow['group_id'].'" '.($group->getAssociation($GroupRow['group_id'])?'checked="checked"':'').' />');
}
$TableGen->bottom();


$HTML->heading(_('CEM Objects'));

$TableGen->top(array(array(_('CEM Object Name'),50),array(_('Associated?'),50)),'%');
$Result=db_query_params("SELECT * FROM groups WHERE status=$1 AND is_public=$2 AND group_id!=$3 AND group_type=$4 ORDER by group_name ASC",array('A',true,$group_id,2));
while($GroupRow=db_fetch_array($Result,null,PGSQL_ASSOC)){
	$TableGen->col('<a href="/projects/'.$GroupRow['unix_group_name'].'">'.$GroupRow['group_name'].'</a>')
			->col('<input type="checkbox" autocomplete="off" class="assoc" data-id="'.$GroupRow['group_id'].'" '.($group->getAssociation($GroupRow['group_id'])?'checked="checked"':'').' />');
}
$TableGen->bottom();
$Layout->endcol();
project_admin_footer();
?>