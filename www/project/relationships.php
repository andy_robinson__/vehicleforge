<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:28 AM 12/29/11
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/Group.class.php';
require_once $gfwww.'include/LayoutGen.php';

$LayoutGen=new LayoutGen;

$GroupID=getIntFromRequest('group_id');
$Group=group_get_object($GroupID);

session_require_perm ('project_read', $GroupID);

use_javascript('jit.js');

$Name=$Group->getPublicName();
if (strlen($Name)>15){
	$Name=substr($Group->getPublicName(),0,12).'...';
}

$JS='var json={
	id:"p'.$Group->getID().'",
	name:\''.$Group->getImageHTML().'<br />'.$Name.'\',
	data:{
		name:"'.$Group->getPublicName().'",
		uname:"'.$Group->getUnixName().'"
	},
	children:[]
};

function init(){
	var st = new $jit.ST({
	injectInto: "infovis",
	constrained:false,
	duration: 300,
	transition: $jit.Trans.Quart.easeInOut,
	levelDistance: 25,
	levelsToShow:1,
	Events:{
	    enable:true,
		onClick:function(node,ev,e){
			if(node){
				$("#cur_selected_id").attr("href","/projects/"+node.data.uname);
				$("#cur_selected_name").text(node.data.name);
			}
		}
	},
	Tree:{
		orientation:"top"
	},
	request: function(nodeId, level, onComplete)    {
		var pid=nodeId.substr(1),
				j={id:nodeId},
				children=[],
				name;
		$.getJSON("/ajax/get_project_association.php",{pid:pid},function(msg){
			for(var i=0;i<msg.length;i++){
				name=msg[i].name;
				if (name.length>15){
					name=name.substr(0,12)+"...";
				}
				children.push({
					id:"p"+msg[i].group_id,
					name:msg[i].img+"<br />"+name,
					data:{
						uname:msg[i].uname,
						name:msg[i].name
					}});
			}

			j.children=children;

			onComplete.onComplete(nodeId,j);
		});
	},
	Navigation: {
		enable:true,
		panning:true,
		zooming: true
	},
	Node: {
		autoHeight:true,
		autoWidth:true,
		type: "rectangle",
		color: "#CCC",
		overridable:true
	},
	NodeStyles:{

	},
	Edge: {
		type: "bezier",
		overridable: true
	},
	onCreateLabel: function(label, node){
		label.id = node.id;
		label.innerHTML = node.name;
		label.onclick = function(){
			st.onClick(node.id,{
				Move:{
					enable:false
				}
			});
		};
		var style = label.style;
		style.cursor = "pointer";
		style.color = "#000";
		style.fontSize = "0.8em";
		style.textAlign= "center";
		style.padding = "3px";
	},
	onBeforePlotNode: function(node){
	  if (node.selected) {
	      node.data.$color = "#8ACFF2";
	  }else {
	      delete node.data.$color;
	      /*if(!node.anySubnode("exist")) {
	          var count = 0;
	          node.eachSubnode(function(n) { count++; });
	          node.data.$color = ["#aaa", "#baa", "#caa", "#daa", "#eaa", "#faa"][count];
	      }*/
	  }
	},
	onBeforePlotLine: function(adj){
		if (adj.nodeFrom.selected && adj.nodeTo.selected) {
		  adj.data.$color = "#000";
		  adj.data.$lineWidth = 3;
		}else {
		  delete adj.data.$color;
		  delete adj.data.$lineWidth;
		}
	}
	});
	st.loadJSON(json);
	st.compute();
	st.geom.translate(new $jit.Complex(-200, 0), "current");
	st.onClick(st.root);
}

init();';
add_js($JS);

site_project_header(array('group'=>$GroupID));

$LayoutGen->col(9);
$HTML->boxTop()->heading("Graph");
?>
<div id="infovis" style="width:80%;height:400px;border:2px solid #000;overflow:hidden;float:left"></div>
<?php
$HTML->boxBottom();
$LayoutGen->endcol()-col(3);
$HTML->boxTop()->heading("Project Properties");
?>
Project: <strong id="cur_selected_name"></strong><br /><br />
<a href="javascript:void(0)" target="_none" id="cur_selected_id">Go to Project Page</a>
<?php
$HTML->boxBottom();
$LayoutGen->endcol();

site_project_footer();
?>