<?php
require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$group_id=getIntFromRequest('group_id');
$group = group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

echo $group_id;

if (!$group || !is_object($group)) {
    exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'admin');
}

site_project_header(array('group'=>$group_id));

site_project_footer();
?>
