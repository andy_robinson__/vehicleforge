<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once 'people_utils.php';
require_once 'skills_utils.php';

use_javascript('jquery.textext.js');
$JS='$(function(){
    $("#keywords").textext({
        plugins: "autocomplete tags ajax",
        ajax:{
            url:"ajax/search_skill_keywords.php",
            dataType:"json",
            cacheResults:false
        }
    });

    var $skill_add_name         = $("#name"),
        $skill_add_keywords     = $("#keywords").siblings("input[type=hidden]"),
        $skill_add_keyword_tags = $("#keywords").siblings(".text-tags"),
        $skill_add_exp_level    = $("#exp_level");

    $("#button_update_skill").click(function(){
        $.ajax({
            url:"ajax/update_skill.php",
            dataType:"json",
            data:{
                name        : $skill_add_name.val(),
                keywords    : $skill_add_keywords.val(),
                exp_level   : $skill_add_exp_level.val(),
                skill_id    : $("#skill_id").val()
            },
            success: function(msg){
                if (!msg.error){

                }
            }
        });
        return false;
    });
});';

add_js($JS);

$Skill=new UserSkill(getIntFromRequest('s'));

people_header(array('title'=>'Edit a Skill: '.$Skill->getSkillName()));
$Layout->col(12,true);

skills_show_skills_form('Edit Skill: '.$Skill->getSkillName(), $Skill->getID());

$Layout->endcol();
people_footer();
?>