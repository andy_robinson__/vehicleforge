<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Name=getStringFromRequest('name');
$Keywords=json_decode(getStringFromRequest('keywords'));
$ExpLevel=getIntFromRequest('exp_level');

$Return=array('error'=>false);

if (!session_get_user()->addSkill($Name, $Keywords, $ExpLevel)){
    $Return['error']=true;
}

echo json_encode($Return);
?>