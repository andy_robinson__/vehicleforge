<script src="/js/helptip.js"></script>

<script>				
	$(window).ready( function() {		
		// Append help button and overlay
		$('body').append('<div id="help-overlay" class="hidden"></div>');
		$('body').append('<div id="help-toggle">help</div>');
		
		// Settings
		$('[helptip-text]').helptip( {
			trigger: 'manual'
		});
		
		$('#help-toggle').click( function(event) {
			var overlay = $('#help-overlay');
			overlay.toggleClass('hidden');
			if (overlay.hasClass('hidden')) {
				$('[helptip-text]').helptip('hide');
				$(this).html('help').css('background-color', '#000');
			} else {
				$('[helptip-text]').helptip('show');
				$(this).html('hide').css('background-color', '#05C');
			}
		});
		
		$('[helptip-text]').hover(
			function(event) { // hover in
				toggleHover(this, function(self) { $(self).data('helptip').isShown() ? '' : $(self).helptip('show'); }, 700);
			},
			function(event) { // hover out
				toggleHover(this, function(self) { $(self).helptip('hide'); }, 500);
			}
		);
	});	
	
	function toggleHover(self, func, delay) {
		if ($('#help-overlay').hasClass('hidden')) {
			clearTimeout( $(self).data('_counter') );
			$(self).data('_counter', setTimeout(function() {func(self)}, delay) );
		}
	}
</script>

<style>
	#help-overlay {
		position: fixed;
		z-index: 1000;
		left: 0;
		width: 100%;
		top: 0;
		height:100%;							
		background-color: #000;
		opacity: 0.3;	
		filter:alpha(opacity=0.3);
		pointer-events: none;
	}
	
	#help-toggle {
		cursor: pointer;
		position: fixed;
		left: -30px;
		width: 100px;
		top: 30%;
		height: 30px;
		z-index: 1010;
		
		padding-top: 10px;				
		text-align: center;
		font-size: 14px;
		font-weight: bold;
		color: #FFF;
		background-color: #000;
		
		border-radius: 5px 5px 0 0;
		-webkit-transform: rotate(90deg);
		-moz-transform: rotate(90deg);
		transform: rotate(90deg);		
	}
	
	.helptip{
		position:absolute;
		z-index:1020;
		display:block;
		padding:5px;
		font-size:13px;
		opacity:0;
		filter:alpha(opacity=0);
		visibility:visible;
	}
	
	.helptip.in{
		opacity:0.95;
		filter:alpha(opacity=95);
	}
	
	.helptip.top{margin-top:-2px;}
	.helptip.right{margin-left:2px;}
	.helptip.bottom{margin-top:2px;}
	.helptip.left{margin-left:-2px;}
	
	.helptip.top .helptip-arrow{bottom:0;left:50%;margin-left:-5px;border-top:5px solid #05C;border-right:5px solid transparent;border-left:5px solid transparent;}
	.helptip.left .helptip-arrow{top:50%;right:0;margin-top:-5px;border-top:5px solid transparent;border-bottom:5px solid transparent;border-left:5px solid #05C;}
	.helptip.bottom .helptip-arrow{top:0;left:50%;margin-left:-5px;border-right:5px solid transparent;border-bottom:5px solid #05C;border-left:5px solid transparent;}
	.helptip.right .helptip-arrow{top:50%;left:0;margin-top:-5px;border-top:5px solid transparent;border-right:5px solid #05C;border-bottom:5px solid transparent;}
	
	.helptip-inner{
		max-width:200px;
		padding:5px 10px;
		color:#ffffff;
		text-align:center;
		text-decoration:none;
		background-color:#05C;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
		border-radius:4px;
	}
	
	.helptip-arrow{
		position:absolute;
		width:0;
		height:0;
	}
</style>