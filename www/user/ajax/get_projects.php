<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$User=user_get_object(getIntFromRequest('uid'));

if ($User){
    $Projects=$User->getOwnedProjects();

    $Return=array();

    foreach($Projects as $i){
        $Return[]=array(
            'group_id'=>$i->getID(),
            'public_name'=>$i->getPublicName()
        );
    }

    echo json_encode($Return);
}
?>