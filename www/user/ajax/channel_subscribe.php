<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:48 PM 11/30/11
 */

//This script toggles the subscription status of a user to an object

require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';

$User=user_get_object(user_getid());
$TypeID=getIntFromRequest('tid');
$RefID=getIntFromRequest('rid');

$CurStatus=$User->getSubscriptionStatus($RefID,$TypeID);
$User->setSubscriptionStatus($RefID,$TypeID,!$CurStatus);

echo json_encode(array('error'=>false,'status'=>!$CurStatus));
?>