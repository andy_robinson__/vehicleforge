<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

//print_r($_SERVER);

session_start();

if (isset($_SERVER['Shib-Application-ID']) && isset($_SERVER['Shib-Session-ID']) && isset($_SERVER['Shib-Identity-Provider'])){
    session_set_new($_SERVER['USER_ID']);

    //echo 'Hi '.$_SERVER['REALNAME'].'! Continue back to VF and you will be logged in!';
    if (isset($_COOKIE['last_page']) && $_COOKIE['last_page']!="/404.php"){
        header("Location: ".$_COOKIE['last_page']);
    }else{
        header("Location: /index2.php");
    }

    //print_r($_COOKIE);
}

?>