<?php
define('MEMBER_PAGE_COUNT', 16);

function validWikiID($id){
	$Result = db_query_params("SELECT * FROM wiki_pages WHERE page_id=$1",array($id));
	$numRows = db_numrows($Result);
	return ($numRows > 0) ? TRUE : FALSE ;
}

function formatMember($UserID, $HideInitially=false){
    $User=user_get_object($UserID);

    $userTile = '<li class="col4'.($HideInitially==true?' hidden':'').'">';
    $userTile .=    '<a href="#" class="thumbnail user_thumb" data-uid="'.$User->getID().'">';
    
    // Add user profile picture
    $userTile .=    $User->getImageHTML(100,100,array('class'=>'user_image'));
    
    // Add user name
    $userTile .=    '<h4>'.$User->getRealName().'</h4>';
    
    // Add user online status 
    $userTile .=    '<p><span class="label ';
    if($User->getOnlineStatus()){
        $userTile .=  'label-success">Online';   
    } else{
        $userTile .=  '">Offline';
    }
    
    $userTile .=    '</span></p></a></li>';

    return $userTile;
}

function getMemberCount(){
    return sizeof(getActiveMembers());
}

function getActiveMembers($Offset=null, $Limit=null){
    $SQL="SELECT user_id FROM users WHERE user_id>102 AND status='A' ORDER BY realname ASC";

    if ($Offset)
        $SQL.=' OFFSET '.$Offset;

    if ($Limit)
        $SQL.=' LIMIT '.$Limit;

    $Result=db_query_params($SQL);
    return util_result_columns_to_array($Result, PGSQL_ASSOC);
}

function getOnlineMemberCount(){
    return sizeof(getOnlineMembers());
}

function getOnlineMembers($Offset=null, $Limit=null){
    $SQL="SELECT user_id FROM users WHERE user_id>102 AND status='A' AND last_activity>".(time()-5*60)." ORDER BY realname ASC";

    if ($Offset)
        $SQL.=' OFFSET '.$Offset;

    if ($Limit)
        $SQL.=' LIMIT '.$Limit;

    $Result=db_query_params($SQL);
    return util_result_columns_to_array($Result, PGSQL_ASSOC);
}

function getPopularDiscussions($Limit=10){
    $Return=array();

    $Result=db_query_params("SELECT forum_messages.topic_id FROM forum_messages,forum_topics,forums WHERE forums.group_id=1 AND forum_topics.forum_id=forums.forum_id AND forum_messages.topic_id=forum_topics.topic_id  GROUP BY forum_messages.topic_id ORDER BY count(forum_messages.message_id) DESC LIMIT $1",array($Limit));
    while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
        $Return[]=new ForumTopic($Row['topic_id']);

    return $Return;
}

function getLatestDiscussions($Limit=10){
    $Return=array();

    $Result=db_query_params("SELECT * FROM forum_topics, forums WHERE forums.group_id=1 AND forum_topics.forum_id=forums.forum_id ORDER BY time_posted DESC LIMIT $1",array($Limit));
    while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
        $Return[]=new ForumTopic($Row['topic_id'], $Row);

    return $Return;
}

function getLatestMembers($Limit=5){
    $Return=array();
    if ($Result=db_query_params("SELECT user_id FROM users ORDER BY add_date DESC LIMIT $1",array($Limit))){
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=user_get_object($Row['user_id']);

        return $Return;
    }

    return false;
}

function getLatestProjects($Limit=5){
    $Return=array();
    if ($Result=db_query_params("SELECT * FROM groups ORDER BY register_time DESC LIMIT $1",array($Limit))){
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=group_get_object($Row['group_id']);

        return $Return;
    }

    return false;
}

function getPopularProjects($Limit=10){
    $Return=array();
    if ($Result=db_query_params("SELECT count(comment_id) \"count\", ref_id group_id FROM home_comments WHERE type_id=2 GROUP BY ref_id ORDER BY \"count\" DESC LIMIT $1",array($Limit))){
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=group_get_object($Row['group_id']);

        return $Return;
    }

    return false;
}

    //wiki_pages [page_id, group_id , user_id , name, contents, creation_time, modified_time, modified_user_id]
    //wiki_categories [cat_id, name]
    //wiki_pages_wiki_categories_join [cat_id, page_id]

function getResourceCategories(){
    $Result=db_query_params("SELECT * FROM wiki_categories");
    return util_result_columns_to_array($Result);
}

function getResources($catID=-1){
        $Return = array();
        
        if($catID > 0){
            $Result=db_query_params("SELECT page_id FROM wiki_pages_wiki_categories_join WHERE cat_id=$1", array($catID));
        } else{
            $Result=db_query_params("SELECT page_id FROM wiki_pages ORDER BY name DESC");
        }
        
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=new WikiPage($Row['page_id']);

        return $Return;
}

function searchResources($query){
        $Return = array();
        $dbQuery = "SELECT page_id FROM wiki_pages WHERE lower(name) LIKE lower('%".$query."%')";
        if(!empty($query)){
            $Result=db_query_params($dbQuery);
        }
        
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=new WikiPage($Row['page_id']);

        return $Return;
}

function getLatestResources($Limit=5){
    $Return=array();
    if ($Result=db_query_params("SELECT page_id FROM wiki_pages ORDER BY modified_time DESC LIMIT $1",array($Limit))){
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=new WikiPage($Row['page_id']);

        return $Return;
    }

    return false;
}

function getPopularResources($Limit=5){
    //TODO

    return false;
}


?>