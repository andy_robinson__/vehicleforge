<?php
require_once '../env.inc.php';
require_once APP_PATH . 'common/include/pre.php';
require_once 'community_util.php';

use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('community_style.css');
use_javascript('bootstrap/tabs.js');

$MemberCount=getMemberCount();
$MembersPerPage=20;
$Members=getActiveMembers(0, MEMBER_PAGE_COUNT);

$JS='$(function(){
        var members=[],
            interval,
            page=1,
            member_count=' . $MemberCount . ',
            $memberArea=$("#members"),
            $page_nav=$("#page_nav li"),
            $page_nav_links=$(".page_nav a"),
            $pageNext=$(".page_next"),
            $pagePrev=$(".page_prev"),
            $searchForm=$("#search"),
            $searchQuery=$("#search_query"),
            $clearSearch=$("#clear_search"),
            $modalUser=$("#modal_user"),
            member_area_height=$memberArea.height(),
            member_cache=[],
            last_page=1;
              
        //Load the first page into cache
        //Necessary for a search, then clearing the results, to load page 1 from cache
        $.ajax({
               url     : "ajax/browse_users.php",
               dataType: "json",
               type    : "post",
               data    : {
               page: 1
               }
               }).done(function(data){
                        member_cache[1]=data.html;
                      });

        //Keeps the divs height static when fading users
        $memberArea.height(member_area_height);

        /**
         * Fades out a single member from the list
         *
         * @param Callback
         */
        function fadeOut(Callback){
            if (members.length){
                members[0].fadeOut(function(){ $(this).remove(); });
                members.splice(0,1);
            }
    
            if (members.length===0){
                clearInterval(interval);
                Callback.call();
            }
        }

        /**
         * Fades in a single member from the list
         */
        function fadeIn(){
            if (members.length){
                members[0].fadeIn();
                members.splice(0,1);

                if (members.length===0)
                    clearInterval(interval);
            }
        }

        /**
         * Fades in all members in the list
         */
        function fadeInMembers(){
            if (members.length)
                interval=setInterval(fadeIn, 50);
        }

        /**
         * Fades out all members in the list
         *
         * @param Callback
         */
        function fadeOutMembers(Callback){
            loadMemberArray();
            members.reverse();
            interval=setInterval(fadeOut, 50, Callback);
        }

        /**
         * Loads a page of users
         * If a page has already been loaded before, it loads the page from a cache
         */
        function load(){
            if (member_cache[page]){
                $memberArea.html(member_cache[page]);
                loadMemberArray();
                fadeInMembers();
            }else{
                $.ajax({
                       url         : "ajax/browse_users.php",
                       type        : "post",
                       dataType    : "json",
                       data        : {
                       page    : page
                       }
                       }).done(function(data){
                                   $memberArea.html(data.html);
                                   loadMemberArray();
                                   member_cache[page]=data.html;
                                   fadeInMembers();
                               });
            }
        }
        
        function updatePageControl(cur_page){
            var num_pages=' . ceil($MemberCount / $MembersPerPage) . ';
            // Show paging only if there is more than one page
            if(num_pages > 1){
                // Create previous page link
                var page_control="<ul class=\"page_nav\"><li id=\"prev_page\" ><a href=\"#\">&lt;</a></li>";
        
                var start=cur_page-3;
                var end=(cur_page*1)+3;
            
                // Check that start and end are within bounds
                if (start < 1){
                    start=1;
                }
                if (end > num_pages){
                    end=num_pages;
                }
                
                // Create links for each page
                var i=1;
                for (i=start;i<=end;i++){
                    page_control += "<li id=\"pg_" + i + "\"><a href=\"#\">" + i + "</a></li>";
                }
            
                // Create next page link
                page_control += "<li id=\"next_page\"><a href=\"#\">&gt;</a></li></ul>";
                
                // Add paging to html
                $("#member_pages").html(page_control);
            
                // Mark the active page
                $page_nav.removeClass("active");
                $("#pg_"+page).addClass("active");
            
                // Disable previous page if we are on the first page
                if(page <= 1){
                    $("#prev_page").addClass("disabled");
                } else {
                    $("#prev_page").removeClass("disabled");
                }
                
                // Disable next page if we are on the last page
                if(page >= num_pages){
                    $("#next_page").addClass("disabled");
                } else {
                    $("#next_page").removeClass("disabled");
                }
            }
                    
        }
        
        $page_nav_links.live("click", function(){
             if(!($(this).parent().hasClass("disabled") || $(this).parent().hasClass("active"))){
                 var page_num=$(this).text();
                 var last_visited_page=page;
                 
                 if (page_num == "<"){
                    // Previous page
                    if(page > 1){
                        page--;
                    }
                 } else if (page_num == ">"){
                    // Next page
                    if (page < (member_count/20)){
                        page++;
                    }
                 } else if (!isNaN(page_num)){
                    // Specified page (the page number is valid)
                    page=page_num;
                 } else {
                    // Default to page 1
                    page=1;
                 }
                 
                 fadeOutMembers(function(){ load(); });
                 updatePageControl(page);
             }
        });

        updatePageControl(page);

        /*
        $alt_page_nav.click(function(){
            if ($(this).text() == "<<"){
                page--;
            } else if ($(this).text() == ">>"){
                page++;
            } else {
                page=$(this).text();
            } 
            fadeOutMembers(function(){ load(); });
        });
        
        $pageNext.click(function(){
            if (page*' . MEMBER_PAGE_COUNT . '<member_count){
                page++;
                fadeOutMembers(function(){ load(); });
                $pageNav.val(page);
            }
        });
        
        $pagePrev.click(function(){
            if (page > 1){
                page--;
                fadeOutMembers(function(){load();});
                $pageNav.val(page);
            }
        });
        */
         
        $searchForm.submit(function(){
            var q=$searchQuery.val(),
                type=$(".search_type:checked").val();
                   
            $.ajax({
              dataType  : "json",
              url       : "ajax/search_users.php",
              data      : {
                            type    : type,
                            q       : q
                          }
            }).done(function(data){
                        last_page=page;
                        $clearSearch.show();
                        fadeOutMembers(function(){
                                        $memberArea.html(data.html);
                                        loadMemberArray();
                                        fadeInMembers();
                                      });
                       });
        
            return false;
        });
 
        /**
         * Loads all members in the #members div into the members array
         */
        function loadMemberArray(){
            members=[];
            $memberArea.children("li").each(function(){
                members.push($(this));
            });
        }

        $(".user_thumb").live("click", function(){
            var uid=$(this).data("uid");
            
            $.ajax({
                url         : "ajax/get_user_data.php",
                dataType    : "json",
                data        : {
                                uid     : uid
                              }
            }).done(function(data){
                               $modalUser.find("h3").text(data.realname);
                               $modalUser.find(".image").html(data.image);
                               $modalUser.find("#profile_link").attr("href", data.link);
                               $modalUser.find(".about_me").html(data.about);
                               var html="";
                               
                               for(var i=0, l=data.skills.length, el; i<l; i++){
                                    el=data.skills[i];
                                    html+=el[0]+"<br /><i>"+el[1].join(", ")+"</i><br />";
                               }
                               
                               $modalUser.find(".skills").html(html);
                               
                               $modalUser.modal("show");
                           });
        });

        $clearSearch.click(function(){
            page=last_page;
            fadeOutMembers(function(){
                load();
                $clearSearch.hide();
            });
        });
    });';
    
add_js($JS);

site_community_header(array('title' => 'VehicleForge Members'));
$Layout -> col(3, true);
?>
    <div class="well">
        <h6>Search Members</h6>
        <form id="search" action="members.php" method="get" class="form form-inline" style="margin-bottom: 0">
            <div class="input-append">
                <input id="search_query" size="16" name="q" type="text" style="width: 108px;"><button class="btn btn-primary" type="button">Search</button>
            </div>
            <div class="controls">
                <label class="radio inline">
                    <input type="radio" class="search_type" name="search_type" id="search_name" value="name" checked> Names
                </label>
                <label class="radio inline">
                    <input type="radio" class="search_type" name="search_type" id="search_skills" value="skill"> Skills
                </label>
            </div>
            <button type="reset" class="btn hidden" id="clear_search">Clear</button>
        </form>
    </div>
<?php
// Check if user is logged in
if (session_loggedin()) {
?>
    <div class="cell">
        <div class="widget featurette">
            <div class="widget-header">
                <h6>People I am Following</h6>
            </div>
            <div class="widget-body">
                <?php
                    $Users=session_get_user() -> getSubscriptionsUsers();
                    if(empty($Users)){
                        echo "<p style='text-align:center'><small>You aren't following anyone.</small></p>";
                    }else{
                        foreach ($Users as $i){
                            echo '<a href="' . $i -> getURL() . '" class="inline">' . $i -> getImageHTML(50, 50) . '</a>';
                        }
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="cell">
        <div class="widget featurette">
            <div class="widget-header">
                <h6>People Following Me</h6>
            </div>
            <div class="widget-body">
                <?php
                    $Users=session_get_user() -> getSubscribers();
                    if(empty($Users)){
                        echo "<p style='text-align:center'><small>You have no followers.</small></p>";
                    }else{
                        foreach ($Users as $i){
                          echo '<a href="' . $i -> getURL() . '" class="inline">' . $i -> getImageHTML(50, 50) . '</a>';
                        }
                    }
                    
                ?>
            </div>
        </div>
    </div>
    <!-- Recently Viewed (TODO)
    <div class="cell">
        <div class="widget featurette">
            <div class="widget-header">
                Recently Viewed
            </div>
            <div class="widget-body">
    
            </div>
        </div>
    </div>
    -->
<?php
    }
    $Layout -> endcol() -> col(9); 
?>
    <div class="cell" id="member_page_control" >
        <div class="widget" style="overflow: hidden; padding: 6px 2px 2px 10px;">
            <!-- Sorting (TODO)
            <div class="pull-left">
                SORT BY
                <select class="span2" style="width: 58%">
                <option value="name">Name</option>
                <option value="reputation">Reputation</option>
                </select>
            </div>
            -->
            <div class="pull-right">
               <div class="pagination" id="member_pages"></div>
            </div>
            <div class="pull-left">
                <h6 style="margin: 5px 0 0 0;"><?=$MemberCount?> Members (<?=getOnlineMemberCount()?> Online)</h6>
            </div>
        </div>
    </div>
    <style>
        .thumbnails .thumbnail {
            height: 100%;
            text-align: center;
        }
    
        .thumbnail img {
            cursor: pointer;
        }
    
        .thumbnails .thumbnail .caption {
            text-align: left;
        }
        #member_page_control .pagination {
            margin: -4px 0 0;
        }
        #member_page_control form {
            margin: 0;
        }
    </style>
    
    <ul class="thumbnails" id="members">
        <?php
           foreach ($Members as $i)
               echo formatMember($i['user_id']);
        ?>
    </ul>
    <div class="modal hide fade" id="modal_user">
        <div class="modal-header">
            <button class="close" data-dismiss="modal">
                x
            </button>
            <h3></h3>
        </div>
        <div class="modal-body">
            <div class="image pull-left"></div>
            <div class="about pull-right">
                <h6>About</h6>
                <p class="about_me"></p>
                <h6>Skills</h6>
                <p class="skills"></p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="pull-right btn" id="profile_link">Visit Profile</a>
        </div>
    </div>
    
<?php
$Layout -> endcol();
include $gfwww . 'help/help.php';
site_community_footer();
?>
