<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

use_stylesheet('/themes/gforge/css/widget.css');
use_javascript('bootstrap/tabs.js');
site_community_header(array('title'=>'Discussion Board'));


$Layout->col(12,true);
$HTML->heading(_('This is the forum to discuss system questions and general problems.'),3);
?>

<?php
$Layout->endcol();
site_community_footer();
?>