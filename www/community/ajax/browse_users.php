<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once '../community_util.php';

$Page=getIntFromRequest('page')-1;

$Return=array();

$HTML='';
$Members=getActiveMembers($Page*MEMBER_PAGE_COUNT, MEMBER_PAGE_COUNT);
//print_r($Members);
for($i=0, $l=sizeof($Members), $Member=null; $i<$l; $i++){
    $Member=$Members[$i];
    $HTML.=formatMember($Member['user_id'], true);
}

echo json_encode(array(
    'html'  => $HTML
));
return true;
?>