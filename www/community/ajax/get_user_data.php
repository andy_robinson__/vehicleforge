<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$UserID=getIntFromRequest('uid');
$User=user_get_object($UserID);
$UserSkills=$User->getSkills();

$Skills=array();

foreach($UserSkills as $i){
    $Skills[]=array($i->getSkillName(), $i->getKeywords());
}

echo json_encode(array(
    'realname'  => $User->getRealName(),
    'unix_name' => $User->getUnixName(),
    'image'     => $User->getImageHTML(150,150),
    'link'      => $User->getURL(),
    'skills'    => $Skills,
    'about'     => $User->getAbout()
));

return true;
?>