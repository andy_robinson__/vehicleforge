<?php
require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/SiteActivity.class.php';
$SiteActivity=new SiteActivity;

use_stylesheet('/themes/gforge/css/feed.css');

site_header(array('title'=>'Site Activity'));
$Layout->col(12,true);

echo $SiteActivity->getSiteActivityContent(10);

$Layout->endcol();
site_footer();
?>