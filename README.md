# README 

## Overview 
VehicleFORGE is a web-based collaboration platform that supports cyber-physical project development and connectivity throughout the digital thread. It also integrates with DOME to provide a service marketplace which allows for cloud-based execution through a browser.

## Getting DOME 
The DOME source code can be retrieved via its git repository or periodic releases at http://projectdmc.org/.

## Contributing 
If you're interested in contributing to the source code base, please contact dmc@uilabs.org.

## Building 
Coming soon

## License 
The vehicleforge core package is distributed under an MIT/X11 open source license. See license.txt for the full copyright and license.

## Packaging Disclaimer 
For convenience, the source code of several dependencies has been bundled with the DOME core package. Many of these have their own licenses, and users of this software are responsible for understanding the implications of modifying this source code as part of the entire distribution. 

